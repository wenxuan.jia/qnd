# Quantum Noise Modeling of LIGO Livingston Detector in O4

This repo contains all codes used to infer quantum noise model for L1 detector in O4. The technical report can be found in [T2300439](https://dcc.ligo.org/LIGO-T2300439). 

Related alogs:

Part I: Sensing function [llo68752](https://alog.ligo-la.caltech.edu/aLOG/index.php?callRep=68752)

Part II: Calibrated shot noise [llo69037](https://alog.ligo-la.caltech.edu/aLOG/index.php?callRep=69037)

Part III: Frequency-Independent Squeezing Study [llo69096](https://alog.ligo-la.caltech.edu/aLOG/index.php?callRep=69096)

Part IV: Infer IFO and FIS Parameters[llo69098](https://alog.ligo-la.caltech.edu/aLOG/index.php?callRep=69098)

Part V: Nominal Frequency-Dependent Squeezing [llo69161](https://alog.ligo-la.caltech.edu/aLOG/index.php?callRep=69161)

## Description

We use Gravitational Wave Interferometer Noise Calculator (GWINC) to model the quantum noise of the L1 detector. The model parameters are inferred from measured DARM noise with frequency-independent and frequency-dependent squeezing at various squeezing angles. 

## Getting Started

You can simply run the install_packages.sh to install all required Python packages. Otherwise, you can read the shell script to customize installation. 

The main code is qnd.ipynb. Each cell of the notebook is supposed to run independently from others. The MCMC simulations can be found in the /fig/ directories. 
