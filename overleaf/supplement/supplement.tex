\onecolumngrid

\appendix

\section*{Supplemental Material}\label{sec:supplement}

\section{Uncertainty Analysis}

The argument that the LIGO detector operates beyond the standard quantum limit (SQL) requires a high statistical significance of the inferred quantum noise below the SQL. We follow our previous work \cite{Haocun_nature} to estimate the total uncertainty of the inferred quantum noise. The formalism is briefly summarized here. The strain noise power spectral density (PSD) of inferred quantum noise $Q(\Omega)$ is obtained from measured reference (unsqueezed) total noise $D_r(\Omega)$, measured squeezed total noise $D_s(\Omega)$, and model of the reference quantum noise $M(\Omega)$:
\begin{equation} \label{eq: Q}
    Q(\Omega) = D_s(\Omega) - (D_r(\Omega) - M_r(\Omega)) .
\end{equation}
%
The total uncertainty of $Q(\Omega)$ is
\begin{equation}
    \Delta Q^2(\Omega) = Q^2(\Omega) \delta G_{cal}^2(\Omega) + \left[\Delta D_s^2(\Omega) + \Delta D_r^2(\Omega) + \Delta M_r^2(\Omega) + (D_r(\Omega) - M_r(\Omega))^2 (\delta N_t^2(\Omega) + \delta N_m^2(\Omega)) \right]
\end{equation}
%
where
\begin{itemize}
    \item $\delta G_{cal}(\Omega)$ is the reported combined calibration error and uncertainty estimate \cite{calib_paper},
    \item $\Delta D(\Omega)$ is the statistical uncertainty due to PSD estimation,
    \item $\Delta M_r(\Omega)$ is the uncertainty of the unsqueezed reference quantum noise model, and
    \item $\delta N(\Omega)$ describes the non-stationary changes in the classical noise contributions, where $\delta N_t(\Omega)$ is time-nonstationarity and $\delta N_m(\Omega)$ is the operating mode nonstationarity between unsqueezed and squeezed operating modes.
\end{itemize}

In this paper, we follow the convention in \cite{Haocun_nature} and use $\Delta$ to describe the 1-$\sigma$ uncertainty of the variable, and use $\delta$ for the relative uncertainty $\delta D = \Delta D/D$. We plot the noise spectrum in units of amplitude spectral density (ASD) $q(\Omega) = \sqrt{Q(\Omega)}$. The relative error in ASD is
\begin{equation}
    \delta q(\Omega) = \dfrac{1}{2} \delta Q(\Omega) =  \sqrt{\dfrac{\delta G_{cal}^2(\Omega)}{4} + \dfrac{1}{4 Q^2(\Omega)} \left[\Delta D_s^2(\Omega) + \Delta D_r^2(\Omega) + \Delta M_r^2(\Omega) + C^2(\Omega) (\delta N_t^2(\Omega) + \delta N_m^2(\Omega)) \right]} .
\end{equation}


\subsection{Re-binning Power Spectral Density}

The statistical uncertainty $\delta D$ of the PSD scales inversely with the square root of the number of averages, which is proportional to the product of the duration $T$ of the time series and the frequency bin width $f$
\begin{equation} \label{eq: deltaD}
    \delta D = \frac{1}{\sqrt{ T f}} .
\end{equation}

We first take the linear FFT of the raw time series to estimate the total noise PSD. For each frequency bin, we take the median statistics to indirectly remove potential glitches in the time series, as described in our previous work \cite{Haocun_nature}. 

The linearly spaced PSD has the constant frequency bin width, for which we choose a frequency resolution of \SI{0.0625}{Hz}. To reduce the statistical uncertainty and fit the model, we re-bin the PSD into a log-spaced frequency bins. Each new frequency bin collects all the energy of the old frequency bins that falls into the bin so that the total spectral energy is conserved. The statistical uncertainty of the new PSD with log-spaced and larger bin width still follows the relation of \cref{eq: deltaD}.

The raw PSD measures the total differential displacement between the two pairs of arm cavity mirrors, which contain many peaks and resonances including harmonics of the 60-Hz power line and 500-Hz violin mechanical modes of test masses suspensions, etc. These peaks would inflate the energy of our re-binned PSD. Therefore, we remove all the known noise peaks before re-binning. 

\subsection{Non-stationarity Verification}
The stationarity uncertainty has two contributing terms: time-nonstationarity $\delta N_t (\Omega)$ that captures slow thermal drifts of the interferometer, and mode-nonstationarity $\delta N_m(\Omega)$ that contains changes introduced by different operating modes of the interferometer, namely with and without squeezing. 

To measure the unsqueezed total noise as closely as the configuration with frequency-dependent squeezing, we set up the squeezing configuration but without squeezed vacuum generated. Specifically, we leave both the squeezer and filter cavity locked on resonance but without the nonlinear parametric down-conversion process. As seen in \cref{fig: LIGO}, we only send auxiliary control sidebands to the squeezer cavity for lock acquisition \cite{O4FDS}, but not the 532-nm pump laser. The squeezer is locked on the resonance to allow transmission of the control field to filter cavity. The filter cavity is also locked on resonance with the auxiliary field to mimic the nominal operation with frequency-dependent squeezing. If there is any extra technical noise introduced with frequency-dependent squeezing, for example backscatter noise driven by filter cavity length fluctuations, the interferometer would sense it in the total noise spectra in both configurations. 

To confirm if there are any excessive noises including backscatter, we compare the total unsqueezed interferometer noise with the following two operating modes. The first one is to open the squeezer beam diverter to mimic the frequency-dependent squeezing case as mentioned above, and the second one is to close the squeezer beam diverter on the injection path such that no backscattered light can be transmitted between interferometer and squeezing system. We follow Eq.(13) in \cite{Haocun_nature} to estimate the uncertainties. We have two PSD of each mode and calculate $\delta N(\Omega)$ between PSDs of the same and different operating modes.

\begin{figure}
    \centering
    \includegraphics[width=0.6\linewidth]{fig/deltaN_m.pdf}
    \caption{Comparison of the unsqueezed noise stationarity between two PSDs measured in the same unsqueezed operating mode (e.g. two segments with the squeezer beam diverter open), or measured in two different unsqueezed operating modes (e.g. with squeezer beam diverter open and beam diverter closed). Both uncertainties are the same, suggesting the squeezer system does not introduce excess technical noise in the full detectors.
    % stationarity uncertainty estimated from two PSDs taken at the same operating mode and different operating mode. Both uncertainties are the same, so the squeezing system doesn't introduce extra technical noise.
    }
    \label{fig: deltaN_m}
\end{figure}

\cref{fig: deltaN_m} shows that the stationarity uncertainty curves are nearly identical between two PSD taken at the same operating mode or different operating mode, confirming that the mode-nonstationarity contribution to the total stationarity uncertainty is negligible. 

The time-nonstationarity occurs due to thermal drifts of the interferometer. For the faster averaging timescales used in our measurements, slow drifts can be reduced with longer averaging times, similarly to statistical PSD estimation. Therefore, both of drifts and statistical uncertainties are reduced after the re-binning process. %It shows similar dependence with averaging like the statistical error. 

Calibration uncertainty $\delta G_{cal}(\Omega)$ are estimated in the same way as \cite{Haocun_nature}. Note that it is a form of systematic error instead of statistical error. Therefore, the calibration error is added to the total uncertainty after re-binning, since it can not be reduced by averaging. The contributions of aforementioned uncertainties to the total uncertainty are shown in \cref{fig: errbudget}.

\begin{table}[ht]
\centering
\caption{Parameters of the LIGO Livingston detector inferred using Markov Chain Monte Carlo (MCMC) methods. Fixed and chosen parameters are input parameters for the MCMC, which infers the ``common" and ``independent" parameters. Common parameters are shared across squeeze angle measurements, whereas independent parameters are allowed to change across squeeze angle measurements. See \cite{aLIGO,Aaron} for more detailed parameters of LIGO, including e.g. optic transmissivities.}
\renewcommand{\arraystretch}{1.25}
\begin{tabular}{l c c c c}
    \hline     
    &  \multicolumn{3}{c}{MCMC Set-up} & Inferred  \\
    & Fixed/  & Prior & Initial walker & Common/  \\
    & Chosen  &  Gaussian $(-\sigma, \sigma)$ & Flat probability &  Independent  \\
    \hline
    \textbf{Interferometer parameters}  \\
    \ \ \ \ Circulating power in arm cavity  && (270, 320) kW & 270 - 310 kW & $257_{-1.6}^{+3.9}$ kW \\
    \ \ \ \ Arm to SEC mismatch & 2.7\%  \\
    \ \ \ \ Arm to SEC mismatch phase  & 0$^\circ$  \\
    \ \ \ \ SEC round-trip detuning phase  & $0.14^{\circ}$ \\
    \ \ \ \ SEC round-trip Gouy phase  && (20, 50) $^{\circ}$ & 20$^{\circ}$ - 70$^{\circ}$ & $43.0_{-5.2}^{+4.5} \ ^\circ$  \\
    \ \ \ \ Readout angle &-11$^\circ$\\
    % \hline
    \textbf{Total readout loss } &&(8, 10) \% & 6\% - 10\%& $8.0^{+1.2}_{-0.5}$ \%\\
    \ \ \ \ IFO to OMC mismatch  && (6, 8) \% & 4\% - 10\%& $3.6_{-0.5}^{+0.5}$ \% \\
    \ \ \ \ IFO to OMC mismatch phase  &&&& Independent \\
    \hline
    \textbf{Squeezing parameters}&&\\
    \ \ \ \ Generated squeezing  & 17.4 dB \\
    \ \ \ \ Squeezing angle &  Chosen \\
    \textbf{Total Injection efficiency}  & 92.9\% \\ 
    \ \ \ \ SQZ to OMC mismatch  && (1, 8) \% & 1\% - 8 \% & $1.1_{-0.2}^{+1.3}$ \% \\
    \ \ \ \ SQZ to OMC mismatch phase  & -45$^\circ$ \\
    \textbf{Phase noise} (RMS)  & Chosen  \\
    \hline
    \textbf{Filter cavity parameters}&&\\
    \ \ \ \ Length  & 300 m\\
    \ \ \ \ Detuning    & & ($-28$, $-25$) Hz & $-31$ Hz - $-26$ Hz &  $-25.6$ Hz  \\ 
    \ \ \ \ Finesse  & 7000   \\
    \ \ \ \ Full-linewidth   & 71 Hz   \\
    \ \ \ \ Input coupler transmission && (800, 900) ppm & 750 ppm - 880 ppm & 797 ppm\\
    \ \ \ \ Derived round-trip loss   && && 100 ppm\\
    \ \ \ \ Squeezer to FC mismatch     & 0.2\%\\
    \ \ \ \ Squeezer to FC mismatch phase    && ($-180$, 180) $^{\circ}$ &$-180^{\circ}$ - 180$^{\circ}$&  $-65^\circ$\\
    \ \ \ \ Length noise (RMS)  && (0.1, 1) pm & 0.1 pm - 2 pm & 0.2 pm  \\
    \hline
\end{tabular}
\label{tab: params}
\end{table}

\section{Full Quantum Noise Model}
The only remaining source of uncertainty to be discussed is the modeling uncertainty $\delta M(\Omega)$. In this paper, we use a novel method to estimate and constrain model parameters with Markov Chain Monte Carlo (MCMC) inference. Before discussing details of the inference method, we briefly explain the latest model of quantum noise. 



The LIGO detector is essentially an assembly of individual optical cavities. The core optics of the interferometer is composed of two 4-km long arm cavities as two Michelson arms. The two input ports of the Michelson have two partially-reflective mirrors to boost arm power and increase signal bandwidth separately at bright and dark port (see \cref{fig: LIGO}). The squeezing system generates squeezed vacuum using an optical parametric amplifer cavity, performs frequency-dependent rotation with a detuned filter cavity, and couples into the interferometer at the dark port. Each cavity in the system has degredations like optical losses and off-resonance detunings. In addition, there are non-zero mismatches between the fundamental spatial modes of two consecutive cavities. The full model captures all of these non-idealities based on our latest theoretical work \cite{Lee_LIGO_response}. 


We use Gravitational Wave Interferometer Noise Calculator (GWINC) to numerically compute the detector quantum noise. It is a phenomenological and analytical model that is derived from input-output relations \cite{buonannoPRD01QuantumNoise}. It extends the optical fields of fundamental (TEM$_{00}$) spatial mode to one higher-order (TEM$_{20}$) mode \cite{buonannoPRD01QuantumNoise, Lee_LIGO_response}. The model includes all the decoherences, degradations, and dephasings of the squeezed vacuum \cite{DnD}. The full sets of parameters can be found in \cref{tab: params}.



For parameter estimation, we rely on external measurements as our priors whenever possible. But, we do not have external measures of several quantities, and for others, external measurements do not have the necessary accuracy and precision, and may vary if not measured in-situ. We use MCMC, informed by external measurements whenever possible, to estimate experimental parameters. The full model of interferometer with frequency-dependent squeezing has a total of 20 impactful parameters. We reduce the problem by isolating the interferometer from the squeezing system first. Then we introduce the squeezer parameters to model frequency-independent squeezing measurements, and finally the filter cavity to model frequency-dependent squeezing measurements. 


\subsection{Inferring Interferometer Parameters}

LIGO employs an active calibration system, known as the Photon Calibrator \cite{Pcal}, to calibrate the measured optical power into meters of differential arm length. The system actively modulates the differential arm length by sending an amplitude-modulated laser beam on the test mass. Therefore, we can directly measure the interferometer's transfer function (in units of meters/milliAmp, often called the ``sensing function'') by sweeping the Photon Calibrator laser frequency. 

At the dark port of the interferometer, LIGO has an additional mirror, known as signal recycling mirror with 32.5\% power transmission, to effectively broaden the sensitivity bandwidth to the differential arm length signal. The cavity formed by signal recycling mirror and the interferometer has parameters such as loss, mode-mismatch, and off-resonance detuning, which directly impact the measured sensing function. Therefore, we can isolate and infer these parameters by fitting the sensing function with MCMC. The total parameter space is reduced after we successfully infer parameters of the signal recycling cavity from the sensing function. 



\subsection{Inferring Frequency-Independent Squeezing Parameters}

After inferring the parameters of the signal recycling cavity, we feed them into the model that describes the squeezed interferometer. We simplify the squeezing system by bypassing the filter cavity first. It is often difficult to infer model parameters when many parameters of the model are degenerate. For example, mode-mismatch and loss between interferometer and output mode cleaner cavity are degenerate when this mode-mismatch is the only mismatch in the optical path \cite{Lee_LIGO_response}. If we introduce multiple mismatches to break the degeneracy, there are redundant parameters that provide more than one solution to satisfy measurements.

To constrain the quantum noise model, we change the squeezing angle $\phi$ to alter the quantum noise $S_{\text{SQZ}}(\Omega)$ in \cref{eq: model} while keeping the filter cavity end mirror misaligned to simplify the system ($\phi$ is frequency-independent in this case). Since we have only changed the squeezing parameter, there should exist a set of model parameters that can fit all of the measurements by only altering the squeezing angle, if the model fully captures the physics. Assuming such a set of common parameters should break certain degeneracies in the model and constrain the parameter space.


Experimentally, we misalign the filter cavity and change the the squeezing angle $\phi$ by adjusting the offset of locking point of the phase-locking-loop between frequency-independent squeezing and the local oscillator field of the interferometer. We operate the interferometer in an unsqueezed mode (pump laser blocked so no squeezed photons are being generated) and frequency-independently squeezed mode at various $\phi$. 20-minute time series data is taken in each operating mode. Assuming the classical noise $C(\Omega)$ is stationary across different configurations, we can take the difference of two measured total noise PSDs and model the quantum noise differences (\cref{eq: model}),
\begin{equation}
    S_\text{diff}(r,\phi) = D_s(r,\phi) - D_r(r=0) = S(r,\phi) - S(r=0)
\end{equation}
where the total measured noise is $D(\Omega) = S(\Omega) + C(\Omega)$. Although we can not directly measure the classical noise, we can still model the quantum noise difference that is measurable. 


\begin{figure}
    \centering
    \includegraphics[width=1.0\linewidth]{fig/MCMC_FIS.pdf}
    \caption{Inference results on the difference of total noise between frequency-independent squeezed and unsqueezed interferometer at various squeezing angles. The negative PSD difference means that the quantum noise is being squeezed. The residual between model and measurements are normalized by the 1-$\sigma$ uncertainty and shown in the bottom plot. }
    \label{fig: MCMC_FIS}
\end{figure}


LIGO reads out optical power fluctuations that are transmitted (cleaned) by the output mode cleaner cavity. The transmitted beam is divided onto two photodetectors using a 50/50 beam splitter, and we read out the summation of the photocurrent signals. The sum reads out the squeezed quantum noise we observe, and the difference of the two photocurrents, known as the null channel, subtracts all of the correlated noise and only leave the uncorrelated noises of the two photodiodes, namely quantum shot noise and dark noise of the detector. The null channel provides a simultaneous monitoring of the calibrated quantum shot noise, which is computed by dividing the flat quantum shot noise in milliAmps by the sensing function. 

We find the best inference of the parameters with MCMC. We use a Gaussian likelihood with a set of Gaussian priors for each parameter. For each measurement with certain squeezing angle, we fit both the noise difference and the quantum shot noise, the latter of which is used to infer the readout loss. The initial walkers are distributed with a flat probability in a bounded interval. As a result, the method is able to find a set of common parameters that minimize the residual of all squeeze angle measurements, as presented in \cref{fig: MCMC_FIS} and \cref{tab: params}.



There are four types of parameters in our inference methods:
\begin{itemize}
    \item ``Fixed" parameters are fixed across all squeeze angle datasets. For example, the signal recycling cavity parameters we inferred earlier are assumed to be the same for all. 
    \item ``Chosen" parameters are selected and different for each squeeze angle dataset. For example, the squeezing angle is actively changed to obtain different squeezing PSD.
    \item ``Common" parameters are shared degrees of freedom that MCMC infers a single value across all squeeze angle datasets. For example, the power within arm cavity should be the same across measurements, and we use MCMC to infer its exact number.
    \item ``Independent" parameters are degrees of freedom of MCMC infers differently for each squeeze angle dataset. % is different across measurements. 
\end{itemize}

In \cref{tab: params}, we set the squeezing angle and phase noise as ``chosen parameters". It is known that the residual phase noise error of the aforementioned phase-locking-loop depends on the control offset and therefore the squeezing angle. To be able to fit the PSD difference, we still need to set the mode-mismatch phasing between interferometer and output mode cleaner (\cref{fig: LIGO}) as an ``independent parameter", which is an extra phase in the optical path calculated from 2-dimensional overlap integral of the wavefronts of two eigenmodes of two cavities \cite{Lee_LIGO_response}. This mode-mismatch phasing only helps us fit the model phenomenologically, and is not expected to physically depend on the squeezing angle. Instead, the MCMC adjusts this phasing to mimic certain physics that is not fully captured in the latest model in order to fit the measurements. 

\cref{fig: MCMC_FIS} validates our quantum noise model as we successfully fit all of the measurements at various squeezing angles by independently tuning a minimal set of parameters. The model uncertainty $\delta M(\Omega)$ is obtained by taking the 16th and 84th percentile of the model curve computed from the parameters of the MCMC chain (after burning in). Now we collect all sources of uncertainties (\cref{fig: errbudget}) and compute the inferred quantum noise with frequency-dependent squeezing. 


\begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\linewidth]{fig/MCMC_FDS.pdf}
    \caption{Inference of the quantum noise with frequency-dependent squeezing. }
    \label{fig: MCMC_FDS}
\end{figure}


\subsection{Inferring Filter Cavity Parameters}

Using the interferometer and squeezer quantum noise models obtained in the previous subsections, we can compute the inferred quantum noise ASD with frequency-dependent squeezing from \cref{eq: Q}. We perform a final MCMC to infer the remaining filter cavity parameters.



Since LIGO is currently operating at a lower arm power than the designed value, the filter cavity is not operating in the optimal configuration \cite{whittlePRD20OptimalDetuning}. This is the reason why the current frequency-dependent squeezed quantum noise does not trace the sub-SQL dips of each frequency-independent measurements, in addition to a noise bump near \SI{80}{Hz} due to scattered light. In the MCMC, we assumed the filter cavity finesse to be 7000 in order to fit external cavity ringdown and linewidth measurements of the filter cavity. The inferred parameters are shown in \cref{tab: params}.


\section{Total Uncertainty Budget}
Now that we have collected all sources of the uncertainties $\delta q (\Omega)$ of the inferred quantum noise amplitude spectral density $q(\Omega)$, we can add these independent noises together in quadrature to obtain the final 1-$\sigma$ uncertainty. The contributions of each uncertainty is shown in \cref{fig: errbudget}.


\begin{figure}[h!]
    \centering
    \includegraphics[width=0.8\linewidth]{fig/errbudget.pdf}
    \caption{Total uncertainty budget of inferred quantum noise from various error sources. }
    \label{fig: errbudget}
\end{figure}

The statistical uncertainty dominates both positive and negative error bars at low frequency due to the small frequency bin width (\cref{eq: deltaD}). At high frequencies above \SI{500}{Hz}, the statistical error decreases as there are more averages available per bin width. Both statistical and stationarity error are symmetrical, whereas the calibration error and modeling error are not. The calibration error, obtained from the calibration pipeline \cite{calib_paper}, dominates at high frequency above \SI{200}{Hz}. 

Considering all measurement uncertainties, the LIGO detector operates with sub-SQL quantum noise at more than 3-$\sigma$ statistical confidence, as enabled by frequency-dependent squeezing (\cref{fig: moneyplot}). 



\section{Sub-SQL Performance}

\begin{figure}[ht]
    \centering
    \includegraphics[width=1.0\linewidth]{fig/SQL_FIS.pdf}
    \caption{Quantum noise reduction in strain amplitude spectral density. Blue, olive, lime, and teal traces show the inferred quantum noise with frequency-independent squeezing injected at four different squeeze angles $\phi$. The three purple traces show the quantum noise with three frequency-dependent squeezing configurations, same as \cref{fig: QND_FIS}.}
    \label{fig: SQL_FIS}
\end{figure}

\cref{fig: SQL_FIS} compares the sub-SQL performance with frequency-independent squeezing (constant squeezing angle $\phi$) and frequency-dependent squeezing ($\phi = \phi(\Omega)$). The sub-SQL dip can be produced by sending squeezing at a fixed angle, as previously observed \cite{Haocun_nature}. However, the dip has a very narrow frequency range. Although we can move the dip frequency by changing squeezing angle, it is not an optimal configuration for maximum sensitivity at all frequencies. As mentioned in the main text, frequency-dependent squeezing can theoretically achieve the sub-SQL envelope that covers all dips that frequency-independent squeezing can achieve (dotted purple). The current and optimal filter cavity are more realistic configurations, and they are the same as \cref{fig: QND_FIS}. 



\section{Future Filter Cavity Upgrade}


While we demonstrate that the optimal lossless filter cavity is able to simultaneously achieve all sub-SQL dips that frequency-independent squeezing can do, we have to acknowledge the fact that a realistic filter cavity has a non-zero loss. The designed round-trip loss of the filter cavity is \SI{60}{ppm}, compared to the loss of \SI{100}{ppm} suggested by our MCMC. A few different filter cavity configurations are shown in \cref{fig: future_FC}.

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.6\linewidth]{fig/Future_FC.pdf}
    \caption{Comparison of the quantum noise with various filter cavity configurations. }
    \label{fig: future_FC}
\end{figure}

\newpage

In \cref{fig: future_FC}, the relative quantum noise curves with current filter cavity (dashed purple) and optimal filter cavity (solid purple) are identical to \cref{fig: QND_FIS}. If we achieve the designed loss of 60 ppm with current filter cavity, the squeezing will improve from dashed purple to the orange curve. It is only possible to achieve squeezing at all frequencies when we adjust the filter cavity linewidth $\gamma_\text{FC}$ to approach $\Omega_\text{SQL} / \sqrt{2}$, for example, reducing the filter cavity input coupler transmission to \SI{584}{ppm} (purple curve) or increasing the arm cavity power to \SI{500}{kW} (blue curve). The lossless filter cavity is shown in the dotted purple trace. Note that the squeezing in the lossless case is not flat because we have a nonzero phase difference between the local oscillator field and the signal field, known as the readout angle. For each trace in \cref{fig: future_FC}, the detuning frequency of the filter cavity is optimized to maximize sensitivity to binary neutron star inspirals - a standard figure of merit for gravitational wave detectors. 

\newpage

\begin{figure}[ht]
    \centering
    \includegraphics[width=0.8\linewidth]{fig/budget.pdf}
    \caption{Sub-budget of contributions to the total quantum noise. }
    \label{fig: budget}
\end{figure}

\cref{fig: budget} shows the contributions of the total quantum noise plotted in \cref{fig: moneyplot}. At low frequencies below \SI{40}{Hz}, quantum noise is mostly limited by misrotation of the squeezed state due to the non-optimal filter cavity. At high frequencies above \SI{200}{Hz}, squeezing is limited by the losses due to injection, readout, and mode-mismatches along the optical path. Reducing these major noise sources is the key to further quantum enhancement in the LIGO detectors. 

