% Use only LaTeX2e, calling the article.cls class and 12-point type.

\documentclass[12pt]{article}

% Users of the {thebibliography} environment or BibTeX should use the
% scicite.sty package, downloadable from *Science* at
% http://www.sciencemag.org/authors/preparing-manuscripts-using-latex 
% This package should properly format in-text
% reference calls and reference-list numbers.

\usepackage{scicite}

\usepackage{times}

\usepackage{orcidlink}
\usepackage{authblk}
% units
\usepackage[
        range-phrase=--,
        range-units=single,
        retain-explicit-plus=true,
        retain-unity-mantissa=false,
    ]{siunitx}
\usepackage{hyperref}
\usepackage{url}
\usepackage[capitalise]{cleveref}


\newcommand{\K}{\mathcal{K}(\Omega)}
\newcommand{\hsql}{h_{\text{SQL}}(\Omega)}
\newcommand{\arm}{\text{arm}}
\newcommand{\sql}{37}

\renewcommand{\figurename}{Fig.}


% The preamble here sets up a lot of new/revised commands and
% environments.  It's annoying, but please do *not* try to strip these
% out into a separate .sty file (which could lead to the loss of some
% information when we convert the file to other formats).  Instead, keep
% them in the preamble of your main LaTeX source file.


% The following parameters seem to provide a reasonable page setup.

\topmargin 0.0cm
\oddsidemargin 0.2cm
\textwidth 16cm 
\textheight 21cm
\footskip 1.0cm


%The next command sets up an environment for the abstract to your paper.

\newenvironment{sciabstract}{%
\begin{quote} \bf}
{\end{quote}}

\definecolor{clr}{RGB}{196, 22, 29}
\renewcommand{\c}[1]{\textcolor{clr}{#1}}



% Include your paper's title here

\title{Response to reviewer's report on \\
``\textbf{LIGO operates with quantum noise below the Standard Quantum Limit}"} 


% Place the author information here.  Please hand-code the contact
% information and notecalls; do *not* use \footnote commands.  Let the
% author contact information appear immediately below the author names
% as shown.  We would also prefer that you don't change the type-size
% settings shown here.

\author
{Wenxuan Jia\orcidlink{0000-0002-5119-6328},$^{1\ast}$   %wenxuan.jia
Victoria Xu\orcidlink{0000-0002-3020-3293},$^{1\dagger}$  %victoria.xu
Kevin Kuns\orcidlink{0000-0003-0630-3902},$^{1}$   %kevin.kuns
Masayuki Nakano\orcidlink{0000-0001-7703-0169},$^{2}$  \ \ \ \ \ \ \ \ \ \   %masayuki.nakano
Lisa Barsotti\orcidlink{0000-0001-9819-2562},$^{1}$   %lisa.barsotti
Matthew Evans\orcidlink{0000-0001-8459-4499},$^{1}$    %matthew.evans
Nergis Mavalvala\orcidlink{0000-0003-0219-9706},$^{1}$  \ \ \ \ \ \ \ \ \ \  \ \ \ \ \ \ \ \ \ \  \ \ \ \ \ \ \ \ \ \   %nergis.mavalvala
Detector group of The LIGO Scientific Collaboration,$^\ddagger$
\normalsize{\ }\\
\normalsize{$^\ast$To whom correspondence should be addressed; E-mail:  wenxuanj@mit.edu.}\\
\normalsize{$^\dagger$victoriaa.xu@ligo.org.}\\
\normalsize{$^\ddagger$ Detector group of The LIGO Scientific Collaboration authors and affiliations are listed at the end of the main text.}
}

% Include the date command, but leave its argument blank.

\date{}



%%%%%%%%%%%%%%%%% END OF PREAMBLE %%%%%%%%%%%%%%%%



\begin{document} 


% Double-space the manuscript.

\baselineskip24pt

% Make the title.

\maketitle 


\noindent Dear Referees,

Thank you for carefully reading our manuscript. Here are our responses to your comments. 



\section*{Reviewer 1}


\c{The paper titled "LIGO operates with quantum noise below the Standard
Quantum Limit" is well-written and thoughtfully structured.
The lengthy introduction greatly enhances the accessibility of the paper,
making it enjoyable even for readers who are not experts in the field. The
authors' efforts in providing context and background information are
commendable and contribute significantly to the overall readability of the
manuscript.}


We are glad that you enjoyed reading our paper. We tried to write our introduction to be interesting and motivating from a fundamental physics point of view. 


\c{I recommend accepting this paper with only a few minor revisions.
Below are the comments for improvement:}



\c{1. Definition of Acronyms: Acronyms such as PSD, FC, etc., should be
explicitly defined upon their first use in the text to ensure clarity for readers
unfamiliar with these terms.}

Thank you for pointing them out. We've added the definition of these acronyms at their first appearance in the introductions of the main text and supplemental material. 


\c{2. Figure S6 Placement: Figure S6 is referenced in the main text but is only
accessible in the supplementary materials. It would enhance the flow and
understanding of the paper if Figure S6 were moved from the supplementary
section and included within the main text where it is cited.
Overall, the paper is a valuable contribution to the field of gravitational
detector designs. Addressing these minor comments will further enhance its
accessibility and impact.}

Actually, the \textit{Science} journal does not allow discussion of future work in original research papers. The editor has requested us to remove Fig. S6 entirely. Instead, we will only discuss how the current filter cavity is sub-optimal compared to its current theoretical performance, but will not plot expectations of its designed performance as before. 




\section*{Review 2}

\c{Overall, this manuscript is interesting and significant enough for publication in
Science.}

\c{However, I have several comments/questions.}

\c{- Page 4, second paragraph from the bottom: Readers may want to see the
result of 2020 against the current one to appreciate the progress.}

% We agree and actually thought about adding it to the Fig.2 of the paper. However, we choose to not plot it there because the plot is already very busy with 8 traces, but we can definitely add it to Fig. S5 for supplemental material readers. 

We agree. For comparison we have now added the trace from 2020 (\textit{12}, Yu, \textit{et al.}, \textit{Nature} \textbf{583}, 43 (2020)) as the green trace in the main text's new Fig. 3a (which was previously Fig. S5a).



\c{- While this is obvious to experts, it is better to state clearly in an early part
of the manuscript that LIGO detectors can be operated with or without the
squeezer, and that switching modes is relatively easy.}

Thanks for reminding us. We add the beam diverter in the schematics diagram in Fig.1, and added to the end of the Experimental Setup section: ``A movable beam diverter is placed in the squeezed vacuum beam path before the Faraday isolator. Squeezing can readily be injected or blocked by opening or closing the beam diverter.''



\c{- S1.1: The meaning of T should be clearly explained. Is my understanding
correct that in this analysis, the time series data of length T is divided into
smaller chunks of duration tau? Then periodograms of the chunks are
computed and averaged to estimate the power spectrum. Readers not familiar
with spectral analysis may not understand this process and be confused to
think "isn't f = 1/T?".}

You are correct. $T$ is the total duration of the time series, which is divided into smaller segments. In S1.1, we now state ``to the product of the total duration $T\sim \SI{20}{min}$ of the time series.''.



\c{- S1.1, last paragraph: Is the subtraction of the lines done in frequency
domain?}

Yes. We have clarified ``...remove all the known narrow noise lines in the frequency domain before re-binning.''


\c{- S1.2: The authors state that the time-nonstationarity is due to slow thermal
drifts. Is thermal drift the only cause of the nonstationarity? What about
alignment drift, for example?}

That's a good point. We have added this to the first paragraph of S1.2, ``slow drifts of the interferometer and squeezer (due to e.g. alignment or thermal drifts)," since alignment is indeed a source of slow drift.

The LIGO main interferometer has a dedicated alignment sensing and control system to stabilize alignment drifts, which can be caused by drifts in the thermal state of the interferometer. Therefore, to minimize the impact of thermalization, we took all of our data with the interferometer well-thermalized, i.e. locked stably for over 3.5 hours. %. We waited 3.5 hours every time after locking LIGO and achieving the observing mode. %\textbf{come back to this}



\c{- S1.2, the second last paragraph: I could not follow the logic here. Why a drift
can be reduced by averaging? A slow drift of instrumental condition can cause
monotonical change in noise. Does averaging remove this effect?}

We apologize for the confusion. You are correct that a monotonical change in noise can't be reduced by averaging. 
We have clarified this point in both the main text (Results section, paragraph 5), and re-written the supplement S1.2 (second last paragraph) as follows: ``The time-nonstationarity occurs due to thermal (or alignment) drifts of the interferometer. To control for drifts, we regularly alternate between roughly 20-min segments with and without squeezing. Thermal drifts are on the hour-scale and thus slower than these 20-minute measurement intervals. The inferred quantum noise is estimated between data blocks of [unsqueezed (20-min), squeezed (20-min)] measurements, thus this chopping on/off of squeezing injection suppresses the impact of hour-timescale slow drifts.''

% In Results paragraph 5 of the main text, this reads ``Statistical uncertainties from PSD estimation and non-stationary classical noise~\cite{detcharO3} limit estimates of low-frequency quantum noise. We interleave multiple 20-minute segments of unsqueezed and squeezed measurements to control for time variations of the classical noise. Fig. 2 shows the total detector noise measured with \SI{1}{hour} of combined data segments in the unsqueezed and squeezed configurations. We find that differences in the classical noise between segments (non-stationarity) was comparable to the total uncertainty from one hour of PSD estimation with optimal frequency binning.''
% We can only average out thermal drifts with a time scale smaller than our averaging times. We have revised this sentence by adding a conditional constraint. \textbf{come back to this}


\c{- S2.2, the second paragraph: the last sentence in the paragraph seems
incomplete.}

Thanks for carefully reading our paper. We have revised this sentence to make it more readable. It now reads: ``Enforcing such a set of common parameters helps break certain degeneracies in the model and constrain the parameter space.'' 


\c{- S2.2, the second last paragraph: How does the introduction of an independent
parameter affect the credibility of the fitted model? To me, the use of a free
parameter to fit the data well seems to undermine the legitimacy of the model.}

You are correct. We only allow the parameters known to change to be independent, for example, the squeezing angle and the phase noise. %However, we still need the IFO to OMC mismatch phase to be independent to successfully fit the measured total noise differences (Fig. S2). We don't have an explanation why it is dependent on the squeezing configurations. It is perhaps a limitation of the phenomenological quantum noise model. There could be some systematic error or other physics that the model fails to capture, but we don't know. 
%
However, as you say, we still need the interferometer (IFO) to output mode cleaner (OMC) mismatch phase to be independent to fit the measured total noise differences (Fig. S2). It would be ideal to have no independent/free parameters in the fitted model.

This is likely due to a limitation of our quantum noise models -- which despite numerically capturing much of the complexity of our physical detectors -- is still limited in scope compared to the full detectors. For example, our quantum noise models only account for mode mismatch into the first LG20/02 mode and not higher-order modes, and moreover our models do not account for misalignment or beam astigmatism separately from mode mismatch. Of course such physical non-idealities are always present in physical systems. Therefore, we believe this discrepancy is a sign that our model has room to yet more fully capture all the physics relevant to detector quantum noise. Ongoing work is aimed at understanding and improving model limitations, to more fully account for the real detectors. %\textbf{does this sound ok/less confused} 

% Still, in short, if we do not allow for this single free parameter for different squeezing configurations, the residual model errors remain within 2-4 sigma across the band. 
 




\section*{Review 3}

\c{In my opinion, the article is of significant interest to all physicists. The
manuscript is clearly written. I have no doubt that it is worth publishing in
Science.}


\c{At the same time, I recommend the authors address the following issues in
order to improve the quality of the presentation.}


\c{1. The concept of the SQL was actually first introduced (albeit without using
the term SQL) as early as in 1968 [JETP, Vol. 26, No. 4, p. 831], available at
http://www.jetp.ras.ru/cgi-bin/e/index/e/26/4/p831?a=list. 
See Eqs. (7-12) of that paper.}

Thanks for letting us know. We have replaced the previous Ref. (\textit{8}, Braginsky and Vorontsov, \textit{Sov. Phys. Usp.} \textbf{17}, 644 (1975)) with this earlier one from 1968, which is now Ref. (\textit{2}) in the revised manuscript. We now include this citation to Ref. (\textit{2}) when first introducing the SQL. 



\c{2. The footnote on page 4 is misleading. Refs.(2,7) follow the second definition
of the QND (the one that implies that there is no quantum back action on the
measured observable).}

You are correct. The concept of quantum nondemolition used within the LIGO community (\textit{11}, Kimble \textit{et al}, \textit{PRD} \textbf{65}, 022002 (2001)) is not exactly the same as QND in a broader context. We have moved the citations (previously, \textit{1, 2, 7}) to their appropriate place at the end of the footenote, to clarify this difference.
% We add the footnote in hope to clarify the long-existed confusion and misunderstanding of QND. Citations (1,2,7) are defining QND as the latter, so we move the citation to the right place. 



\c{3. Page 6, paragraph 2. "At frequencies below 100 Hz, measurement back
action dominates...". Actually, as it is mentioned a bit later and also can be
seen from Fig.2, it dominates below ~40Hz.}

Thanks for carefully reading our manuscript. We have fixed this to being 40 Hz. 




\c{4. Section "Experimental Setup". The filter cavity is the key element of the
setup. In my opinion, it would be useful to provide some more details about it,
at least the explicit values of the linewidth and detuning and why these values
were used. Also, in (\textit{11}, Kimble \textit{et al}, \textit{PRD} \textbf{65}, 022002 (2001)), the sequence of two cavities was considered, while
only one is used in this work. This discrepancy should be discussed briefly.}

% That's a good idea. We thought about writing filter cavity in more detail in the experimental setup section, for example, how we can achieve a broadband squeezing with only one filter cavity instead of two as suggested in (\textit{11}, Kimble et al, \textit{PRD} \textbf{65}, 022002 (2001)). The reason we dropped these sentences was that it would be too technical to explain how a single filter cavity can approximate the desired frequency-dependent phase rotation under the assumption that the SQL frequency $\Omega_{\text{SQL}}$ is much less than the detector's signal bandwidth $\gamma_0$. Nonetheless, we should talk about it more explicitly in the supplementary material. \textbf{come back to this}

Thanks for the suggestions. The ``Experimental Setup" section is quite short because we have discussed the experimental details in our recent paper (\textit{13}, Ganapathy \textit{et al}, \textit{PRX} \textbf{13}, 041021 (2023)). The citation of (\textit{11}, Kimble \textit{et al}, \textit{PRD} \textbf{65}, 022002 (2001)) is potentially misleading there because we only used one filter cavity instead of two. Therefore, we removed (\textit{11}) and emphasize (\textit{13}) in the sentence ``As described in our recent experimental work (\textit{13}) ..." to direct readers to our recent wokr with details on the filter cavity's technical implementation.


\c{5. Section "Results" is very long (almost half of the main text of the paper).
Most of it is occupied by the description of the classical noise subtraction
procedure. I agree that this subtraction is an important part of the work, and
it should be presented in some form. However, I would suggest moving the
details to the Supplementary Material, leaving only a short resume in the main
text. Instead, I would welcome some discussion on the sources of the classical
noise and the prospects of their suppression.}

Thanks for the suggestions. We spent quite some space explaining the subtraction process and uncertainties in order to support our observation of sub-SQL quantum noise. %The subtracted quantum noise is verified to be below SQL with 3-$\sigma$ statistical significance. 
We wanted to summarize uncertainties in the main text to justify our error bars, and further describe what limits quantum noise inference in the LIGO detectors. However, we agree the discussion is quite long, so we have shortened the discussion of statistical uncertainties in the fifth paragraph of the ``Results" section (starting with ``Statistical uncertainties from PSD estimation..."). 
% We have moved as many details of the uncertainty analysis to the supplementary material as possible (S1-S3 are dedicated for this purpose). 

Indeed the classical noise is very interesting and worth discussing. We have added a citation to the O3 instrument science paper detailing our understanding of classical noises (see second paragraph of results, ``Accurate estimation of squeezed quantum noise below 100 Hz is complicated by the presence of non-quantum (``classical”) noises that are a factor of 2 higher in amplitude (\textit{20}, Buikema, \textit{et al.}, \textit{PRD} \textbf{102}, 062003 (2020))."

We are currently preparing the instrument science paper for LIGO's Observing Run 4, which will present our updated understanding of classical noises given the increased laser power and addition of frequency-dependent squeezing. This work will also discuss prospects of further reducing classical noise. %Per \textit{Science} guidelines, we cannot cite manuscripts in preparation, and since dominant classical noises are largely similar to O3, we hope the added citation to previous work provides useful context. \textbf{come back to this}






\c{6. At the same time, the sections S4 and S5 are very interesting and, may be,
worth moving to the main text in some form.}

Thanks. We have moved the text from Sections S4 and S5 into the main text (Results section, last 2 paragraphs), and clarified the discussion there. This includes adding Figure S5 back into the main text as Figure 3, which now contains 2 subplots: 3a (moved into main text, the quantum-limited strain noises); and 3b (same as before, quantum noise relative to no squeezing, in decibels). 

The previous Figure S6 has been removed to comply with \textit{Science} editorial guidelines, which do not allow discussion of future work in original research papers. We now only discuss how the current filter cavity is sub-optimal compared to its current theoretical performance, but no longer plot expectations of its designed performance.





\section*{Cross Reviews}

\c{Reviewer: 2}

\c{I read other reviewer's comments and they are all reasonable.}

$\\
\\
$

Besides the revisions following the referee's report, we also noticed a typo in the legend of Fig. 3. The squeezing angle $\phi$ should be -22$^\circ$ and -10$^\circ$ for olive and blue traces. They are corrected in the new Fig. 3a. 

The difference between two documents are attached as follows for your references. Note that the citation numbers quoted in the ``difference" document is not the same as the main text because we've made changes to the bibliography. Please refer to the main text PDF for the correct citation numbers. 

$\\$

Thank you very much,

Wenxuan and Victoria (corresponding authors)

\today

\end{document}


