% Use only LaTeX2e, calling the article.cls class and 12-point type.

\documentclass[12pt]{article}

% Users of the {thebibliography} environment or BibTeX should use the
% scicite.sty package, downloadable from *Science* at
% http://www.sciencemag.org/authors/preparing-manuscripts-using-latex 
% This package should properly format in-text
% reference calls and reference-list numbers.

\usepackage{scicite}

\usepackage{times}

\usepackage{orcidlink}
\usepackage{authblk}
% units
\usepackage[
        range-phrase=--,
        range-units=single,
        retain-explicit-plus=true,
        retain-unity-mantissa=false,
    ]{siunitx}
\usepackage{hyperref}
\usepackage{url}
\usepackage[capitalise]{cleveref}


\newcommand{\K}{\mathcal{K}(\Omega)}
\newcommand{\hsql}{h_{\text{SQL}}(\Omega)}
\newcommand{\arm}{\text{arm}}
\newcommand{\sql}{37}

\renewcommand{\figurename}{Fig.}


% The preamble here sets up a lot of new/revised commands and
% environments.  It's annoying, but please do *not* try to strip these
% out into a separate .sty file (which could lead to the loss of some
% information when we convert the file to other formats).  Instead, keep
% them in the preamble of your main LaTeX source file.


% The following parameters seem to provide a reasonable page setup.

\topmargin 0.0cm
\oddsidemargin 0.2cm
\textwidth 16cm 
\textheight 22cm
\footskip 1.0cm


%The next command sets up an environment for the abstract to your paper.

\newenvironment{sciabstract}{%
\begin{quote} \bf}
{\end{quote}}

\definecolor{clr}{RGB}{196, 22, 29}
\renewcommand{\c}[1]{\textcolor{clr}{#1}}



% Include your paper's title here

\title{Response to reviewer's and editor's comments on \\
``\textbf{LIGO operates with quantum noise below the Standard Quantum Limit}"} 


% Place the author information here.  Please hand-code the contact
% information and notecalls; do *not* use \footnote commands.  Let the
% author contact information appear immediately below the author names
% as shown.  We would also prefer that you don't change the type-size
% settings shown here.

\author
{Wenxuan Jia,$^{1\ast}$   %wenxuan.jia
Victoria Xu,$^{1\ast}$  %victoria.xu
Kevin Kuns,$^{1}$   %kevin.kuns
Masayuki Nakano,$^{2}$  \ \ \ \ \ \ \ \ \ \  \ \ \  \ \ \ \ \ \ \ \ \ \  \ \ \ \ \ \ \ \ \ \    %masayuki.nakano
Lisa Barsotti,$^{1}$   %lisa.barsotti
Matthew Evans,$^{1}$    %matthew.evans
Nergis Mavalvala,$^{1}$  \ \ \ \ \ \ \ \  \ \ \  \ \ \ \ \ \ \ \ \ \  \ \ \ \ \ \ \ \ \ \  \ \  \ \ \ \ \ \ \ \ \ \  \ \ \ \ \ \ \ \ \ \   %nergis.mavalvala
and members of the LIGO Scientific Collaboration,$^\dagger$
\normalsize{\ }\\
\normalsize{$^\ast$Corresponding authors: wenxuanj@mit.edu.}\\
\normalsize{$^\ast$victoriaa.xu@ligo.org.}\\
\normalsize{$^\dagger$Members of the LIGO Scientific Collaboration authors and affiliations are listed at the end of the main text.}
}

% Include the date command, but leave its argument blank.

\date{}



%%%%%%%%%%%%%%%%% END OF PREAMBLE %%%%%%%%%%%%%%%%



\begin{document} 


% Double-space the manuscript.

\baselineskip24pt

% Make the title.

\maketitle 

$
\\$

\noindent Dear Referees,

We are glad that our revisions are satisfactory. Thank you again for reviewing our manuscript.

$\\
$

\noindent Dear Editor,

Thank you very much for carefully editing our manuscript. We have followed your thorough comments on editing the paper. Here are some of the points where we prefer to keep our original writing. The responses to each of your suggested wording (red color text) are listed below. 

$\\$

\section*{Title}
\c{Avoid acronyms in the title. LIGO is specified in the abstract anyway.}

We've changed the title to ``Squeezing the quantum noise of a gravitational-wave detector below the standard quantum limit". The title you proposed is very similar to a paper from 2011 (``A gravitational wave observatory operating beyond the quantum shot-noise limit", \textit{Nature Physics} volume 7, 962–965 (2011)). We would like to avoid the confusion with the previous work. 


\section*{Abstract}

\c{The Heisenberg uncertainty principle dictates that the position and momentum
of an object cannot both be precisely measured, a limitation known as the standard quantum limit (SQL).}

Following your suggestions, we have edited this to be ``The Heisenberg uncertainty principle dictates that the position and momentum
of an object cannot be simultaneously measured with arbitrary precision, giving rise to an apparent limitation known as the standard quantum limit". We hope to keep the word ``apparently'' to make clear this is not a physical limitation and that 
% We prefer our original introduction to SQL, namely ``The Heisenberg uncertainty principle dictates that the position and momentum
% of an object cannot be simultaneously measured with arbitrary precision, giving rise to an apparent limitation known as the standard quantum limit". 
the SQL is not equivalent to the Heisenberg uncertainty principle (otherwise, it would violate quantum mechanics to surpass the SQL). 


\section*{Introduction}

% \c{The Heisenberg uncertainty principle of quantum mechanics states that the product of the measurement...}

% We'd like to write it as ``One of the most profound consequences of quantum mechanics is the Heisenberg uncertainty
% principle, which states that the product of the measurement...". We think that this is a more appealing opening to readers. %We understand that it is a rather subjective sentence, but the Heisenberg uncertainty principle is not our work so we are allowed to call it as ``one of the most profound consequence of quantum mechanics". 


\c{The addition of
a detuned Fabry-Perot filter cavity would impose a frequency-dependent phase shift upon the squeezed vacuum states reflected within the cavity, allowing the SQL to be circumvented over a broader frequency range.}

We changed the verb to ``impose'' as suggested. We want to clarify that it is the cavity's resonant phase shift in reflection that provides the frequency-dependent phase shift for the squeezed light, and not the off-resonant reflection. 
% decided to write this as ``The addition of a
% detuned Fabry-Perot filter cavity would impose a frequency-dependent phase shift upon the
% squeezed vacuum states reflected from it, allowing the SQL to be surpassed over a broader
% frequency range (11)." It is misleading to say that the squeezed vacuum is reflected ``within" the cavity as if it's a single reflection. The cavity build-up of all fields causes the phase shift. 


\c{Footnotes are not allowed in the Research Article format. Either integrate this into the text, move it to the supplement, or delete it.}

We have moved the footnote to the citations (reference 9). In the comment, we also reduced the number of citations, and updated the cited work to a review article that covers much of the modern research into quantum nondemolition measurements and experiments; this review is cited in reference 10. 


\section*{Simplified quantum noise model}

\c{the subscript SQL indicates the limit expected from Heisenberg's uncertainty principle.}

The SQL is not equivalent to the Heisenberg uncertainty principle; it is simply the result of the Heisenberg uncertainty principle for the case of ``standard laser light'' which probes the mirror positions with uncorrelated photons. The subscript SQL was explained in the sentence preceding the equation, and prior in the introduction, so are keeping the subscript in the equation. 




% \c{In the case of LIGO, it is used to reduce the imprecision noise while increasing the quantum back action noise.}

% Thanks for this comment, indeed this was confusing. We re-wrote this according to your suggestion as, ``In the previous observing run, frequency-independent
% squeezing was injected into LIGO to reduce the imprecision noise while increasing
% the quantum back action noise". It is a good place to introduce frequency-independent squeezing after introducing squeezed vacuum. 


\c{$e^{-2r}$ is an undefined variable}

In the quantum optics community, the quantity $e^{-2r}$ is traditionally used and referred to as the ``squeeze factor,'' while alone the variable $r$ is typically not defined. We defer to the standards in this field for the definition.
% We didn't define $r$ but defined $e^{-2r}$ as the factor of squeezing. It is clear that it reaches zero asymptotically so we can never squeeze the quantum noise to zero in principle. The other way to do this is to define $r$ in additional to $e^{-2r}$, but we think it's redundant. 


\c{Frequency-dependent squeezed states, in which the squeeze angle varies only as a function of frequency
$\phi \rightarrow \phi(\Omega)$ }

We have written this sentence to be: ``Frequency-dependent squeezed states (FDSQZ), in which the input squeeze angle is varied
as a function of frequency $\phi \rightarrow \phi(\Omega)$", to define the equation subscript ``FDSQZ.'' The filter cavity enables the input squeezing angle to be frequency-dependent, so $\phi$ is varied. This is more specific than writing a squeezing angle that is ``only" a function of frequency, as that phrasing does not specify from where the squeeze angle's frequency-dependence arises.


\section*{Observation of quantum noise below the SQL}

\c{This is presented in a confusing order, by stating the results first before explaining how you determined them. I've moved this passage down to a more appropriate location.}

We believe a more clear order is actually Figure 2 (main results) then Figure 3 (characterization of limitations), as originally presented. 
% We would like to present our main result at the early stage of the paper, so it will invoke readers' curiosity on how we reach our main result. 
This first section presents the main result, then explains how we infer the quantum noise from measurements of the total noise, as presented in Fig. 2 (Quantum-enhanced sensitivity of the LIGO L1 interferometer). 

After showing Fig. 2, we then show the reader about the comparison between squeezing with and without a filter cavity in Fig. 3. So, Fig. 3 further explains the physics behind Fig. 2, and is used to reveal and discuss the limitations in the conclusions. 
% Fig. 3 has a lot of information that is partially shown in Fig. 2. So it is easier for readers to parse through Fig.3 after understanding Fig. 2. Otherwise, Fig.3 is rather overwhelming. 


\section*{Figures}

\c{Fig. 2: Would `Hz$^{-0.5}$' be clearer notation here?}

The convention in our field uses $\sqrt{\text{Hz}}$, so we will stick to this notation. 



\section*{Acknowledgments}

\c{Are there no other grants to individual authors that need to be declared here?} 
% If any are missing, add them using phrasing like this:<author initials> was supported by organisation under grant number}

Discussing with collaboration leadership, we believe we have covered all the grants appropriate for the LIGO observatories.


\section*{Author list}

\c{As discussed via email, it would save substantial print space if you place this list at the start of the (online) supplement, rather than at the end of the (print) main text. They will still be counted as authors by bibliographic databases. }

We would prefer to list all author names in the main text, which is a standard practice for our collaboration to credit all authors. % by listing their names in the main text. 


\c{You can include first names if you wish, which would also make this list consistent with the main text.}

Since the long author list takes space in the main text, we will keep their names short by only showing their first initials. 


\c{Affiliation 29: If these are two different departments they should be split into two affiliation numbers (like 3 \& 14 are two departments at Caltech)}

The affiliation 29 is actually one department, and it has been updated.




\section*{Supplementary materials}

\c{Incomplete title page. You don't have to use the Word template if you prefer LaTeX, but you must ensure the first page of the supplement has the same information as the template. See the link on the checklist.}

The updated supplementary materials document now has the proper cover page and list of contents.



\c{General comment: The supplement can only contain material that directly supports statements made in the main text or that is required to reproduce the results. It cannot be used to present additional data that weren't mentioned there, tangential discussion, general background etc.}

We have reorganized the supplementary material according to these two categories. Most of the content is methodology; all but the last section are now part of the Materials and Methods. 

The last section presents the total uncertainty budget to support our arguments about the statistical and systematic uncertainties, described in the main text. It has been categorized as supplementary text. 

We have also removed the discussion of the quantum noise sub-budget, as it is additional data not mentioned in the main text. In doing so, we removed a subsection of the supplement, and removed the previous Fig. S5.

\c{I'm surprised you don't cite any additional references in the supplement, which almost every Science paper does. If you add any references to the supplement, insert them as references 33, 34 etc. at the end of the list *in the main text*.}

Following your suggestions on citing the GWINC software, it is now cited as Reference 32, and we now have references that only belong to the supplementary materials. We have updated the references list accordingly. 

\c{All multi-panel figures must label the panels A, B, C etc. and refer to them by letter in the caption. See the checklist.}

We've added the panel labels to figures with more than one panel. 

\c{Many of these traces cannot be distinguished by colour blind readers (~5\% of the population). Red vs green is particularly problematic. Please add different line styles, plotting symbols, or pick better colours.}

We have changed the marker styles for the individual traces in Figure S2, to help distinguish between traces.

\c{Very inadequate caption. Explain both panels, the meaning of all symbols, line styles, error bars etc.}

We have carefully written the captions of all supplementary figures according to your guidelines. We have corrected all figures as recommended.

$\\$

The difference between two documents are attached as follows for your references. Note that the citation numbers quoted in the ``difference" document is not the same as the main text because we have made changes to the bibliography. Please refer to the main text PDF for the correct citation numbers. 

$\\$

Thank you very much,

Wenxuan and Victoria (corresponding authors)

\today

\end{document}


