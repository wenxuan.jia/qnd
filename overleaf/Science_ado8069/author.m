fid = fopen('./author.txt');
excel = 'sub_SQL.xls';

col1 = {
    'FirstName';
    'Wenxuan';
    'Victoria';
    'Kevin';
    'Masayuki';
    'Lisa';
    'Matthew';
    'Nergis';
    };

col2 = {
    'LastName';
    'Jia';
    'Xu';
    'Kuns';
    'Nakano';
    'Barsotti';
    'Evans';
    'Mavalvala';
    };

col3 = {
    'Email';
    'wenxuanj@mit.edu';
    'victoriaa.xu@ligo.org';
    'kkuns@mit.edu';
    'masayuki@caltech.edu';
    'lisabar@ligo.mit.edu';
    'm3v4n5@mit.edu';
    'nergis@mit.edu';
    };



rank = 0;
tline = fgetl(fid);
while tline~=-1
    rank = rank + 1;

    if rank <= 7
        tline = fgetl(fid);
        continue
    end
    % while (isstrprop(tline(id),'digit'))
    %     id = id + 1;
    % end

    loc = strfind(tline, '%');
    loc = loc(end);

    name = tline(loc+1:end);

    comma = strfind(name, '.');

    first = name(1:comma-1);
    last = name(comma+1:end);

    email = [name '@ligo.org'];

    % capitalize first letter
    first(1) = upper(first(1));
    last(1) = upper(last(1));

    % if rank > 7
        % shorten first name
        first = [first(1) '.'];
    % end
    
    col1 = [col1; first];
    col2 = [col2; last];
    col3 = [col3; email];
    
    
    tline = fgetl(fid);
end
fclose(fid);

T = table(col1, col2, col3);

writetable(T,excel,'WriteVariableNames',false,'Range',['A1:C' num2str(length(col1))]);