# from scipy.optimize import minimize


# import gwinc
# from gwinc import load_budget
# from gwinc import Struct
# from gwinc.noise.quantum import shotrad_debug
# from wield.pytest import fpath_join, tpath_join, dprint
# from wield.bunch import Bunch

import numpy as np
import scipy as sp
import scipy.signal
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec

import os
from copy import deepcopy
import csv
import glob

from wield.utilities.file_io import load, save


class DARM:
    def __init__(self, filename=None, deltaf=2**(-4)):
        self.filename = filename
        self.fs = 2**14
        self.deltaf = deltaf
        # self.err = []
        # self.G_CAL = lambda f: (1-1j*f/30)**6/(1-1j*f/0.3)**6 ## CAL-DELTAL calibration
        self.L = 3994.5 # arm length [m]
        self.knownDARMLines = [
            (19, 19.1875), 
            (19.5, 19.8125),
            (20.0625, 20.4375), 
            (27, 28), # bounce mode (I forgot which SUS)
            (40.5, 41.25), 
            (45, 45.5),
            (59.25, 60.75), # Power line
            (119.5, 120.5), # Power line harmonics
            (179.5, 180.5), # Power line harmonics?
            (299.5, 300.5), # Power line harmonics?
            (305.75, 306.75),
            (307, 308), 
            (314.5, 315.75),
            (504.5, 520.5), # 1st-order violin modes of four test mass
            (612, 613), 
            (614.5, 615.5),
            (629.5, 631),
            (841.5, 842.5),
            (918.5, 919.5),
            (945, 946),
            (959.5, 961.5),
            (995, 997.5), # 2nd-order violin
            (1003, 1006), # 2nd-order violin
            (1007, 1019), # 2nd-order violin
            (1022, 1026), # 2nd-order violin
            (1160, 1170),
            (1187, 1189),
            (1308, 1309),
            (1374.5, 1376),
            (1464, 1466),
            (1483, 1485), # 3rd-order violin
            (1489, 1503), # 3rd-order violin
            (1509, 1514), # 3rd-order violin
            (1745, 1755),
            (1936, 1939),
            (1952, 1974),
            (1984, 1990),
            (1993, 1995),
            (2095, 2105),
            (2270, 2280),
            (2295, 2305),
            (2320, 2330),
            (2385, 2388),
            (2413, 2437),
            (2444, 2465),
            (2525, 2555),
            (2837, 2841),
            (2868, 2892),
            (2900, 2932),
            (3173, 3177),
            (3288, 3292),
            (3318, 3325),
            (3328, 3362),
            (3368, 3371),
            (3381, 3396),
            (3404, 3410),
            (3446, 3450),
            (3610, 3620),
            (3752, 3762),
            (3797, 3815),
            (3819, 3822),
            (3828.5, 3835),
            (3845, 3848) # Classical noise dominates above 4 kHz
        ]
        self.knownCALLines = [
            (33.25, 34.5), # 33.93 Hz calibration line
            (53.5, 55), # 54.17 Hz calibration line
            (77.5, 79), # 78.23 Hz calibration line
            (101, 102), # 101.63 Hz calibration line
            (282, 285), # 283.41 Hz calibration line
            (434, 436), # 434.9 Hz calibration line
            (1082.5, 1084), # 1083.1 Hz calibration line
        ]
        self.knownScatLines = [
            (80, 86),
            (2500, 2510)
        ]

        if filename is not None:
            if filename[-2:] == 'h5':
                self.loadDARM(filename)
        
    
    def __str__(self):
        result = 'This object contains the following attributes: '
        # print(result)
        attr = self.__dir__()
        for j in range(len(attr)):
            if '__' not in attr[j]:
                # print(prop[j])
                result = result + attr[j] + ', '
        return result

    def loadDARM(self, filename):
        data = load(filename)
        for key in data.keys():
            exec('self.' + key + ' = data[key]')
    
    def loadRawData(self, filename):
        if isinstance(filename, list):
            time_series = []
            for name in filename:
                temp = self.loadOneRawData(name, col=0)
                time_series = np.concatenate((time_series, temp), axis=0)
        else:
            time_series = self.loadOneRawData(filename, col=0)
        return time_series
    
    def loadOneRawData(self, filename, col=0):
        assert filename != None
        if filename[-3:] == 'mat':
            time_series = load(filename)['raw'][col]
        elif filename[-3:] == 'txt':
            time_series = None
        elif filename[-4:] == 'hdf5':
            time_series = load(filename)['H1:GDS-CALIB_STRAIN_NOLINES']
        return time_series
    
    def loadRawPDData(self, filename):
        if isinstance(filename, list):
            time_series1 = []
            time_series2 = []
            for name in filename:
                temp1 = self.loadOneRawData(name, col=1)
                time_series1 = np.concatenate((time_series1, temp1), axis=0)
                temp2 = self.loadOneRawData(name, col=2)
                time_series2 = np.concatenate((time_series2, temp2), axis=0)
        else:
            time_series1 = self.loadOneRawData(filename, col=1)
            time_series2 = self.loadOneRawData(filename, col=2)
        return (time_series1, time_series2)

    def save(self, filename):
        attr = self.__dir__()
        data = {}
        for key in attr:
            value = None
            if '__' in key:
                break
            exec('data[key] = self.' + key)
        # print(data)
        save(filename, data)
        
    def db(self, val):
        return 20*np.log10(val)
    
    def db2mag(self, val):
        return 10**(val/20)


    def welch(self, deltaf=None, average='median'):
        time_series = self.loadRawData(self.filename)
        if deltaf is not None:
            self.deltaf = deltaf
        nperseg = self.fs/self.deltaf
        f,S = scipy.signal.welch(time_series,self.fs,window='hann',nperseg=nperseg,noverlap=0.5*nperseg,average=average)
        f = f[1:]
        S = S[1:]
        self.f = f
        self.S = S
        self.relerrD_n = 1/np.sqrt(len(time_series)/self.fs*self.deltaf)*np.ones(f.shape) # relative error of S from PSD estimation
        self.relerrD_p = self.relerrD_n

    def welch_null(self, deltaf=None, average='median'):
        (time_series1, time_series2) = self.loadRawPDData(self.filename)
        time_series = (time_series1 - time_series2)/2 # L1 xcorr pipeline has a factor of 2 embedded
        if deltaf is not None:
            self.deltaf = deltaf
        nperseg = self.fs/self.deltaf
        f,S = scipy.signal.welch(time_series,self.fs,window='hann',nperseg=nperseg,noverlap=0.5*nperseg,average=average)
        f = f[1:]
        S = S[1:]
        self.f = f
        self.S_null = S*(abs(self.G_CAL(f))/self.L)**2

    def welch_sum(self, deltaf=None, average='median'):
        (time_series1, time_series2) = self.loadRawPDData(self.filename)
        time_series = (time_series1 + time_series2)/2 # L1 xcorr pipeline has a factor of 2 embedded
        if deltaf is not None:
            self.deltaf = deltaf
        nperseg = self.fs/self.deltaf
        f,S = scipy.signal.welch(time_series,self.fs,window='hann',nperseg=nperseg,noverlap=0.5*nperseg,average=average)
        f = f[1:]
        S = S[1:]
        self.f = f
        self.S_sum = S*(abs(self.G_CAL(f))/self.L)**2

    def xcorr(self, deltaf=None, average='mean'):
        (time_series1, time_series2) = self.loadRawPDData(self.filename)
        if deltaf is not None:
            self.deltaf = deltaf
        nperseg = self.fs/self.deltaf
        f,S = scipy.signal.csd(time_series1,time_series2,self.fs,window='hann',nperseg=nperseg,noverlap=0.5*nperseg,average=average)
        f = f[1:]
        S = S[1:]
        self.f = f
        self.S_xcorr = S*(abs(self.G_CAL(f))/self.L)**2  # L1 xcorr pipeline has a factor of 2 embedded

    def G_CAL(self, f):
        return (1-1j*f/30)**6/(1-1j*f/0.3)**6 ## CAL-DELTAL calibration
        
    # kernelSize = 5 Hz
    def removeLines(self, method='DARM', kernelSize=5):
        if hasattr(self, 'err_n') is False and hasattr(self, 'err_p') is False:
            self.setZeroErr()
        f = self.f
        S = self.S
        deltaf = self.deltaf
        hardcode = False
        if method == 'medfilt':
            # kernelSize = 5 # Hz
            flt = int(kernelSize/deltaf)
            flt = flt + flt % 2 - 1
            S_filt = scipy.signal.medfilt(S, flt)
        elif method == 'DARM':
            hardcode = True
            lines = self.knownDARMLines
        elif method == 'CAL':
            hardcode = True
            lines = self.knownCALLines
        elif method == 'scatter':
            hardcode = True
            lines = self.knownScatLines
        
        if hardcode:
            S_filt = deepcopy(S)
            err_n_filt = deepcopy(self.err_n)
            err_p_filt = deepcopy(self.err_p)
            for flow, fhigh in lines:
                # print((f >= flow) & (f <= fhigh))
                S_filt[(f >= flow) & (f <= fhigh)] = np.nan
                err_n_filt[(f >= flow) & (f <= fhigh)] = np.nan
                err_p_filt[(f >= flow) & (f <= fhigh)] = np.nan
            self.err_n_unfilt = self.err_n
            self.err_p_unfilt = self.err_p
            self.err_n = err_n_filt
            self.err_p = err_p_filt
            
        self.S_unfilt = S
        self.S = S_filt
            
    def setZeroErr(self):
        f = self.f
        self.err_n = np.zeros(f.shape)
        self.relerr_n = np.zeros(f.shape)
        self.err_p = np.zeros(f.shape)
        self.relerr_p = np.zeros(f.shape)
        
    def calcErr(self, relerr_n=None, relerr_p=None):
        if relerr_n is not None:
            self.relerr_n = relerr_n
        else:
            if relerr_p is not None:
                self.relerr_n = relerr_p
                
        if relerr_p is not None:
            self.relerr_p = relerr_p
        else:
            if relerr_n is not None:
                self.relerr_p = relerr_n
                
        self.err_n = self.S*self.relerr_n
        self.err_p = self.S*self.relerr_p
        
        
    # assign='left': Each energy sum E[i] is assigned to fnew[i]. Snew[-1] = []
    # assign='right': Each energy sum E[i] is assigned to fnew[i+1]. Snew[0] = []
    # integrate='left': Each energy sum E[i] uses S[idx[i]] to S[idx[i+1]-1]
    # integrate='right': Each energy sum E[i] uses S[idx[i]+1] to S[idx[i+1]]. Default because searchsorted returns f[idx[i]] > fnew[i]
    def rebin_any(self, fnew, assign='left', integrate='right'): 
        if hasattr(self, 'err_n') is False and hasattr(self, 'err_p') is False:
            self.setZeroErr()
        f = self.f
        S = self.S
        Snew = np.zeros(fnew.shape)
        err_n = self.err_n
        err_p = self.err_p
        errnew_n = np.zeros(fnew.shape)
        errnew_p = np.zeros(fnew.shape)
        relerr_n = self.relerr_n
        relerr_p = self.relerr_p
        relerrnew_n = np.zeros(fnew.shape)
        relerrnew_p = np.zeros(fnew.shape)
        assert f[0] <= fnew[0]
        assert f[-1] >= fnew[-1]
        idx = np.searchsorted(f, fnew)

        ilist = np.arange(0,len(fnew)-1)
        if assign == 'left':
            Sid = ilist
            # Snew[-1] = S[idx[-1]]
        elif assign == 'right':
            Sid = ilist + 1
            # Snew[0] = S[0]
        elif assign != 'left':
            raise Warning("'assign' parameter has to be either 'left' or 'right'")

        if integrate == 'right':
            idx = idx + 1
        elif integrate != 'left':
            raise Warning("'integrate' parameter has to be either 'left' or 'right'")

        for i in ilist:
        # for i in [0]:
            deltaf = f[idx[i+1]] - f[idx[i]]
            if deltaf == 0:
                raise Warning("Only 1 point in new bin. Try increasing bin width of the new bin or increasing resolution of old bin")
            S_oldbin = S[idx[i]:idx[i+1]] # Note it's not S[idx[i]] to S[idx[i+1]-1] because Python [a:b] doesn't include b. 
            compensateNanScale = len(S_oldbin)/np.sum(~np.isnan(S_oldbin))
            E = compensateNanScale*self.deltaf*np.nansum(S_oldbin) 
            deltafnew = fnew[i+1] - fnew[i]
            S_new = E/deltafnew
            Snew[Sid[i]] = S_new
            err_n_oldbin = err_n[idx[i]:idx[i+1]]
            err_p_oldbin = err_p[idx[i]:idx[i+1]]
            relerr_n_new = np.sqrt(compensateNanScale*np.nansum(err_n_oldbin**2))/(compensateNanScale*np.nansum(S_oldbin))
            relerr_p_new = np.sqrt(compensateNanScale*np.nansum(err_p_oldbin**2))/(compensateNanScale*np.nansum(S_oldbin))
            relerrnew_n[Sid[i]] = relerr_n_new
            relerrnew_p[Sid[i]] = relerr_p_new
            errnew_n[Sid[i]] = relerr_n_new*S_new
            errnew_p[Sid[i]] = relerr_p_new*S_new
            # print((f[idx[i]], f[idx[i+1]-1], deltaf, S_oldbin, S_new, err_oldbin, err_new))
        
        if assign == 'left':
            fnew = fnew[:-1]
            Snew = Snew[:-1]
            errnew_n = errnew_n[:-1]
            errnew_p = errnew_p[:-1]
            relerrnew_n = relerrnew_n[:-1]
            relerrnew_p = relerrnew_p[:-1]
        elif assign == 'right':
            fnew = fnew[1:]
            Snew = Snew[1:]
            errnew_n = errnew_n[1:]
            errnew_p = errnew_p[1:]
            relerrnew_n = relerrnew_n[1:]
            relerrnew_p = relerrnew_p[1:]
        
        self.f_lin = f
        self.f = fnew
        self.S_lin = S
        self.S = Snew
        self.err_n_lin = err_n
        self.err_p_lin = err_p
        self.err_n = errnew_n
        self.err_p = errnew_p
        self.relerr_n_lin = relerr_n
        self.relerr_p_lin = relerr_p
        self.relerr_n = relerrnew_n
        self.relerr_p = relerrnew_p



    # The new frequency vector fnew has to be uniformly log-scaled
    def rebin_log(self, fnew): 
        if hasattr(self, 'err_n') is False and hasattr(self, 'err_p') is False:
            self.setZeroErr()

        freq_db = self.db(fnew)
        delta_db = freq_db[1] - freq_db[0]
        fnewbin = self.db2mag(np.append(freq_db, (freq_db[-1] + delta_db)) - delta_db/2) # new frequency bin borders

        f = self.f
        fbin = f - self.deltaf/2 # integrate=left in this case

        S = self.S
        Snew = np.zeros(fnew.shape)
        err_n = self.err_n
        err_p = self.err_p
        errnew_n = np.zeros(fnew.shape)
        errnew_p = np.zeros(fnew.shape)
        relerr_n = self.relerr_n
        relerr_p = self.relerr_p
        relerrnew_n = np.zeros(fnew.shape)
        relerrnew_p = np.zeros(fnew.shape)

        assert f[0] <= fnew[0]
        assert f[-1] >= fnew[-1]
        idx = np.searchsorted(fbin, fnewbin)

        ilist = np.arange(0,len(fnew))
        for i in ilist:
        # for i in [0]:
            deltaf = fbin[idx[i+1]] - fbin[idx[i]]
            if deltaf == 0:
                raise Warning("Only 1 point in new bin. Try increasing bin width of the new bin or increase resolution of old bin")
            S_oldbin = S[idx[i]:idx[i+1]] # Note it's not S[idx[i]] to S[idx[i+1]-1] because Python [a:b] doesn't include b. 
            err_n_oldbin = err_n[idx[i]:idx[i+1]]
            err_p_oldbin = err_p[idx[i]:idx[i+1]]
            
            # if removeLines is not None:
            #     if removeLines == 'std':
            #         median = np.median(S_oldbin)
            #         std = np.std(S_oldbin)
            #         for j in range(len(S_oldbin)):
            #             if abs(S_oldbin[j] - median) > factor*std:
            #                 S_oldbin[j] = np.nan
            #                 err_oldbin[j] = np.nan
            
            
            compensateNanScale = len(S_oldbin)/np.sum(~np.isnan(S_oldbin))
            E = compensateNanScale*self.deltaf*np.nansum(S_oldbin)
            deltafnew = fnewbin[i+1] - fnewbin[i]
            S_new = E/deltafnew
            Snew[i] = S_new
            
            
            relerr_n_new = np.sqrt(compensateNanScale*np.nansum(err_n_oldbin**2))/(compensateNanScale*np.nansum(S_oldbin))
            relerr_p_new = np.sqrt(compensateNanScale*np.nansum(err_p_oldbin**2))/(compensateNanScale*np.nansum(S_oldbin))
            relerrnew_n[i] = relerr_n_new
            relerrnew_p[i] = relerr_p_new
            errnew_n[i] = relerr_n_new*S_new
            errnew_p[i] = relerr_p_new*S_new
            # print((fbin[idx[i]], fbin[idx[i+1]], deltaf, f[idx[i]], f[idx[i+1]], fnewbin[i], fnewbin[i+1], deltafnew))

        self.f_lin = f
        self.f = fnew
        self.S_lin = S
        self.S = Snew
        self.err_n_lin = err_n
        self.err_p_lin = err_p
        self.err_n = errnew_n
        self.err_p = errnew_p
        self.relerr_n_lin = relerr_n
        self.relerr_p_lin = relerr_p
        self.relerr_n = relerrnew_n
        self.relerr_p = relerrnew_p

    
    def __add__(self, other):
        if hasattr(self, 'err_n') is False and hasattr(self, 'err_p') is False:
            self.setZeroErr()
        if hasattr(other, 'err_n') is False and hasattr(other, 'err_p') is False:
            other.setZeroErr()
        assert all(self.f == other.f)
        result = DARM()
        result.deltaf = self.deltaf
        
        result.f = self.f
        result.S = self.S + other.S
        result.err_n = np.sqrt( (self.err_n**2 + other.err_n**2) )
        result.err_p = np.sqrt( (self.err_p**2 + other.err_p**2) )
        result.relerr_n = np.sqrt( (self.err_n**2 + other.err_n**2)/result.S**2 )
        result.relerr_p = np.sqrt( (self.err_p**2 + other.err_p**2)/result.S**2 )
        return result
    
    def __sub__(self, other):
        if hasattr(self, 'err_n') is False and hasattr(self, 'err_p') is False:
            self.setZeroErr()
        if hasattr(other, 'err_n') is False and hasattr(other, 'err_p') is False:
            other.setZeroErr()
        assert all(self.f == other.f)
        result = DARM()
        result.deltaf = self.deltaf
        
        result.f = self.f
        result.S = self.S - other.S
        result.err_n = np.sqrt( (self.err_n**2 + other.err_n**2) )
        result.err_p = np.sqrt( (self.err_p**2 + other.err_p**2) )
        result.relerr_n = np.sqrt( (self.err_n**2 + other.err_n**2)/result.S**2 )
        result.relerr_p = np.sqrt( (self.err_p**2 + other.err_p**2)/result.S**2 )
        return result

    def plotASDAllErr(self):
        fig = plt.figure(figsize=(10, 18));
        gs = gridspec.GridSpec(3, 1, hspace=0.05);
        ax1 = fig.add_subplot(gs[0]);
        ax2 = fig.add_subplot(gs[1], sharex=ax1);
        ax3 = fig.add_subplot(gs[2], sharex=ax1);
        
        zeros = self.relerr_n*0

        ax1.loglog(self.f, self.relerrD_p/2, label=r'$\delta D$')
        ax1.loglog(self.f, self.relerrN_p/2, label=r'$\delta N$')
        ax1.loglog(self.f, self.relerrG_p/2, label=r'$\delta G$')
        # ax1.loglog(self.f, self.relerr_M, label='$\delta M$')
        ax1.loglog(self.f, self.relerr_p/2, label='Sum')
        ax1.fill_between(self.f, self.relerr_p/2, zeros, where = self.relerr_p/2>0, alpha = 0.25, interpolate=True)
        
        ax1.grid(which='both')
        ax1.set_yticks([0.001, 0.01, 0.1, 1], ['0.1%', '1%', '10%', '100%'])
        ax1.set_xlim(20, 3000); ax1.set_ylim(0.001, 1)
        ax1.set_ylabel('Positive relative uncertainty'); 
        ax1.legend()
        
        ax2.loglog(self.f, self.relerrD_n/2, label=r'$\delta D$')
        ax2.loglog(self.f, self.relerrN_n/2, label=r'$\delta N$')
        ax2.loglog(self.f, self.relerrG_n/2, label=r'$\delta G$')
        ax2.loglog(self.f, self.relerr_n/2, label='Sum')
        ax2.fill_between(self.f, self.relerr_n/2, zeros, where = self.relerr_n/2>0, alpha = 0.25, interpolate=True)
        
        # ax2.xlabel('Frequency [Hz]'); 
        ax2.set_ylabel('Negative relative uncertainty'); 
        # ax2.xscale("log")
        # ax2.yscale(yscale)
        # ax = ax2.gca()
        # ax.yaxis.set_minor_formatter('{x:.1f}')
        # ax.yaxis.set_major_formatter('{x:.1f}')
        ax2.grid(which='both')
        # ax2.legend()
        # ax2.title(titles[j])
        ax2.set_yticks([0.001, 0.01, 0.1, 1], ['-0.1%', '-1%', '-10%', '-100%'])
        ax2.set_xlim(20, 4000); ax2.set_ylim(0.001, 1)
        # plt.gca().invert_yaxis()
        ax2.invert_yaxis()
        
        
        # plt.subplot(3,1,3)
        # ax = plt.figure(figsize=(7.2, 4.2),dpi = 300)
        ax3.errorbar(self.f, np.sqrt(self.S), np.sqrt(self.S)*[self.relerr_n, self.relerr_p]/2, ecolor=[10/255,160/255,10/255], marker='o', markerfacecolor=[10/255,160/255,10/255], alpha=0.8,
                     ms=4.0,ls='-',lw=0.5,c=[10/255,160/255,10/255],elinewidth=1.5,capsize=0,mew=0,zorder=20,label=r'Inferred quantum noise with squeezing injected at $\phi=$35\xb0')
        #ax3.title('Spectrum')
        # ax3.loglog(fbin,darm2_10,c=[100/255,100/255,100/255],label = 'Total noise of interferometer with unsqueezed vacuum state',linewidth = 0.7,zorder=10)
        # ax3.loglog(fbin,darm2_11,c=[140/255,75/255,20/255],label = 'Total noise with squeezing injected at $\phi=$35\xb0',linewidth = 0.7,zorder=3)
        # ax3.loglog(fbin,qmns_nosqz,c = [26/255,115/255,232/255],lw = 0.7,label='Quantum noise model with unsqueezed vacuum state',zorder=15)
        # ax3.loglog(fbin,techns,c = 'lightgray', lw = 0.7,label='Classical noise contribution to total noise of interferometer', zorder = 5) 
        
        # SQL = np.sqrt(scc.hbar/(2*10))/(scc.pi*fbin)
        # ax3.loglog(fbin,SQL,c=[200/255,10/255,10/255],label='Standard Quantum Limit',linestyle='--',linewidth = 0.7, zorder = 5)
        
        
        ax3.set_ylabel(r'DARM $\mathrm{[1/\sqrt{Hz}]}$')
        ax3.legend(bbox_to_anchor=(0.426, 0.492, 0.32, 0.4), 
                facecolor=[246/255,246/255,250/255],framealpha = 1,fancybox = 'True',
                  labelspacing= 0.5).set_zorder(50)
        ax3.set_xlim(20, 4000)
        # ax3.xticks([20,50,100,500,1000,3000], ('20', '50','100', '500','1000','3000'))
        # ax3.yticks([0.8e-20,1e-20,3e-20,5e-20,1e-19,5e-19], ('0.8','1','3','5','10','50'))
        # ax.text(0.115, 0.887,r'$\mathrm{\times10^{-20}}$', fontsize=7)
        # ax3.set_ylim(8e-21, 5e-19)
        # ax3.tick_params(labelsize=7, width=0.5)
        # ax3.xscale('log')
        ax3.set_yscale('log')
        
        # ax3.grid(True, which='both', axis='both', linestyle='-.',linewidth=0.5,color = [207/255,207/255,207/255])
        ax3.grid(which='both')
        
        
        # fig = plt.gcf()
        # fig.set_size_inches(10, 18)
        # fig.suptitle(title, y=0.93, fontsize=16)
        plt.setp(ax1.get_xticklabels(), visible=False);
        plt.setp(ax2.get_xticklabels(), visible=False);
        plt.xlabel('Frequency [Hz]')
        # plt.savefig('./Sup_Fig_1_allerr_dur_10.pdf')
        # plt.show()


    # def setErr_calib(self, freq, deltaG_m, deltaG_p=None):
    #     if deltaG_p is None:
    #         deltaG_p = deltaG_m
    #     self.detlaG_m = np.interp(self.f, freq, deltaG_m)
    #     self.detlaG_p = np.interp(self.f, freq, deltaG_p)

    def plotNegativePSD(self, ax1=None, ax2=None, xlim=(20,4000), ylim=(1e-48, 1e-43), f=None, S=None, label=None):
        if ax1 is None and ax2 is None:
            fig = plt.figure(figsize=(6, 8));
            gs = gridspec.GridSpec(3, 1, hspace=0.08);
            ax1 = fig.add_subplot(gs[0]);
            ax2 = fig.add_subplot(gs[1], sharex=ax1);

        plotError = True
        # plotError = False
        if hasattr(self, 'err_n') is False and hasattr(self, 'err_p') is False:
            self.setZeroErr()
            plotError = False
            
        nullval = np.nan
        
        S_p = deepcopy(self.S)
        S_p[S_p < 0] = nullval
        err_p = deepcopy(self.err_p)
        err_n = deepcopy(self.err_n)
        err_p[S_p < 0] = 0
        err_n[S_p < 0] = 0
        if plotError:
            ax1.errorbar(self.f, S_p, [err_n, err_p], marker='o', ms=4.0, ls='-', lw=0, alpha=0.8, elinewidth=1.5, capsize=0, mew=0, zorder=20,
                        label=label)
                         # markerfacecolor=[10/255,160/255,10/255], ecolor=[10/255,160/255,10/255], c=[10/255,160/255,10/255])
        else:
            ax1.loglog(self.f, S_p, label=label)
        if f is not None:
            temp = deepcopy(S)
            temp[S < 0] = nullval
            ax1.loglog(f, temp, color=ax1.lines[-1].get_color()) # color of previous plot
        
        ax1.grid(which='both')
        # ax1.set_yticks([0.001, 0.01, 0.1, 1], ['0.1%', '1%', '10%', '100%'])
        ax1.set_xlim(xlim); ax1.set_ylim(ylim)
        # ax1.set_ylabel('Positive PSD [1/Hz]'); 
        ax1.set_ylabel('Total noise difference [1/Hz]'); 
        ax1.set_xscale('log'); ax1.set_yscale('log')
        # ax1.legend()

        
        S_n = deepcopy(self.S)
        S_n[S_n > 0] = nullval
        err_p = deepcopy(self.err_p)
        err_n = deepcopy(self.err_n)
        err_p[S_n > 0] = 0
        err_n[S_n > 0] = 0
        if plotError:
            ax2.errorbar(self.f, -S_n, [err_n, err_p], marker='o', ms=4.0, ls='-', lw=0, alpha=0.8, elinewidth=1.5, capsize=0, mew=0, zorder=20,
                        label=label)
        else:
            ax2.loglog(self.f, -S_n, label=label)
        if f is not None:
            temp = deepcopy(S)
            temp[S > 0] = nullval
            ax2.loglog(f, -temp, color=ax2.lines[-1].get_color())
        
        ax2.set_xscale('log'); ax2.set_yscale('log')
        ax2.grid(which='both')
        # ax2.legend()
        # ax2.title(titles[j])
        # ax2.set_yticks([0.001, 0.01, 0.1, 1], ['-0.1%', '-1%', '-10%', '-100%'])
        
        ax2.set_yticks(ax2.get_yticks().tolist())
        ax2.set_xlim(xlim); ax2.set_ylim(ylim)
        # yticklbl = ax2.get_yticklabels()
        # print(yticklbl)
        # for j in range(len(yticklbl)):
        #     txt = yticklbl[j].get_text()
        #     print(txt)
        #     yticklbl[j].set_text(txt[0:14]+'-'+txt[14:])
        # ax2.set_yticklabels(yticklbl)
        # ax2.set_ylabel('Negative PSD [1/Hz]'); 
        ax1.set_ylabel('Total noise difference [-1/Hz]'); 
        ax2.invert_yaxis()
        
        
        # fig = plt.gcf()
        # fig.set_size_inches(10, 18)
        # fig.suptitle(title, y=0.93, fontsize=16)
        plt.setp(ax1.get_xticklabels(), visible=False);
        plt.xlabel('Frequency [Hz]')


class CalError:
    def __init__(self, filename=None):
        self.filename = filename
        if filename is not None:
            self.load(filename)
            self.loadMonitor(filename)
    
    # def __str__(self):
    #     return result
    
    def load(self, filename):
        assert filename != None
        data = np.loadtxt(filename)
        self.f = data[:,0]
        self.mag = data[:,1]
        self.phase = data[:,2]*180/np.pi
        self.mag_n = data[:,3]
        self.phase_n = data[:,4]*180/np.pi
        self.mag_p = data[:,5]
        self.phase_p = data[:,6]*180/np.pi
        
        self.gps = int(filename[-14:-4])
    
    def loadcsv(self, filename):
        data = []
        with open(filename) as fp:
            reader = csv.reader(fp, delimiter=",", quotechar='"')
            j = 0
            for row in reader:
                # print(j, row)
                if j == 0:
                    header = row[1]
                    header = header.replace(header[2], ".")
                    freq = float(header[header.find(':')+1:header.rindex('}')])
                else:
                    if len(row) != 0:
                        # print(j, row)
                        data.append(float(row[1]))
                j = j+1
                # data_read = [row for row in reader]
        
        return {'f': freq, 'data': np.array(data)}
    
    def loadMonitor(self, filename):
        self.addrname = filename[:filename.rindex('/')+1]
        monitor = []
        for file in glob.glob(self.addrname + "*mag*.csv"):
            monitor.append(self.loadcsv(file))
        self.monitor = monitor
        
    def correct(self, corrfile):
        data = np.loadtxt(corrfile)
        freq = data[:,0]
        real = data[:,1]
        imag = data[:,2]
        
        corr_mag = abs(real + 1j*imag)
        corr_phase = np.angle(real + 1j*imag)*180/np.pi
        
        self.mag = self.mag*corr_mag
        self.mag_n = self.mag_n*corr_mag
        self.mag_p = self.mag_p*corr_mag
        self.phase = self.phase + corr_phase
        self.phase_n = self.phase_n + corr_phase
        self.phase_p = self.phase_p + corr_phase
    
    def plot(self):
        plt.subplot(211)
        plt.semilogx(self.f, self.mag_n, '--')
        plt.semilogx(self.f, self.mag)
        plt.semilogx(self.f, self.mag_p, '--')
        for mon in self.monitor:
            freq = mon['f']
            mag = mon['data']
            freq = freq*np.ones(mag.shape)
            plt.plot(freq, mag, '.', markersize=0.5)
        plt.ylabel('Mag [-]')
        plt.grid(which='both')
        plt.title('Calibration uncertainty at L1, ' + str(self.gps))

        plt.subplot(212)
        plt.semilogx(self.f, self.phase_n, '--')
        plt.semilogx(self.f, self.phase, '--')
        plt.semilogx(self.f, self.phase_p, '--')
        plt.ylabel('Phase [deg]'); plt.xlabel('Frequency [Hz]')
        # plt.xscale("log")
        # plt.yscale("log")
        plt.grid(which='both')
    
    def db(self, val):
        return 20*np.log10(val)
    
    def db2mag(self, val):
        return 10**(val/20)



def getDeltaN_t(filelist):
    unsqzlist = []
    for name in filelist:
        unsqz = DARM(name)
        # unsqz.calcErr(unsqz.relerr_D)
        # unsqz.removeLines(method='DARM')
        # unsqz.rebin_log(f_bin)
        unsqzlist.append(unsqz)
    
    sum_N2_ij = np.zeros(unsqz.S.shape)
    n = len(unsqzlist)
    for i in np.arange(0, n):
        S_i = unsqzlist[i].S
        for j in np.arange(i+1, n):
            S_j = unsqzlist[j].S
            N_ij = 2*(S_i - S_j)/(S_i + S_j)
            sum_N2_ij = sum_N2_ij + N_ij**2
            # print('Summing ('+str(i)+','+str(j)+')')
    
    delta_N_t = np.sqrt(2/(n*(n-1))*sum_N2_ij/n)
    return delta_N_t

def getAngleSqzHighFreq(budget, f=4000, theta=None, plot=False):
    freq = np.array([float(f)])
    ifo = budget.ifo
    theta_SQZ = ifo.Squeezer.SQZAngle # don't forget to revert budget back to beginning state!
    if theta is None:
        theta_LO = -(ifo.Optics.Quadrature.dc*180/np.pi - 90)
        sqzanglelist = np.linspace(theta_LO - 5, theta_LO + 5, 11)
        S_end = []
        for phi in sqzanglelist:
            ifo.Squeezer.SQZAngle = phi*np.pi/180
            trace = budget.run(freq=freq, ifo=ifo)
            S_end.append(trace.Quantum.asd[0])
        theta = sqzanglelist[np.argmin(S_end)]
    sqzanglelist = np.linspace(theta - 1, theta + 1, 21)
    S_end = []
    for phi in sqzanglelist:
        ifo.Squeezer.SQZAngle = phi*np.pi/180
        trace = budget.run(freq=freq, ifo=ifo)
        S_end.append(trace.Quantum.asd[0])
    if plot:
        plt.plot(sqzanglelist, S_end)
    ifo.Squeezer.SQZAngle = theta_SQZ
    return sqzanglelist[np.argmin(S_end)]


def convertUnit(samples, key):
    for i in range(len(key)):
        k = key[i]
        if k == 'Squeezer.SQZAngle' or k == 'Optics.SRM.SRCGouy_rad' or k == 'Optics.MM_IFO_OMCphi' or k == 'Optics.MM_ARM_SRCphi' or k == 'Squeezer.MM_SQZ_OMCphi' or k == 'Squeezer.FilterCavity.psi_mm':
            # print('Squeezer.SQZAngle')
            samples[:,i] = samples[:,i]*180/np.pi
        if k == 'Optics.SRM.Tunephase':
            samples[:,i] = db2mag(samples[:,i])*180/np.pi
        if k == 'Laser.ArmPower':
            # print('Laser.ArmPower')
            samples[:,i] = db2mag(samples[:,i])/1000
        if k == 'Optics.PhotoDetectorEfficiency' or k == 'Optics.MM_IFO_OMC' or k == 'Squeezer.FilterCavity.L_mm' or k == 'Squeezer.InjectionLoss' or k == 'Optics.MM_ARM_SRC' or k == 'Squeezer.MM_SQZ_OMC':
            # print('Optics.PhotoDetectorEfficiency')
            samples[:,i] = db2mag(samples[:,i])*100
        if k == 'Squeezer.SQZAngleRMS':
            # print('Squeezer.SQZAngleRMS')
            samples[:,i] = db2mag(samples[:,i])*1000
        if k == 'Squeezer.FilterCavity.Ti' or k == 'Squeezer.FilterCavity.Lrt' or k == 'Optics.BSLoss':
            # print('Squeezer.FilterCavity.Lrt')
            samples[:,i] = db2mag(samples[:,i])*1e6
        if k == 'Squeezer.FilterCavity.Lrms':
            samples[:,i] = db2mag(samples[:,i])*1e12
    return samples

def db(num):
    return 20*np.log10(num)

def db2mag(num):
    return 10**(num/20)