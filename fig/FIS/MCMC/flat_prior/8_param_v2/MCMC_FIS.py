import numpy as np
from numpy.random import randn, rand
import scipy as sp
import scipy.signal

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import corner
from wand.image import Image as WImage


import os
from copy import deepcopy
import time
import csv
import glob

import emcee
from multiprocessing import Pool

# import sys
# sys.path.insert(0, './pygwinc/')

import gwinc

from gwinc.noise.quantum import shotrad_debug
import lib



def db(num):
    return 20*np.log10(num)

def db2mag(num):
    return 10**(num/20)


def model(param, freq, budget, key):
    fcutoff = 300
    
    # param = [
    #     ifo.Laser.ArmPower, # kW
    #     db(1 - ifo.Optics.PhotoDetectorEfficiency), 
    #     db(ifo.Optics.MM_IFO_OMC), #
    #     ifo.Optics.MM_IFO_OMCphi, # rad
    #     db(ifo.Squeezer.MM_SQZ_OMC), 
    #     ifo.Squeezer.SQZAngleRMS, # rad
    #     # ifo.Optics.MM_ARM_SRCphi,
    #     # ifo.Optics.SRM.Tunephase, 
    #     ifo.Optics.SRM.SRCGouy_rad,
    #     ifo.Squeezer.SQZAngle, # rad
    #     # ifo.Squeezer.AmplitudedB,
    #     # db(ifo.Squeezer.InjectionLoss),
    #     # ifo.Optics.Quadrature.dc - 90*np.pi/180,
    #     # db(ifo.Optics.MM_ARM_SRC),
    #     # db(ifo.Optics.BSLoss),
    #     # ifo.Squeezer.MM_SQZ_OMCphi
    # ]
    
    ifo = budget.ifo
    ifo[key[0]] = param[0]
    ifo[key[1]] = 1 - db2mag(param[1])
    ifo[key[2]] = db2mag(param[2])
    ifo[key[3]] = param[3]
    ifo[key[4]] = db2mag(param[4])
    ifo[key[5]] = param[5]
    ifo[key[6]] = param[6]
    ifo[key[7]] = param[7]
    # ifo[key[8]] = param[8]
    # ifo[key[9]] = param[9]
    # ifo[key[10]] = db2mag(param[10])
    # ifo[key[11]] = param[11]
    # ifo[key[12]] = param[12] + 90*np.pi/180
    # ifo[key[13]] = db2mag(param[13])
    # ifo[key[14]] = db2mag(param[14])
    # ifo[key[15]] = param[15]
    
    ifo.Squeezer.Type = 'None'
    trace = budget.run(freq=freq, ifo=ifo)
    S_uns = trace.Quantum.psd
    S_shot = trace.Quantum.RelASSqz.psd + trace.Quantum.MM.psd + trace.Quantum.Readout.psd
    
    ifo.Squeezer.Type = 'Freq Independent'
    trace = budget.run(freq=freq, ifo=ifo)
    S_fis = trace.Quantum.psd
    S_diff = S_fis - S_uns
    return np.concatenate((S_diff, S_shot[freq > fcutoff]), axis=0)
    # return np.concatenate((S_diff, S_shot), axis=0)


def logFlatPrior(param, bound):
    prior = 0
    for j in range(len(param)):
        if param[j] < bound[j][0] or param[j] > bound[j][1]:
            #print(str(param[j]) + " out of " + str(bound[j]))
            prior = -np.inf
    return prior


def logLike(param, freq, budget, key, data, relerr):
    mdl = model(param, freq, budget, key)
    result = -np.sum(abs(data - mdl)**2/abs(data*relerr)**2 + np.log(2*np.pi*abs(data*relerr)**2)/2)
    # result = -np.sum(abs(data - mdl)**2/abs(data*relerr)**2)
    if np.isnan(result):
        result = -np.inf
    return result


def logPost(param, freq, budget, key, bound, data, relerr):
    result = logLike(param, freq, budget, key, data, relerr) + logFlatPrior(param, bound)
    # if np.isnan(resut):
    #     print(logLike(param, freq, ifo, key, data))
    return result


def mcmc(fislist):
    dir = './data/';
    filelist = [
            '0514_0_FDS',
            '0514_1_Unsqz_FCmis_LOonCLF',
            '0514_2_FIS',
            '0514_3_FIAS',
            '0514_4_FIS_minus140',
            '0514_5_Unsqz_FCmis_LOonCLF',
            '0514_6_FIS_minus150',
            '0514_7_FIS_minus170',
            '0514_8_FIS_minus140',
            '0514_9_Unsqz_FCmis_LOonCLF',
            '0514_10_QND_minus30',
            '0514_11_QND_minus50',
            '0514_12_QND_plus100',
            '0514_13_Unsqz_FCmis_LOonCLF',
            '0514_14_QND_plus143',
            '0514_15_QND_plus140',
            '0514_16_QND_plus140', # Lost lock after this
            '0514_17_Unsqz_FCmis_LOonCLF',
            '0514_18_QND_plus200',
            '0514_19_QND_plus250',
            '0514_20_Unsqz_FCmis_LOonCLF',
            '0514_21_FDS',
            '0514_22_FDS',
            '0514_23_Unsqz_FCmis_LOonCLF',
            '0514_24_QND_plus40',
            '0514_25_Unsqz_FCmis_LOonCLF',
            '0514_26_QND_plus70',
            '0514_27_Unsqz_FCmis_LOonCLF',
            '0514_28_QND_plus140',
            '0514_29_Unsqz_FCmis_LOonCLF',
            '0514_30_QND_plus170', # Lost lock
        ]
    ext = '.h5'


    sqzanglelist = np.array([np.nan]*31)
    list1 = [ 2,  3,   4,   6,  7,   8,  10,    11,  12,  14,  15,  16,  18,   19,  24,  26,  28,  30]
    list2 = [ 0, 78, 5.2, 9.7, 24, 5.6, -14, -11.7, -28, -21, -21, -21, -15, -9.2, -58, -37, -22, -18]
    sqzanglelist[list1] = list2

    freq = np.geomspace(20,4000,50)
    fcutoff = 300 # Above 100 Hz, all classical noises are known

    # fislist = list1
    # fislist =[ 2,  3,   4,   6,  7,   10,    11,  12,    19,  24,  26,  28,  30]
    # fislist = [19]


    Slist = np.array([])
    relerrlist = np.array([])
    freqlist = np.array([])
    # residual = []
    for i in range(len(filelist)):
        if i not in fislist:
            continue

        if i > 0 and i < 5:
            unsfile = filelist[1]
        if i > 5 and i < 9:
            unsfile = filelist[5]
        if i > 9 and i < 13:
            unsfile = filelist[9]
        if i > 13 and i < 17:
            unsfile = filelist[13]
        if i > 17 and i < 20:
            unsfile = filelist[17]
        if i > 20 and i < 23:
            unsfile = filelist[20]
        if i > 23 and i < 25:
            unsfile = filelist[23]
        if i > 25 and i < 27:
            unsfile = filelist[25]
        if i > 27 and i < 29:
            unsfile = filelist[27]
        if i > 29 and i < 31:
            unsfile = filelist[29]
        fisfile = filelist[i]
        uns = lib.DARM(dir+unsfile+ext)
        uns.relerr_n = uns.relerrD_n; uns.relerr_p = uns.relerrD_p # Sum relerrD first
        uns.calcErr()

        fis = lib.DARM(dir+fisfile+ext)
        shot = deepcopy(fis)
        fis.relerr_n = fis.relerrD_n; fis.relerr_p = fis.relerrD_p
        fis.calcErr()

        diff = fis - uns
        diff.err_n = np.sqrt(diff.err_n**2 + (diff.S*fis.relerrG_n)**2 + (uns.S*uns.relerrN_n)**2)
        diff.err_p = np.sqrt(diff.err_p**2 + (diff.S*fis.relerrG_p)**2 + (uns.S*uns.relerrN_p)**2)
        diff.relerr_n = diff.err_n/abs(diff.S)
        diff.relerr_p = diff.err_p/abs(diff.S)
        diff.removeLines('DARM'); diff.rebin_log(freq)
        diff.relerr = (diff.relerr_n + diff.relerr_p)/2

        shot.S = shot.S_null - shot.S_dark
        shot.relerr_n = np.sqrt(shot.relerrD_n**2+shot.relerrG_n**2+shot.relerrN_n**2)
        shot.relerr_p = np.sqrt(shot.relerrD_p**2+shot.relerrG_p**2+shot.relerrN_p**2)
        shot.calcErr()
        shot.removeLines('DARM'); shot.removeLines('CAL'); shot.rebin_log(freq)
        shot.relerr = (shot.relerr_n + shot.relerr_p)/2

        # uns.S = uns.S_xcorr
        # uns.relerr_n = uns.relerrD_n; uns.relerr_p = uns.relerrD_p # Sum relerrD first
        # uns.calcErr()
        # fisqn = fis - uns
        # fisqn.err_n = np.sqrt(fisqn.err_n**2 + (fisqn.S*fis.relerrG_n)**2 + (uns.S*uns.relerrN_n)**2)
        # fisqn.err_p = np.sqrt(fisqn.err_p**2 + (fisqn.S*fis.relerrG_p)**2 + (uns.S*uns.relerrN_p)**2)
        # fisqn.relerr_n = fisqn.err_n/abs(fisqn.S)
        # fisqn.relerr_p = fisqn.err_p/abs(fisqn.S)
        # fisqn.removeLines('DARM'); fisqn.rebin_log(freq)
        # fisqn.relerr = (fisqn.relerr_n + fisqn.relerr_p)/2

        # plt.errorbar(freq, abs(diff.S), [diff.err_n, diff.err_p], marker='o', ms=1.75,
        #              ls='-', lw=0.15, elinewidth=1.5, label='DARM difference')
        # plt.errorbar(freq, shot.S, shot.S*[shot.relerr_n, shot.relerr_p], marker='*', ms=1.75,
        #              ls='-', lw=0.15, elinewidth=1.5, label='Shot')
        # Slist.append(np.concatenate((diff.S, shot.S), axis=0))
        # Slist = np.concatenate((Slist, np.concatenate((diff.S, shot.S[freq > fcutoff], fisqn.S[freq > fcutoff]), axis=0)), axis=0)
        # relerrlist = np.concatenate((relerrlist, np.concatenate((diff.relerr, shot.relerr[freq > fcutoff], fisqn.relerr[freq > fcutoff]), axis=0)), axis=0)
        # freqlist = np.concatenate((freqlist, np.concatenate((freq, freq[freq > fcutoff], freq[freq > fcutoff]), axis=0)), axis=0)
        Slist = np.concatenate((Slist, np.concatenate((diff.S, shot.S[freq > fcutoff]), axis=0)), axis=0)
        relerrlist = np.concatenate((relerrlist, np.concatenate((diff.relerr, shot.relerr[freq > fcutoff]), axis=0)), axis=0)
        freqlist = np.concatenate((freqlist, np.concatenate((freq, freq[freq > fcutoff]), axis=0)), axis=0)
        # Slist = np.concatenate((Slist, np.concatenate((diff.S, shot.S), axis=0)), axis=0)
        # relerrlist = np.concatenate((relerrlist, np.concatenate((diff.relerr, shot.relerr), axis=0)), axis=0)
        # freqlist = np.concatenate((freqlist, np.concatenate((freq, freq), axis=0)), axis=0)


    data = Slist
    relerr = relerrlist
    # plt.errorbar(freqlist, abs(data), abs(data)*[relerr, relerr], marker='*', ms=1.75,
    #              ls='-', lw=0.15, elinewidth=1.5, label='all')
    sqzAngle = sqzanglelist[fislist][0]




    budget = gwinc.load_budget('Aplus')
    ifo = budget.ifo
    ifo.Optics.ITM.Transmittance = 0.0148
    ifo.Optics.ETM.Transmittance = 5e-6
    ifo.Optics.Quadrature.dc = np.pi / 180 * (90-11) # theta_LO
    ifo.Laser.ArmPower = 300e3; 
    if hasattr(ifo.Laser, 'Power'):
        del ifo.Laser.Power
    ifo.Optics.PhotoDetectorEfficiency = 1-0.16 # 1 - (0.025 + 0.01 + 800e-6 + 0.015 + 0.007 + 0.02)
    ifo.Optics.MM_IFO_OMC = 0.05
    ifo.Optics.MM_IFO_OMCphi = -20 *np.pi/180

    ifo.Optics.SRM.SRCGouy_rad = 19 *np.pi/180
    ifo.Optics.SRM.Tunephase = 0.14 *np.pi/180 # SRCL_detuning
    ifo.Optics.BSLoss = 500e-6
    ifo.Optics.is_OPD = False # default is False
    ifo.Optics.MM_ARM_SRC = 10**(-31.46/20)
    ifo.Optics.MM_ARM_SRCphi = 0 *np.pi/180

    ifo.Squeezer.Type = 'Freq Independent'
    ifo.Squeezer.AmplitudedB = 17.4 # z_eff_dB
    ifo.Squeezer.SQZAngleRMS = 30e-3; # [rad]
    ifo.Squeezer.InjectionLoss = 0.046; # [-]
    ifo.Squeezer.direct_mm_sqz_ifo = False; # whether MM_SQZ_OMC is SQZ2OMC (False, default) or SQZ2IFO (True)
    ifo.Squeezer.MM_SQZ_OMC = 0.05; # mode-mismatch between squeezer to IFO or OMC
    ifo.Squeezer.MM_SQZ_OMCphi = -45 *np.pi/180; # mismatch phasing
    ifo.Squeezer.SQZAngle = (lib.getAngleSqzHighFreq(budget)+sqzAngle+0.2)*np.pi/180; # 12.3 is sqz corresponding to -11 deg LO angle, 0.14 deg SRCL detuning, and -31 dB MM_SR


    title = r'$\theta_{SQZ}$=' + str(round(ifo.Squeezer.SQZAngle*180/np.pi,2)) + '$^{\circ}$, ' + str(ifo.Squeezer.AmplitudedB) + 'dB generated, ' \
        + '$\phi_{rms}$=' + str(ifo.Squeezer.SQZAngleRMS*1000) +'mrad, ' + '$\Lambda_{inj}=$' + str(round(ifo.Squeezer.InjectionLoss*100,2)) + '%, ' \
        + r'$\Upsilon_{SQZ}$=(' + str(round(ifo.Squeezer.MM_SQZ_OMC*100,2)) + '%, ' + str(ifo.Squeezer.MM_SQZ_OMCphi*180/np.pi) + '$^{\circ}$)\n' \
        + r'$\theta_{LO}$=-11$^{\circ}$, $P_{arm}$=' + str(round(ifo.Laser.ArmPower/1e3)) + 'kW, ' + '$\Lambda_{RO}$=' + str(round((1-ifo.Optics.PhotoDetectorEfficiency)*100, 2)) +'%, '\
        + r'$\Upsilon_{OMC}$=(' + str(round(ifo.Optics.MM_IFO_OMC*100,2)) + '%, ' + str(ifo.Optics.MM_IFO_OMCphi*180/np.pi) + '$^{\circ}$)\n'\
        + r'$\psi_{SR}$= ' + str(ifo.Optics.SRM.SRCGouy_rad*180/np.pi) + '$^{\circ}$, ' + '$\Delta \phi_{SR}$=' + str(round(ifo.Optics.SRM.Tunephase*180/np.pi, 2)) + '$^{\circ}$, '\
        + 'OPD=' + str(ifo.Optics.is_OPD) + ', ' + '$\Lambda_{SR}$=' + str(round(ifo.Optics.BSLoss*1e6)) + ' ppm, '\
        + r'$\Upsilon_{SR}$=(' + str(round(ifo.Optics.MM_ARM_SRC*100,2)) + '%, ' + str(ifo.Optics.MM_ARM_SRCphi*180/np.pi) + '$^{\circ}$)\n'\



    key = [
        'Laser.ArmPower',
        'Optics.PhotoDetectorEfficiency', 
        'Optics.MM_IFO_OMC',
        'Optics.MM_IFO_OMCphi',
        'Squeezer.MM_SQZ_OMC',
        'Squeezer.SQZAngleRMS',
        # 'Optics.MM_ARM_SRCphi',
        # 'Optics.SRM.Tunephase', 
        'Optics.SRM.SRCGouy_rad',
        'Squeezer.SQZAngle',
        # 'Squeezer.AmplitudedB',
        # 'Squeezer.InjectionLoss',
        # 'Optics.Quadrature.dc',
        # 'Optics.MM_ARM_SRC',
        # 'Optics.BSLoss',
        # 'Squeezer.MM_SQZ_OMCphi'
    ]

    param = [
        ifo.Laser.ArmPower, # kW
        db(1 - ifo.Optics.PhotoDetectorEfficiency), 
        db(ifo.Optics.MM_IFO_OMC), #
        ifo.Optics.MM_IFO_OMCphi, # rad
        db(ifo.Squeezer.MM_SQZ_OMC), 
        ifo.Squeezer.SQZAngleRMS, # rad
        # ifo.Optics.MM_ARM_SRCphi,
        # ifo.Optics.SRM.Tunephase, 
        ifo.Optics.SRM.SRCGouy_rad,
        ifo.Squeezer.SQZAngle, # rad
        # ifo.Squeezer.AmplitudedB,
        # db(ifo.Squeezer.InjectionLoss),
        # ifo.Optics.Quadrature.dc - 90*np.pi/180,
        # db(ifo.Optics.MM_ARM_SRC),
        # db(ifo.Optics.BSLoss),
        # ifo.Squeezer.MM_SQZ_OMCphi
    ]
    
    
    ndim = len(key) # number of parameters to estimate
    nwalkers = 16*ndim # number of walkers
    nsteps = 2000 # number of steps each walker will take
    nthreads = 16 # number of parallel threads to use


    # Choose initial conditions

    bound = [
        (0, 1000e3), # arm power
        (db(1e-4), db(1)), # loss_RO
        (db(1e-4), db(1)), # MM_OMC
        (-360*np.pi/180, 360*np.pi/180), # mmPsi_OMC
        (db(1e-4), db(1)), # MM_SQZ
        (0, 1000e-3), # phase noise
        (-360*np.pi/180, 360*np.pi/180), # SR Gouy
        # (-2*np.pi, 2*np.pi), # mmPsi_SR
        # (-2*np.pi, 2*np.pi), # SRCL detuning
        (-360*np.pi/180, 360*np.pi/180), # SQZ angle
        # (0, 30), # Gen sqz
        # (db(1e-4), db(1)), # injection loss
        # (-2*np.pi, 2*np.pi), # LO angle
        # (db(1e-4), db(1)), # MM_SR
        # (-160, 0), # loss_SR
        # (-2*np.pi, 2*np.pi), # mmPsi_SQZ
    ]

    print('Max posterior = ' + str(-np.sum(np.log(2*np.pi*abs(data*relerr)**2)/2)))
    print('Initial posterior is '+str(logPost(param, freq, budget, key, bound, data, relerr)))
    return
    
    bound0 = [
        (250e3, 350e3), # arm power
        (db(0.01), db(0.2)), # loss_RO
        (db(0.01), db(0.15)), # MM_OMC
        (-60*np.pi/180, 20*np.pi/180), # mmPsi_OMC
        (db(0.01), db(0.15)), # MM_SQZ
        (10e-3, 80e-3), # phase noise
        (10*np.pi/180, 90*np.pi/180), # SR Gouy
        # (-2*np.pi, 2*np.pi), # mmPsi_SR
        # (-2*np.pi, 2*np.pi), # SRCL detuning
        (-5*np.pi/180, 5*np.pi/180), # SQZ angle
        # (0, 30), # Gen sqz
        # (-80, 0), # injection loss
        # (-2*np.pi, 2*np.pi), # LO angle
        # (-80, 0), # MM_SR
        # (-160, 0), # loss_SR
        # (-2*np.pi, 2*np.pi), # mmPsi_SQZ
    ]

    param0 = np.array(
        [[bound0[i][0] + (bound0[i][1]-bound0[i][0])*rand()
         for i in range(ndim)]
        for ii in range(nwalkers)]) 

    
    print('Start MCMC on theta_SQZ = ' + str(sqzAngle))
    start = time.time()
    sampler = emcee.EnsembleSampler(nwalkers, ndim, logPost, args=(freq, budget, key, bound, data, relerr), threads=nthreads);
    sampler.run_mcmc(param0, nsteps, store=True);
    end = time.time()
    print(str((end - start)/60) + ' min')
    
    
    nburn = 1000
    samples = sampler.chain[:, nburn:, :].reshape((-1, ndim))
    
    
    
    # np.save('')
    
    
    
    
    
    
    print('Final posterior is '+str(logPost(np.median(samples, axis=0), freq, budget, key, bound, data, relerr)))
    
    samples[:,0] = samples[:,0]/1000 # arm power
    samples[:,1] = db2mag(samples[:,1])*100 # loss_RO
    samples[:,2] = db2mag(samples[:,2])*100 # MM_OMC
    samples[:,3] = samples[:,3]*180/np.pi # mmPsi_OMC
    samples[:,4] = db2mag(samples[:,4])*100 # MM_SQZ
    samples[:,5] = samples[:,5]*1000 # phase noise
    samples[:,6] = samples[:,6]*180/np.pi # SR Gouy
    # samples[:,6] = samples[:,6]*180/np.pi # mmPsi_SR
    # samples[:,7] = samples[:,7]*180/np.pi # SRCL detuning
    samples[:,7] = samples[:,7]*180/np.pi # SQZ angle
    # samples[:,9] = samples[:,9] # Gen sqz
    # samples[:,10] = db2mag(samples[:,10])*100 # injection loss
    # samples[:,12] = samples[:,12]*180/np.pi # LO angle
    # samples[:,13] = db2mag(samples[:,13])*100 # MM_SR
    # samples[:,14] = db2mag(samples[:,14])*1e6 # loss_SR
    # samples[:,15] = samples[:,15]*180/np.pi # mmPsi_SQZ
    
    
    median = np.percentile(samples, 50, axis=0)
    medn1 = np.percentile(samples, 16, axis=0)
    medp1 = np.percentile(samples, 84, axis=0)
    print(median.round(2))
    print((median-medn1).round(2))
    print((medp1-median).round(2))

    labels = ['$P_{arm}$ [kW]', 
              r'$\Lambda_{RO}$ [%]', 
              r'$\Upsilon_{OMC}$ [%]', 
              r'$\Upsilon_{OMC}$ phase [deg]', 
              r'$\Upsilon_{SQZ}$ [%]', 
              '$\phi_{rms}$ [mrad]', 
              '$\psi_{SR}$ [deg]', 
              # r'$\Upsilon_{SR}$ phase [deg]', 
              # r'$\Delta \phi_{SR}$ [deg]', 
              r'$\theta_{SQZ}$ [deg]', 
              # 'Gen SQZ [dB]', 
              # '$\Lambda_{INJ}$ [%]', 
              # r'$\theta_{LO}$ [deg]', 
              # r'$\Upsilon_{SR}$ [%]', 
              # '$\Lambda_{SR}$ [ppm]', 
              # r'$\Upsilon_{SQZ}$ phase [deg]'
             ]
    corner.corner(samples, labels=labels, quantiles=[0.16, 0.5, 0.84], show_titles=True, title_kwargs={"fontsize": 10});
    fig = plt.gcf()
    # fig.set_size_inches(18,16)
    fig.suptitle(title, y=0.96, fontsize=12)
    plt.savefig('./fig/FIS/MCMC/8_param_v2/0514_'+str(fislist[0])+'_theta_' + str(sqzAngle) + '_8param.pdf')


#print('MCMC started')
list1 = [ 2,  3,   4,   6,  7,   8,  10,    11,  12,  14,  15,  16,  18,   19,  24,  26,  28,  30]
list2 = [ 0, 78, 5.2, 9.7, 24, 5.6, -14, -11.7, -28, -21, -21, -21, -15, -9.2, -58, -37, -22, -18]



fislist = [11, 4, 10, 30, 2, 28, 12, 26, 6, 7, 3, 24]
fislist = [19]
for fis in fislist:
    mcmc([fis])