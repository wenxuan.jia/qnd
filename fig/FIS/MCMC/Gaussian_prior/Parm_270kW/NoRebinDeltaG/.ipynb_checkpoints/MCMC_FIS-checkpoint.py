import numpy as np
from numpy.random import randn, rand
import scipy as sp
import scipy.signal

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import corner
#from wand.image import Image as WImage


import os
from copy import deepcopy
import time
import csv
import glob

import emcee
from multiprocessing import Pool

import sys
sys.path.insert(0, './pygwinc/')
import gwinc

from gwinc.noise.quantum import shotrad_debug
import lib


import multiprocessing
import queue
from itertools import count


def db(num):
    return 20*np.log10(num)

def db2mag(num):
    return 10**(num/20)


def init(budget):
    ifo = budget.ifo
    ifo.Optics.ITM.Transmittance = 0.0148
    ifo.Optics.ETM.Transmittance = 5e-6
    ifo.Optics.Quadrature.dc = np.pi / 180 * (90-11) # theta_LO
    ifo.Laser.ArmPower = 270e3; 
    if hasattr(ifo.Laser, 'Power'):
        del ifo.Laser.Power
    ifo.Optics.PhotoDetectorEfficiency = 1-0.16 # 1 - (0.025 + 0.01 + 800e-6 + 0.015 + 0.007 + 0.02)
    ifo.Optics.MM_IFO_OMC = 0.05
    ifo.Optics.MM_IFO_OMCphi = -20 *np.pi/180

    ifo.Optics.SRM.SRCGouy_rad = 19 *np.pi/180
    ifo.Optics.SRM.Tunephase = 0.14 *np.pi/180 # SRCL_detuning
    ifo.Optics.BSLoss = 500e-6
    ifo.Optics.is_OPD = False # default is False
    ifo.Optics.MM_ARM_SRC = 10**(-31.46/20)
    ifo.Optics.MM_ARM_SRCphi = 0 *np.pi/180

    ifo.Squeezer.Type = 'Freq Independent'
    ifo.Squeezer.AmplitudedB = 17.4 # z_eff_dB
    ifo.Squeezer.SQZAngleRMS = 30e-3; # [rad]
    ifo.Squeezer.InjectionLoss = 0.064; # [-]
    ifo.Squeezer.direct_mm_sqz_ifo = False; # whether MM_SQZ_OMC is SQZ2OMC (False, default) or SQZ2IFO (True)
    ifo.Squeezer.MM_SQZ_OMC = 0.05; # mode-mismatch between squeezer to IFO or OMC
    ifo.Squeezer.MM_SQZ_OMCphi = -45 *np.pi/180; # mismatch phasing
    # ifo.Squeezer.SQZAngle = (lib.getAngleSqzHighFreq(budget)+sqzAngle+0.2)*np.pi/180; # 12.3 is sqz corresponding to -11 deg LO angle, 0.14 deg SRCL detuning, and -31 dB MM_SR

    
def getdata(freq, fislist):
    dir = './data/';
    filelist = [
            '0514_0_FDS',
            '0514_1_Unsqz_FCmis_LOonCLF',
            '0514_2_FIS',
            '0514_3_FIAS',
            '0514_4_FIS_minus140',
            '0514_5_Unsqz_FCmis_LOonCLF',
            '0514_6_FIS_minus150',
            '0514_7_FIS_minus170',
            '0514_8_FIS_minus140',
            '0514_9_Unsqz_FCmis_LOonCLF',
            '0514_10_QND_minus30',
            '0514_11_QND_minus50',
            '0514_12_QND_plus100',
            '0514_13_Unsqz_FCmis_LOonCLF',
            '0514_14_QND_plus143',
            '0514_15_QND_plus140',
            '0514_16_QND_plus140', # Lost lock after this
            '0514_17_Unsqz_FCmis_LOonCLF',
            '0514_18_QND_plus200',
            '0514_19_QND_plus250',
            '0514_20_Unsqz_FCmis_LOonCLF',
            '0514_21_FDS',
            '0514_22_FDS',
            '0514_23_Unsqz_FCmis_LOonCLF',
            '0514_24_QND_plus40',
            '0514_25_Unsqz_FCmis_LOonCLF',
            '0514_26_QND_plus70',
            '0514_27_Unsqz_FCmis_LOonCLF',
            '0514_28_QND_plus140',
            '0514_29_Unsqz_FCmis_LOonCLF',
            '0514_30_QND_plus170', # Lost lock
        ]
    ext = '.h5'



    sqzanglelist = np.array([np.nan]*31)
    list1 = [ 2,  3,   4,   6,  7,   8,  10,    11,  12,  14,  15,  16,  18,   19,  24,  26,  28,  30]
    list2 = [ 0, 78, 5.2, 9.7, 24, 5.6, -14, -11.7, -28, -21, -21, -21, -15, -9.2, -58, -37, -22, -18]
    sqzanglelist[list1] = list2

    
    fcutoff = 300 # Above 100 Hz, all classical noises are known

    # fislist = list1
    # fislist =[ 2,  3,   4,   6,  7,   10,    11,  12,    19,  24,  26,  28,  30]
    # fislist = [19]


    Slist = np.array([])
    relerrlist = np.array([])
    freqlist = np.array([])
    # residual = []
    # for i in range(len(filelist)):
        # if i not in fislist:
        #     continue
    fislist = [fislist]
    for i in fislist:
        if i > 0 and i < 5:
            unsfile = filelist[1]
        if i > 5 and i < 9:
            unsfile = filelist[5]
        if i > 9 and i < 13:
            unsfile = filelist[9]
        if i > 13 and i < 17:
            unsfile = filelist[13]
        if i > 17 and i < 20:
            unsfile = filelist[17]
        if i > 20 and i < 23:
            unsfile = filelist[20]
        if i > 23 and i < 25:
            unsfile = filelist[23]
        if i > 25 and i < 27:
            unsfile = filelist[25]
        if i > 27 and i < 29:
            unsfile = filelist[27]
        if i > 29 and i < 31:
            unsfile = filelist[29]
        fisfile = filelist[i]
        uns = lib.DARM(dir+unsfile+ext)
        uns.relerr_n = uns.relerrD_n; uns.relerr_p = uns.relerrD_p # Sum relerrD first
        uns.calcErr()

        fis = lib.DARM(dir+fisfile+ext)
        shot = deepcopy(fis)
        fis.relerr_n = fis.relerrD_n; fis.relerr_p = fis.relerrD_p
        fis.calcErr()

        diff = fis - uns
        # diff.err_n = np.sqrt(diff.err_n**2 + (diff.S*fis.relerrG_n)**2 + (uns.S*uns.relerrN_n)**2)
        # diff.err_p = np.sqrt(diff.err_p**2 + (diff.S*fis.relerrG_p)**2 + (uns.S*uns.relerrN_p)**2)
        # diff.removeLines('DARM'); diff.rebin_log(freq)
        diff.err_n = np.sqrt(diff.err_n**2 + (uns.S*uns.relerrN_n)**2)
        diff.err_p = np.sqrt(diff.err_p**2 + (uns.S*uns.relerrN_p)**2)
        diff.removeLines('DARM'); diff.rebin_log(freq)
        diff.err_n = np.sqrt(diff.err_n**2 + (diff.S*np.interp(freq, fis.f, fis.relerrG_n))**2)
        diff.err_p = np.sqrt(diff.err_p**2 + (diff.S*np.interp(freq, fis.f, fis.relerrG_p))**2)

        diff.relerr_n = diff.err_n/abs(diff.S)
        diff.relerr_p = diff.err_p/abs(diff.S)
        diff.relerr = (diff.relerr_n + diff.relerr_p)/2

        shot.S = shot.S_null - shot.S_dark
        # shot.relerr_n = np.sqrt(shot.relerrD_n**2+shot.relerrG_n**2+shot.relerrN_n**2)
        # shot.relerr_p = np.sqrt(shot.relerrD_p**2+shot.relerrG_p**2+shot.relerrN_p**2)
        # shot.calcErr()
        # shot.removeLines('DARM'); shot.removeLines('CAL'); shot.rebin_log(freq)
        shot.relerr_n = np.sqrt(shot.relerrD_n**2+shot.relerrN_n**2)
        shot.relerr_p = np.sqrt(shot.relerrD_p**2+shot.relerrN_p**2)
        shot.calcErr()
        shot.removeLines('DARM'); shot.removeLines('CAL'); shot.rebin_log(freq)
        shot.relerr_n = np.sqrt(shot.relerr_n**2+np.interp(freq, shot.f_lin, shot.relerrG_n)**2)
        shot.relerr_p = np.sqrt(shot.relerr_p**2+np.interp(freq, shot.f_lin, shot.relerrG_p)**2)

        shot.relerr = (shot.relerr_n + shot.relerr_p)/2

        # uns.S = uns.S_xcorr
        # uns.relerr_n = uns.relerrD_n; uns.relerr_p = uns.relerrD_p # Sum relerrD first
        # uns.calcErr()
        # fisqn = fis - uns
        # fisqn.err_n = np.sqrt(fisqn.err_n**2 + (fisqn.S*fis.relerrG_n)**2 + (uns.S*uns.relerrN_n)**2)
        # fisqn.err_p = np.sqrt(fisqn.err_p**2 + (fisqn.S*fis.relerrG_p)**2 + (uns.S*uns.relerrN_p)**2)
        # fisqn.relerr_n = fisqn.err_n/abs(fisqn.S)
        # fisqn.relerr_p = fisqn.err_p/abs(fisqn.S)
        # fisqn.removeLines('DARM'); fisqn.rebin_log(freq)
        # fisqn.relerr = (fisqn.relerr_n + fisqn.relerr_p)/2

        # plt.errorbar(freq, abs(diff.S), [diff.err_n, diff.err_p], marker='o', ms=1.75,
        #              ls='-', lw=0.15, elinewidth=1.5, label='DARM difference')
        # plt.errorbar(freq, shot.S, shot.S*[shot.relerr_n, shot.relerr_p], marker='*', ms=1.75,
        #              ls='-', lw=0.15, elinewidth=1.5, label='Shot')
        # Slist.append(np.concatenate((diff.S, shot.S), axis=0))
        # Slist = np.concatenate((Slist, np.concatenate((diff.S, shot.S[freq > fcutoff], fisqn.S[freq > fcutoff]), axis=0)), axis=0)
        # relerrlist = np.concatenate((relerrlist, np.concatenate((diff.relerr, shot.relerr[freq > fcutoff], fisqn.relerr[freq > fcutoff]), axis=0)), axis=0)
        # freqlist = np.concatenate((freqlist, np.concatenate((freq, freq[freq > fcutoff], freq[freq > fcutoff]), axis=0)), axis=0)
        # Slist = np.concatenate((Slist, np.concatenate((diff.S, shot.S[freq > fcutoff]), axis=0)), axis=0)
        # relerrlist = np.concatenate((relerrlist, np.concatenate((diff.relerr, shot.relerr[freq > fcutoff]), axis=0)), axis=0)
        # freqlist = np.concatenate((freqlist, np.concatenate((freq, freq[freq > fcutoff]), axis=0)), axis=0)
        Slist = np.concatenate((Slist, np.concatenate((diff.S, shot.S), axis=0)), axis=0)
        relerrlist = np.concatenate((relerrlist, np.concatenate((diff.relerr, shot.relerr), axis=0)), axis=0)
        freqlist = np.concatenate((freqlist, np.concatenate((freq, freq), axis=0)), axis=0)


    data = Slist
    relerr = relerrlist
    return (data, relerr)
    
    
def model(param, freq, budget, key):
    fcutoff = 300
    
    # param = [
    #     ifo.Laser.ArmPower, # kW
    #     db(1 - ifo.Optics.PhotoDetectorEfficiency), 
    #     db(ifo.Optics.MM_IFO_OMC), #
    #     ifo.Optics.MM_IFO_OMCphi, # rad
    #     db(ifo.Squeezer.MM_SQZ_OMC), 
    #     ifo.Squeezer.SQZAngleRMS, # rad
    #     # ifo.Optics.MM_ARM_SRCphi,
    #     # ifo.Optics.SRM.Tunephase, 
    #     ifo.Optics.SRM.SRCGouy_rad,
    #     ifo.Squeezer.SQZAngle, # rad
    #     # ifo.Squeezer.AmplitudedB,
    #     # db(ifo.Squeezer.InjectionLoss),
    #     # ifo.Optics.Quadrature.dc - 90*np.pi/180,
    #     # db(ifo.Optics.MM_ARM_SRC),
    #     # db(ifo.Optics.BSLoss),
    #     # ifo.Squeezer.MM_SQZ_OMCphi
    # ]
    
    ifo = budget.ifo
    # ifo[key[0]] = db2mag(param[0])
    ifo[key[0]] = param[0] # SQZ angle
    ifo[key[1]] = 1 - db2mag(param[1])
    ifo[key[2]] = db2mag(param[2])
    ifo[key[3]] = param[3]
    ifo[key[4]] = db2mag(param[4])
    ifo[key[5]] = db2mag(param[5])
    ifo[key[6]] = param[6]
    # ifo[key[7]] = param[7]
    # ifo[key[8]] = param[8]
    # ifo[key[9]] = param[9]
    # ifo[key[10]] = db2mag(param[10])
    # ifo[key[11]] = param[11]
    # ifo[key[12]] = param[12] + 90*np.pi/180
    # ifo[key[13]] = db2mag(param[13])
    # ifo[key[14]] = db2mag(param[14])
    # ifo[key[15]] = param[15]
    
    
    
    ifo.Squeezer.Type = 'None'
    trace = budget.run(freq=freq, ifo=ifo)
    S_uns = trace.Quantum.psd
    S_shot = trace.Quantum.RelASSqz.psd + trace.Quantum.MM.psd + trace.Quantum.Readout.psd
    
    ifo.Squeezer.Type = 'Freq Independent'
    trace = budget.run(freq=freq, ifo=ifo)
    S_fis = trace.Quantum.psd
    S_diff = S_fis - S_uns
    return np.concatenate((S_diff, S_shot), axis=0)


# def logFlatPrior(param, bound):
#     prior = 0
#     for j in range(len(param)):
#         if param[j] < bound[j][0] or param[j] > bound[j][1]:
#             #print(str(param[j]) + " out of " + str(bound[j]))
#             prior = -np.inf
#     return prior

def logGaussianPrior(param, bound): # bound[0] and bound[1] are +- sigma
    prior = 0
    for i in range(len(param)):
        mu = (bound[i][0]+bound[i][1])/2
        sigma = abs(bound[i][1]-mu)
        prior += np.log(1.0/(np.sqrt(2*np.pi*sigma**2)))-0.5*(param[i]-mu)**2/sigma**2
    return prior


def logLike(param, freq, budget, key, data, relerr):
    mdl = model(param, freq, budget, key)
    result = -np.sum(abs(data - mdl)**2/abs(data*relerr)**2 + np.log(2*np.pi*abs(data*relerr)**2)/2)
    # result = -np.sum(abs(data - mdl)**2/abs(data*relerr)**2)
    if np.isnan(result):
        result = -np.inf
    return result


def logPost(param, freq, budget, key, bound, data, relerr):
    result = logLike(param, freq, budget, key, data, relerr) + logGaussianPrior(param, bound)
    # if np.isnan(resut):
    #     print(logLike(param, freq, ifo, key, data))
    return result


def pltmcmcsetup(param, bound, xlabel):
    plt.hist(param, density=True, label='Initial walkers')
    mu = (bound[1]+bound[0])/2
    sigma = abs(bound[1]-bound[0])/2
    x = np.linspace(mu-2.5*sigma, mu+2.5*sigma, 100)
    plt.plot(x, 1/np.sqrt(2*np.pi*sigma**2)*np.exp(-0.5*(x-mu)**2/sigma**2), '-', label='Prior')
    plt.plot([mu-sigma,mu-sigma], [0, 1/np.sqrt(2*np.pi*sigma**2)], '--', color='black', label='Prior $\pm 1 \sigma$')
    plt.plot([mu+sigma,mu+sigma], [0, 1/np.sqrt(2*np.pi*sigma**2)], '--', color='black')
    # plt.yscale(yscale)
    # ax = plt.gca()
    # ax.yaxis.set_minor_formatter('{x:.1f}')
    # ax.yaxis.set_major_formatter('{x:.1f}')
    plt.grid(which='both')
    plt.xlabel(xlabel)
    # plt.legend()
    # plt.title(title0)
    
    

def mcmc(result_queue, fislist, param0):
    # freq = np.geomspace(20,4000,50)
    freq = np.geomspace(20,2000,100)
    [data, relerr] = getdata(freq, fislist)

    sqzanglelist = np.array([np.nan]*31)
    list1 = [ 2,  3,   4,   6,  7,   8,  10,    11,  12,  14,  15,  16,  18,   19,  24,  26,  28,  30]
    list2 = [ 0, 78, 5.2, 9.7, 24, 5.6, -14, -11.7, -28, -21, -21, -21, -15, -9.2, -58, -37, -22, -18]
    sqzanglelist[list1] = list2
    sqzAngle = sqzanglelist[fislist]
    
    budget = gwinc.load_budget('Aplus')
    ifo = budget.ifo
    init(budget)
    ifo.Squeezer.SQZAngle = (lib.getAngleSqzHighFreq(budget)+sqzAngle+0.2)*np.pi/180; # 12.3 is sqz corresponding to -11 deg LO angle, 0.14 deg SRCL detuning, and -31 dB MM_SR
    
    key = [
        # 'Laser.ArmPower',
        'Squeezer.SQZAngle',
        'Optics.PhotoDetectorEfficiency', 
        'Optics.MM_IFO_OMC',
        'Optics.MM_IFO_OMCphi',
        'Squeezer.MM_SQZ_OMC',
        'Squeezer.SQZAngleRMS',
        # 'Optics.MM_ARM_SRCphi',
        # 'Optics.SRM.Tunephase', 
        'Optics.SRM.SRCGouy_rad',
        # 'Squeezer.AmplitudedB',
        # 'Squeezer.InjectionLoss',
        # 'Optics.Quadrature.dc',
        # 'Optics.MM_ARM_SRC',
        # 'Optics.BSLoss',
        # 'Squeezer.MM_SQZ_OMCphi'
    ]
    
    param = [
        # db(ifo.Laser.ArmPower), # kW
        ifo.Squeezer.SQZAngle, # rad
        db(1 - ifo.Optics.PhotoDetectorEfficiency), 
        db(ifo.Optics.MM_IFO_OMC), #
        ifo.Optics.MM_IFO_OMCphi, # rad
        db(ifo.Squeezer.MM_SQZ_OMC), 
        db(ifo.Squeezer.SQZAngleRMS), # rad
        # ifo.Optics.MM_ARM_SRCphi,
        # ifo.Optics.SRM.Tunephase, 
        ifo.Optics.SRM.SRCGouy_rad,
        # ifo.Squeezer.AmplitudedB,
        # db(ifo.Squeezer.InjectionLoss),
        # ifo.Optics.Quadrature.dc - 90*np.pi/180,
        # db(ifo.Optics.MM_ARM_SRC),
        # db(ifo.Optics.BSLoss),
        # ifo.Squeezer.MM_SQZ_OMCphi
    ]

    
    bound = [
        (ifo.Squeezer.SQZAngle-1*np.pi/180, ifo.Squeezer.SQZAngle+1*np.pi/180), # SQZ angle
        # (db(300e3), db(320e3)), # arm power
        (db(0.08), db(0.10)), # loss_RO
        (db(0.04), db(0.12)), # MM_OMC
        (-40*np.pi/180, -10*np.pi/180), # mmPsi_OMC
        (db(0.01), db(0.08)), # MM_SQZ
        (db(20e-3), db(30e-3)), # phase noise
        (20*np.pi/180, 50*np.pi/180), # SR Gouy
        # (-2*np.pi, 2*np.pi), # mmPsi_SR
        # (-2*np.pi, 2*np.pi), # SRCL detuning
        # (0, 30), # Gen sqz
        # (-80, 0), # injection loss
        # (-2*np.pi, 2*np.pi), # LO angle
        # (-80, 0), # MM_SR
        # (-160, 0), # loss_SR
        # (-2*np.pi, 2*np.pi), # mmPsi_SQZ
    ]

    [nwalkers, ndim] = param0.shape
    nsteps = 10000 # number of steps each walker will take
    nthreads = 16 # number of parallel threads to use
    
    print('Start MCMC')
    sampler = emcee.EnsembleSampler(nwalkers, ndim, logPost, args=(freq, budget, key, bound, data, relerr), threads=nthreads);
    sampler.run_mcmc(param0, nsteps, store=True, skip_initial_state_check=True);

    samples = sampler.chain
    # np.save(folder + 'chain_' + str(round(rand()*10000000)) + '.npy', samples.astype(np.float32))
    result_queue.put((samples, True))


def main():
    # fislist  = [  19,    11,   4,  10,  30, 2,  28,  12,  26,   6,  7,  3,  24]
    # fislist  = [  19,    11,   4,  10,  30, 2,  28,  12,  26,   6, 7]
    fislist = [19, 11, 4, 10, 30, 2]
    # fislist = [28,  12,  26,   6, 7]

    # folder = './fig/FIS/MCMC/all/'
    folder = './'
    temp = np.linspace(1,100,100)
    np.save(folder + 'test.npy', temp.astype(np.float32))
    
    
    
    sqzanglelist = np.array([np.nan]*31)
    list1 = [ 2,  3,   4,   6,  7,   8,  10,    11,  12,  14,  15,  16,  18,   19,  24,  26,  28,  30]
    list2 = [ 0, 78, 5.2, 9.7, 24, 5.6, -14, -11.7, -28, -21, -21, -21, -15, -9.2, -58, -37, -22, -18]
    sqzanglelist[list1] = list2
    
    
    start = time.time()
    
    for fis in fislist:
        sqzAngle = sqzanglelist[fis]
        
        budget = gwinc.load_budget('Aplus')
        init(budget)
        ifo = budget.ifo
        ifo.Squeezer.SQZAngle = (lib.getAngleSqzHighFreq(budget)+sqzAngle+0.2)*np.pi/180; # 12.3 is sqz corresponding to -11 deg LO angle, 0.14 deg SRCL detuning, and -31 dB MM_SR


        title = str(ifo.Squeezer.AmplitudedB) + 'dB generated, ' \
            + '$\phi_{rms}$=' + str(round(ifo.Squeezer.SQZAngleRMS*1000)) +'mrad, ' + '$\Lambda_{inj}=$' + str(round(ifo.Squeezer.InjectionLoss*100,2)) + '%, ' \
            + r'$\Upsilon_{SQZ}$=(' + str(round(ifo.Squeezer.MM_SQZ_OMC*100,2)) + '%, ' + str(ifo.Squeezer.MM_SQZ_OMCphi*180/np.pi) + '$^{\circ}$)\n' \
            + r'$\theta_{LO}$=-11$^{\circ}$, $P_{arm}$=' + str(round(ifo.Laser.ArmPower/1e3)) + 'kW, ' + '$\Lambda_{RO}$=' + str(round((1-ifo.Optics.PhotoDetectorEfficiency)*100, 2)) +'%, '\
            + r'$\Upsilon_{OMC}$=(' + str(round(ifo.Optics.MM_IFO_OMC*100,2)) + '%, ' + str(ifo.Optics.MM_IFO_OMCphi*180/np.pi) + '$^{\circ}$)\n'\
            + r'$\psi_{SR}$= ' + str(ifo.Optics.SRM.SRCGouy_rad*180/np.pi) + '$^{\circ}$, ' + '$\Delta \phi_{SR}$=' + str(round(ifo.Optics.SRM.Tunephase*180/np.pi, 2)) + '$^{\circ}$, '\
            + 'OPD=' + str(ifo.Optics.is_OPD) + ', ' + '$\Lambda_{SR}$=' + str(round(ifo.Optics.BSLoss*1e6)) + ' ppm, '\
            + r'$\Upsilon_{SR}$=(' + str(round(ifo.Optics.MM_ARM_SRC*100,2)) + '%, ' + str(ifo.Optics.MM_ARM_SRCphi*180/np.pi) + '$^{\circ}$)\n'\


        key = [
            'Squeezer.SQZAngle',
            # 'Laser.ArmPower',
            'Optics.PhotoDetectorEfficiency', 
            'Optics.MM_IFO_OMC',
            'Optics.MM_IFO_OMCphi',
            'Squeezer.MM_SQZ_OMC',
            'Squeezer.SQZAngleRMS',
            # 'Optics.MM_ARM_SRCphi',
            # 'Optics.SRM.Tunephase', 
            'Optics.SRM.SRCGouy_rad',
            # 'Squeezer.AmplitudedB',
            # 'Squeezer.InjectionLoss',
            # 'Optics.Quadrature.dc',
            # 'Optics.MM_ARM_SRC',
            # 'Optics.BSLoss',
            # 'Squeezer.MM_SQZ_OMCphi'
        ]


        ndim = len(key) # number of parameters to estimate
        ncores = multiprocessing.cpu_count()
        print('Total ' + str(ncores) + ' CPU cores')
        nwalkerPerCore = ndim*2
        nwalkers = ncores*nwalkerPerCore # number of walkers


        # Choose initial conditions

        # print('Max likelihood = ' + str(-np.sum(np.log(2*np.pi*abs(data*relerr)**2)/2)))
        # defaultLike = logLike(param, freq, budget, key, data, relerr)
        # print('Initial default likelihood is '+str(defaultLike))


        bound0 = [
            (ifo.Squeezer.SQZAngle-2*np.pi/180, ifo.Squeezer.SQZAngle+2*np.pi/180), # SQZ angle
            # (db(280e3), db(340e3)), # arm power
            (db(0.01), db(0.15)), # loss_RO
            (db(0.01), db(0.15)), # MM_OMC
            (-60*np.pi/180, 20*np.pi/180), # mmPsi_OMC
            (db(0.01), db(0.12)), # MM_SQZ
            (db(10e-3), db(70e-3)), # phase noise
            (10*np.pi/180, 80*np.pi/180), # SR Gouy
            # (-2*np.pi, 2*np.pi), # mmPsi_SR
            # (-2*np.pi, 2*np.pi), # SRCL detuning
            # (0, 30), # Gen sqz
            # (-80, 0), # injection loss
            # (-2*np.pi, 2*np.pi), # LO angle
            # (-80, 0), # MM_SR
            # (-160, 0), # loss_SR
            # (-2*np.pi, 2*np.pi), # mmPsi_SQZ
        ]



        # param0 = np.array(
        #     [[(bound0[i][1]+bound0[i][0])/2 + abs((bound0[i][1]-bound0[i][0])/2)*randn()
        #      for i in range(ndim)]
        #     for ii in range(nwalkers)]) 

        param0 = np.array(
            [[bound0[i][0] + (bound0[i][1]-bound0[i][0])*rand()
             for i in range(ndim)]
            for ii in range(nwalkers)]) 

        # np.save(folder + '0514_'+str(fislist[0])+'_theta_' + str(sqzAngle) + '_param0.npy', param0.astype(np.float32))



        # labels = ['$P_{arm}$ [kW]', 
        #           r'$\Lambda_{RO}$ [%]', 
        #           r'$\Upsilon_{OMC}$ [%]', 
        #           r'$\Upsilon_{OMC}$ phase [deg]', 
        #           r'$\Upsilon_{SQZ}$ [%]', 
        #           '$\phi_{rms}$ [mrad]', 
        #           '$\psi_{SR}$ [deg]', 
        #           # r'$\Upsilon_{SR}$ phase [deg]', 
        #           # r'$\Delta \phi_{SR}$ [deg]', 
        #           r'$\theta_{SQZ}$ [deg]', 
        #           # 'Gen SQZ [dB]', 
        #           # '$\Lambda_{INJ}$ [%]', 
        #           # r'$\theta_{LO}$ [deg]', 
        #           # r'$\Upsilon_{SR}$ [%]', 
        #           # '$\Lambda_{SR}$ [ppm]', 
        #           # r'$\Upsilon_{SQZ}$ phase [deg]'
        #          ]
        # plt.figure(1)
        # for i in np.arange(0,7):
        #     plt.subplot(3,3,i+1)
        #     pltmcmcsetup(param0[:,i], bound[i], labels[i])
        #     # plt.ticklabel_format(axis='x', style='sci', scilimits=(4,4))
        #     if i == 0:
        #         plt.legend(loc='upper right')
        #         ax = plt.gca()
        #         ax.xaxis.set_major_formatter(lambda x, pos: str(round(db2mag(x)/1000, 1)))
        #     elif i in [1,2,4]:
        #         ax = plt.gca()
        #         ax.xaxis.set_major_formatter(lambda x, pos: str(round(db2mag(x)*100, 1)))
        #     elif i in [3,6,7]:
        #         ax = plt.gca()
        #         ax.xaxis.set_major_formatter(lambda x, pos: str(round(x*180/np.pi, 1)))
        #     elif i == 5:
        #         ax = plt.gca()
        #         ax.xaxis.set_major_formatter(lambda x, pos: str(round(db2mag(x)*1e3, 1)))
        #     if i%3 == 0:
        #         plt.ylabel('Probability distribution')
        # plt.subplot(3,3,9)
        # post = []
        # for p in param0:
        #     post.append(logLike(p, freq, budget, key, data, relerr))
        # # print(post)
        # plt.hist(post, density=False, label='Likelihood of initial walkers')
        # plt.plot([defaultLike, defaultLike], [0, nwalkers], '--', color='black', label='Likelihood of default param$')
        # plt.xlabel('log(Likelihood)')
        # plt.ticklabel_format(axis='x', style='sci', scilimits=(4,4))
        # plt.grid(which='both')
        # plt.legend()
        # # plt.grid(which='both')
        # fig = plt.gcf()
        # fig.set_size_inches(16,14)
        # fig.suptitle(title, y=0.95, fontsize=10)
        # plt.savefig(folder+'0514_'+str(fislist[0])+'_theta_' + str(sqzAngle) + '_initial.pdf')



        result_queue = multiprocessing.Queue()
        workers = []
        for i in range(ncores):
            worker = multiprocessing.Process(
                target=mcmc, args=(result_queue, fis, param0[i*nwalkerPerCore:(i+1)*nwalkerPerCore, :])
            )
            # print('Start '+str(i))
            worker.start()
            workers.append(worker)

        finished = 0
        while True:
            try:
                chain, result = result_queue.get(timeout=0.1)
                if result:
                    finished += 1
                    print(str(finished) + " cores finished!")
                    if finished == 1:
                        chains = chain
                    else:
                        chains = np.concatenate((chains, chain), axis=0)
                    if finished == ncores:
                        break
            except KeyboardInterrupt:
                break
            except (multiprocessing.TimeoutError, queue.Empty):
                pass
        for worker in workers:
            worker.kill()

        
        np.save(folder + '0514_'+str(fis)+'_theta_' + str(sqzAngle) + '.npy', chains.astype(np.float32))
        
    end = time.time()
    print(str((end - start)/60) + ' min')

if __name__ == "__main__":
    main()