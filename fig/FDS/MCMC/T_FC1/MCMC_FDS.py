import numpy as np
from numpy.random import randn, rand
import scipy as sp
import scipy.signal

import matplotlib
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import corner
#from wand.image import Image as WImage


import os
from copy import deepcopy
import time
import csv
import glob

import emcee
from multiprocessing import Pool

import sys
sys.path.insert(0, './pygwinc/')
import gwinc

from gwinc.noise.quantum import shotrad_debug
import lib


import multiprocessing
import queue
from itertools import count


def db(num):
    return 20*np.log10(num)

def db2mag(num):
    return 10**(num/20)


def init(budget):
    sqzAngle  = [-22.562, -12.435, -7.83,  -4.284,  0.585,  2.714,    4.2,   13.9,   19.0,  23.25, 36.473]
    loss_RO   = [  8.724,   8.702, 7.223,    6.71,  8.503,   8.71,  9.721,  9.657, 10.163,  9.717, 10.014]
    mm_OMC    = [   8.04,    6.88, 7.997,   7.704,  6.599,  6.388,  5.921,  6.564,  6.062,  6.881,  7.472]
    mmPsi_OMC = [   64.9,    42.8, -17.5,    65.6,   38.7,   8.37,  -24.6,  -50.6,  -1.75,    3.5,    -11]
    mm_SQZ    = [   3.09,   4.427,  0.93,   0.776,  1.298,  1.269,  1.152,  7.187,  5.963,  5.708,  7.669]
    phirms    = [ 76.589,  24.358, 19.685, 19.452, 42.398, 41.207, 37.446, 34.727, 28.142, 22.507, 23.404]
    psi_SR    = [ 53.041,  50.572,   60.4, 56.811, 56.765, 56.222, 48.727, 48.662, 42.085, 41.754, 53.132]
    # mmPsi_OMC = [46.595978, 53.87251, 19.161562, 23.487387, 6.018777, -1.6073924, -22.345772, -49.662807, -37.113842, -36.705215, -40.117657]
    
    i = 7
    
    ifo = budget.ifo
    ifo.Optics.ITM.Transmittance = 0.0148
    ifo.Optics.ETM.Transmittance = 5e-6
    ifo.Optics.Quadrature.dc = np.pi / 180 * (90-11) # theta_LO
    ifo.Laser.ArmPower = 257.2e3; 
    if hasattr(ifo.Laser, 'Power'):
        del ifo.Laser.Power
    ifo.Optics.PhotoDetectorEfficiency = 0.92 # 1 - (0.025 + 0.01 + 800e-6 + 0.015 + 0.007 + 0.02)
    ifo.Optics.MM_IFO_OMC = 0.036
    ifo.Optics.MM_IFO_OMCphi = mmPsi_OMC[i] *np.pi/180

    ifo.Optics.SRM.SRCGouy_rad = 43 *np.pi/180
    ifo.Optics.SRM.Tunephase = 0.14 *np.pi/180 # SRCL_detuning
    ifo.Optics.BSLoss = 500e-6
    ifo.Optics.is_OPD = False # default is False
    ifo.Optics.MM_ARM_SRC = 10**(-31.46/20)
    ifo.Optics.MM_ARM_SRCphi = 0 *np.pi/180

    ifo.Squeezer.Type = 'Freq Independent'
    ifo.Squeezer.AmplitudedB = 17.4 # z_eff_dB
    ifo.Squeezer.SQZAngleRMS = phirms[i]*1e-3; # 28.4 [rad]
    ifo.Squeezer.InjectionLoss = 0.064; # [-]
    ifo.Squeezer.direct_mm_sqz_ifo = False; # whether MM_SQZ_OMC is SQZ2OMC (False, default) or SQZ2IFO (True)
    ifo.Squeezer.MM_SQZ_OMCphi = 0 *np.pi/180; # mismatch phasing
    ifo.Squeezer.MM_SQZ_OMC = 7.09e-2; # mode-mismatch between squeezer to IFO or OMC
    ifo.Squeezer.SQZAngle = 9*np.pi/180; # 12.3 is sqz corresponding to -11 deg LO angle, 0.14 deg SRCL detuning, and -31 dB MM_SR
    
    ifo.Squeezer.FilterCavity.fdetune = -26; # [Hz], FC detuning
    ifo.Squeezer.FilterCavity.Lrt = 20e-6; # [-], FC RTL
    ifo.Squeezer.FilterCavity.Ti = 800e-6; # [-] FC1 power Transmission
    ifo.Squeezer.FilterCavity.L_mm = 0.1e-2; # [-], mode-mismatch from OPO to FC
    ifo.Squeezer.FilterCavity.psi_mm = -14.9*np.pi/180; # [rad], mismatch phasing
    ifo.Squeezer.FilterCavity.Gouy_rad = 49.7*np.pi/180; # [rad], FC Gouy phase
    ifo.Squeezer.FilterCavity.Lrms = 0.12e-12; # [m], FC RMS motion
    

    
def getdata(freq, filelist):
    dir = './data/'
    ext = '.h5'

    
    fcutoff = 300 # Above 100 Hz, all classical noises are known

    # fislist = list1
    # fislist =[ 2,  3,   4,   6,  7,   10,    11,  12,    19,  24,  26,  28,  30]
    # fislist = [19]
    
    budget = gwinc.load_budget('Aplus')
    init(budget)
    ifo = budget.ifo
    
    modelerrfile = './fig/FIS/MCMC/all/FitmmPsiOMC/deltaM.npy'
    (relerrM_n, relerrM_p) = np.load(modelerrfile)


    Slist = np.array([])
    relerrlist = np.array([])
    freqlist = np.array([])
    filelist = [filelist]
    
    for file in filelist:
        fdsfile = file[0]
        unsfile = file[1]
        sqzAngle = file[2]
        
        
        
        D_r = lib.DARM(dir+unsfile+ext)
        D_s = lib.DARM(dir+fdsfile+ext)

        ifo.Squeezer.Type = 'None'
        trace = budget.run(freq=D_r.f, ifo=ifo)
        M = lib.DARM()
        M.f = D_r.f
        M.S = trace.Quantum.psd

        C = D_r - M

        C.relerr_n = D_s.relerrN_n; C.relerr_p = D_s.relerrN_p
        C.calcErr(); C.removeLines('DARM'); 
        C.rebin_log(freq)

        D_r.relerr_n = D_r.relerrD_n; D_r.relerr_p = D_r.relerrD_p
        D_r.calcErr(); D_r.removeLines('DARM'); 
        D_r.rebin_log(freq)

        D_s.relerr_n = D_s.relerrD_n; D_s.relerr_p = D_s.relerrD_p
        D_s.calcErr(); D_s.removeLines('DARM'); 
        D_s.rebin_log(freq)

        ifo.Squeezer.Type = 'None'
        trace = budget.run(freq=D_r.f, ifo=ifo)
        M = lib.DARM()
        M.f = D_r.f
        M.S = trace.Quantum.psd
        M.err_n = M.S*np.interp(M.f, np.geomspace(20,2000,100), relerrM_n)
        M.err_p = M.S*np.interp(M.f, np.geomspace(20,2000,100), relerrM_p)


        Q = D_s - C
        Q.err_n = np.sqrt((D_s.err_n)**2 + (D_r.err_n)**2 + (C.err_n)**2 + (Q.S*np.interp(Q.f, D_s.f_lin, D_s.relerrG_n))**2 + (M.err_n)**2)
        Q.err_p = np.sqrt((D_s.err_p)**2 + (D_r.err_p)**2 + (C.err_p)**2 + (Q.S*np.interp(Q.f, D_s.f_lin, D_s.relerrG_p))**2 + (M.err_p)**2)
        
        
        Slist = np.concatenate((Slist, np.sqrt(Q.S)), axis=0)
        relerrlist = np.concatenate((relerrlist, (Q.err_n + Q.err_p)/2/Q.S/2), axis=0)
        freqlist = np.concatenate((freqlist, freq), axis=0)


    data = Slist
    relerr = relerrlist
    return (data, relerr)
    
    
def model(param, freq, budget, key):
    fcutoff = 300
    
    ifo = budget.ifo
    ifo[key[0]] = param[0] # SQZ angle
    # ifo[key[1]] = db2mag(param[1]) # arm power
    # ifo[key[2]] = param[2] # SRC gouy
    # ifo[key[3]] = 1 - db2mag(param[3]) # loss_RO
    # ifo[key[4]] = db2mag(param[4]) # MM_OMC
    # ifo[key[1]] = param[1] # mmPsi_OMC
    # ifo[key[6]] = param[6] # Gen SQZ
    ifo[key[1]] = db2mag(param[1]) # MM_SQZ
    # ifo[key[8]] = db2mag(param[8]) # phase noise
    ifo[key[2]] = param[2] # FC detune
    ifo[key[3]] = db2mag(param[3]) # loss_FC
    ifo[key[4]] = db2mag(param[4]) # MM_FC
    ifo[key[5]] = param[5] # mmPsi_FC
    ifo[key[6]] = db2mag(param[6]) # L_FC_rms
    ifo[key[7]] = db2mag(param[7]) # T_FC1
    
    
    
    ifo.Squeezer.Type = 'Freq Dependent'
    trace = budget.run(freq=freq, ifo=ifo)
    S_fds = trace.Quantum.psd
    return np.sqrt(S_fds)


# def logFlatPrior(param, bound):
#     prior = 0
#     for j in range(len(param)):
#         if param[j] < bound[j][0] or param[j] > bound[j][1]:
#             #print(str(param[j]) + " out of " + str(bound[j]))
#             prior = -np.inf
#     return prior

def logGaussianPrior(param, bound): # bound[0] and bound[1] are +- sigma
    prior = 0
    for i in range(len(param)):
        mu = (bound[i][0]+bound[i][1])/2
        sigma = abs(bound[i][1]-mu)
        prior += np.log(1.0/(np.sqrt(2*np.pi*sigma**2)))-0.5*(param[i]-mu)**2/sigma**2
    return prior


def logLike(param, freq, budget, key, data, relerr):
    mdl = model(param, freq, budget, key)
    result = -np.sum(abs(data - mdl)**2/abs(data*relerr)**2 + np.log(2*np.pi*abs(data*relerr)**2)/2)
    # result = -np.sum(abs(data - mdl)**2/abs(data*relerr)**2)
    if np.isnan(result):
        result = -np.inf
    return result


def logPost(param, freq, budget, key, bound, data, relerr):
    result = logLike(param, freq, budget, key, data, relerr) + logGaussianPrior(param, bound)
    # if np.isnan(resut):
    #     print(logLike(param, freq, ifo, key, data))
    return result


def pltmcmcsetup(param, bound, xlabel):
    plt.hist(param, density=True, label='Initial walkers')
    mu = (bound[1]+bound[0])/2
    sigma = abs(bound[1]-bound[0])/2
    x = np.linspace(mu-2.5*sigma, mu+2.5*sigma, 100)
    plt.plot(x, 1/np.sqrt(2*np.pi*sigma**2)*np.exp(-0.5*(x-mu)**2/sigma**2), '-', label='Prior')
    plt.plot([mu-sigma,mu-sigma], [0, 1/np.sqrt(2*np.pi*sigma**2)], '--', color='black', label='Prior $\pm 1 \sigma$')
    plt.plot([mu+sigma,mu+sigma], [0, 1/np.sqrt(2*np.pi*sigma**2)], '--', color='black')
    # plt.yscale(yscale)
    # ax = plt.gca()
    # ax.yaxis.set_minor_formatter('{x:.1f}')
    # ax.yaxis.set_major_formatter('{x:.1f}')
    plt.grid(which='both')
    plt.xlabel(xlabel)
    # plt.legend()
    # plt.title(title0)
    
    
    
def convertUnit(samples, key):
    for i in range(len(key)):
        k = key[i]
        if k == 'Squeezer.SQZAngle' or k == 'Optics.SRM.SRCGouy_rad' or k == 'Optics.MM_IFO_OMCphi' or k == 'Optics.MM_ARM_SRCphi' or k == 'Squeezer.MM_SQZ_OMCphi' or k == 'Squeezer.FilterCavity.psi_mm':
            # print('Squeezer.SQZAngle')
            samples[:,i] = samples[:,i]*180/np.pi
        if k == 'Optics.SRM.Tunephase':
            samples[:,i] = db2mag(samples[:,i])*180/np.pi
        if k == 'Laser.ArmPower':
            # print('Laser.ArmPower')
            samples[:,i] = db2mag(samples[:,i])/1000
        if k == 'Optics.PhotoDetectorEfficiency' or k == 'Optics.MM_IFO_OMC' or k == 'Squeezer.FilterCavity.L_mm' or k == 'Squeezer.InjectionLoss' or k == 'Optics.MM_ARM_SRC' or k == 'Squeezer.MM_SQZ_OMC':
            # print('Optics.PhotoDetectorEfficiency')
            samples[:,i] = db2mag(samples[:,i])*100
        if k == 'Squeezer.SQZAngleRMS':
            # print('Squeezer.SQZAngleRMS')
            samples[:,i] = db2mag(samples[:,i])*1000
        if k == 'Squeezer.FilterCavity.Ti' or k == 'Squeezer.FilterCavity.Lrt' or k == 'Optics.BSLoss' or k == 'Squeezer.FilterCavity.Ti':
            # print('Squeezer.FilterCavity.Lrt')
            samples[:,i] = db2mag(samples[:,i])*1e6
        if k == 'Squeezer.FilterCavity.Lrms':
            samples[:,i] = db2mag(samples[:,i])*1e12
    return samples

    

def mcmc(result_queue, filelist, param0):
    # freq = np.geomspace(20,4000,50)
    freq = np.geomspace(20,2000,100)
    [data, relerr] = getdata(freq, filelist)
    
    budget = gwinc.load_budget('Aplus')
    ifo = budget.ifo
    init(budget)
    # ifo.Squeezer.SQZAngle = (lib.getAngleSqzHighFreq(budget))*np.pi/180; # 12.3 is sqz corresponding to -11 deg LO angle, 0.14 deg SRCL detuning, and -31 dB MM_SR
    
    key = [
        'Squeezer.SQZAngle',
        # 'Laser.ArmPower',
        # 'Optics.SRM.SRCGouy_rad',
        # 'Optics.PhotoDetectorEfficiency', 
        # 'Optics.MM_IFO_OMC',
        # 'Optics.MM_IFO_OMCphi',
        # 'Squeezer.AmplitudedB',
        'Squeezer.MM_SQZ_OMC',
        # 'Squeezer.SQZAngleRMS',
        # 'Squeezer.MM_SQZ_OMCphi',
        'Squeezer.FilterCavity.fdetune', 
        'Squeezer.FilterCavity.Lrt',
        'Squeezer.FilterCavity.L_mm',
        'Squeezer.FilterCavity.psi_mm',
        'Squeezer.FilterCavity.Lrms',
        'Squeezer.FilterCavity.Ti',
        # 'Optics.MM_ARM_SRCphi',
        # 'Optics.SRM.Tunephase', 
        # 'Squeezer.InjectionLoss',
        # 'Optics.Quadrature.dc',
        # 'Optics.MM_ARM_SRC',
        # 'Optics.BSLoss',
    ]
    
    
    
    bound = [
        (ifo.Squeezer.SQZAngle-1*np.pi/180, ifo.Squeezer.SQZAngle+1*np.pi/180), # SQZ angle
        # (db(260e3), db(300e3)), # arm power
        # (50*np.pi/180, 60*np.pi/180), # SR Gouy
        # (db(0.06), db(0.10)), # loss_RO
        # (db(0.06), db(0.10)), # MM_OMC
        # (-60*np.pi/180, 20*np.pi/180), # mmPsi_OMC
        # (16.5, 17.5), # Gen sqz
        (db(0.001), db(0.08)), # MM_SQZ
        # (db(20e-3), db(30e-3)), # phase noise
        (-28, -25), # FC detuning
        (db(20e-6), db(60e-6)), # loss_FC
        (db(0.001), db(0.02)), # MM_FC
        (-np.pi, np.pi), # mmPsi_FC
        (db(0.1e-12), db(2e-12)), # L_FC_rms
        # # (-np.pi, np.pi), # mmPsi_SQZ
        (db(900e-6), db(1000e-6)), # T_FC1
        # (-2*np.pi, 2*np.pi), # mmPsi_SR
        # (-2*np.pi, 2*np.pi), # SRCL detuning
        # (-80, 0), # injection loss
        # (-2*np.pi, 2*np.pi), # LO angle
        # (-80, 0), # MM_SR
        # (-160, 0), # loss_SR
    ]

    [nwalkers, ndim] = param0.shape
    nsteps = 10000 # number of steps each walker will take
    nthreads = 16 # number of parallel threads to use
    
    print('Start MCMC with ' + str(nwalkers) + ' walkers, ' + str(nsteps) + ' steps, ' + str(ndim) + ' dims')
    sampler = emcee.EnsembleSampler(nwalkers, ndim, logPost, args=(freq, budget, key, bound, data, relerr), threads=nthreads);
    sampler.run_mcmc(param0, nsteps, store=True, skip_initial_state_check=True);

    samples = sampler.chain
    # np.save(folder + 'chain_' + str(round(rand()*10000000)) + '.npy', samples.astype(np.float32))
    result_queue.put((samples, True))


def main():
    # fislist  = [  19,    11,   4,  10,  30, 2,  28,  12,  26,   6,  7,  3,  24]
    # fislist  = [  19,    11,   4,  10,  30, 2,  28,  12,  26,   6, 7]
    # fislist = [19, 11, 4, 10, 30, 2]
    # fislist = [28,  12,  26,   6, 7]
    filelist = [
        # ('0514_0_FDS', '0514_1_Unsqz_FCmis_LOonCLF', 0),
        # ('0514_21_FDS', '0514_20_Unsqz_FCmis_LOonCLF', 0),
        ('0514_22_FDS', '0514_all_Unsqz_FCmis_LOonCLF', 0),
        # ('0515_01_FDS', '0515_5_Unsqz_FCmis_LOfinalized', 0),
        # ('0515_02_FDS', '0515_5_Unsqz_FCmis_LOfinalized',  0),
        # ('0515_03_FDS', '0515_5_Unsqz_FCmis_LOfinalized',  0),
        # ('0515_04_FDS', '0515_5_Unsqz_FCmis_LOfinalized',  0),
        # ('0515_3_FDS', '0515_5_Unsqz_FCmis_LOfinalized',  0),
        # ('0523_1_FDS', '0523_8_Unsqz_FConRLFCLF_LOengaged', 0),
        # ('0523_5_FDS_angle1', '0523_8_Unsqz_FConRLFCLF_LOengaged', 12-1.5),
        # ('0523_6_FDS_angle2', '0523_8_Unsqz_FConRLFCLF_LOengaged', -7-0.5),
        # ('0523_7_FDS_angle3', '0523_8_Unsqz_FConRLFCLF_LOengaged', -15),
        # ('0523_10-13_FDS', '0523_9_Unsqz_FConRLFCLF_LOengaged_PRX', 0),
        # ('0523_11_FDS', '0523_9_Unsqz_FConRLFCLF_LOengaged_PRX', 0),
        # ('0523_12_FDS', '0523_9_Unsqz_FConRLFCLF_LOengaged_PRX', 0),
        # ('0523_13_FDS', '0523_9_Unsqz_FConRLFCLF_LOengaged_PRX', 0),
    ]

    # folder = './fig/FIS/MCMC/all/'
    folder = './'
    temp = np.linspace(1,100,100)
    np.save(folder + 'test.npy', temp.astype(np.float32))
    
    
    
    start = time.time()
    
    for file in filelist:
        sqzAngle = file[2]
        
        budget = gwinc.load_budget('Aplus')
        init(budget)
        ifo = budget.ifo
        # ifo.Squeezer.SQZAngle = (lib.getAngleSqzHighFreq(budget)+sqzAngle)*np.pi/180; # 12.3 is sqz corresponding to -11 deg LO angle, 0.14 deg SRCL detuning, and -31 dB MM_SR


        # title = str(ifo.Squeezer.AmplitudedB) + 'dB generated, ' \
        #     + '$\phi_{rms}$=' + str(round(ifo.Squeezer.SQZAngleRMS*1000)) +'mrad, ' + '$\Lambda_{inj}=$' + str(round(ifo.Squeezer.InjectionLoss*100,2)) + '%, ' \
        #     + r'$\Upsilon_{SQZ}$=(' + str(round(ifo.Squeezer.MM_SQZ_OMC*100,2)) + '%, ' + str(ifo.Squeezer.MM_SQZ_OMCphi*180/np.pi) + '$^{\circ}$)\n' \
        #     + r'$\theta_{LO}$=-11$^{\circ}$, $P_{arm}$=' + str(round(ifo.Laser.ArmPower/1e3)) + 'kW, ' + '$\Lambda_{RO}$=' + str(round((1-ifo.Optics.PhotoDetectorEfficiency)*100, 2)) +'%, '\
        #     + r'$\Upsilon_{OMC}$=(' + str(round(ifo.Optics.MM_IFO_OMC*100,2)) + '%, ' + str(ifo.Optics.MM_IFO_OMCphi*180/np.pi) + '$^{\circ}$)\n'\
        #     + r'$\psi_{SR}$= ' + str(ifo.Optics.SRM.SRCGouy_rad*180/np.pi) + '$^{\circ}$, ' + '$\Delta \phi_{SR}$=' + str(round(ifo.Optics.SRM.Tunephase*180/np.pi, 2)) + '$^{\circ}$, '\
        #     + 'OPD=' + str(ifo.Optics.is_OPD) + ', ' + '$\Lambda_{SR}$=' + str(round(ifo.Optics.BSLoss*1e6)) + ' ppm, '\
        #     + r'$\Upsilon_{SR}$=(' + str(round(ifo.Optics.MM_ARM_SRC*100,2)) + '%, ' + str(ifo.Optics.MM_ARM_SRCphi*180/np.pi) + '$^{\circ}$)\n'\


        key = [
            'Squeezer.SQZAngle',
            # 'Laser.ArmPower',
            # 'Optics.SRM.SRCGouy_rad',
            # 'Optics.PhotoDetectorEfficiency', 
            # 'Optics.MM_IFO_OMC',
            # 'Optics.MM_IFO_OMCphi',
            # 'Squeezer.AmplitudedB',
            'Squeezer.MM_SQZ_OMC',
            # 'Squeezer.SQZAngleRMS',
            # 'Squeezer.MM_SQZ_OMCphi',
            'Squeezer.FilterCavity.fdetune', 
            'Squeezer.FilterCavity.Lrt',
            'Squeezer.FilterCavity.L_mm',
            'Squeezer.FilterCavity.psi_mm',
            'Squeezer.FilterCavity.Lrms',
            'Squeezer.FilterCavity.Ti',
            # 'Optics.MM_ARM_SRCphi',
            # 'Optics.SRM.Tunephase', 
            # 'Squeezer.InjectionLoss',
            # 'Optics.Quadrature.dc',
            # 'Optics.MM_ARM_SRC',
            # 'Optics.BSLoss',
        ]


        ndim = len(key) # number of parameters to estimate
        ncores = multiprocessing.cpu_count()
        print('Total ' + str(ncores) + ' CPU cores')
        nwalkerPerCore = ndim*2
        nwalkers = ncores*nwalkerPerCore # number of walkers


        # Choose initial conditions

        # print('Max likelihood = ' + str(-np.sum(np.log(2*np.pi*abs(data*relerr)**2)/2)))
        # defaultLike = logLike(param, freq, budget, key, data, relerr)
        # print('Initial default likelihood is '+str(defaultLike))


        bound0 = [
            (ifo.Squeezer.SQZAngle-3*np.pi/180, ifo.Squeezer.SQZAngle+3*np.pi/180), # SQZ angle
            # (db(260e3), db(300e3)), # arm power
            # (30*np.pi/180, 70*np.pi/180), # SR Gouy
            # (db(0.04), db(0.10)), # loss_RO
            # (db(0.04), db(0.10)), # MM_OMC
            # (-60*np.pi/180, 20*np.pi/180), # mmPsi_OMC
            # (15.5, 17.5), # Gen sqz
            (db(0.001), db(0.1)), # MM_SQZ
            # (db(10e-3), db(60e-3)), # phase noise
            (-31, -26), # FC detuning
            (db(20e-6), db(60e-6)), # loss_FC
            (db(0.001), db(0.02)), # MM_FC
            (-180*np.pi/180, 180*np.pi/180), # mmPsi_FC
            (db(0.1e-12), db(3e-12)), # L_FC_rms
            # (-np.pi, np.pi), # mmPsi_SQZ
            (db(750e-6), db(900e-6)), # T_FC1
            # (-2*np.pi, 2*np.pi), # mmPsi_SR
            # (-2*np.pi, 2*np.pi), # SRCL detuning
            # (-80, 0), # injection loss
            # (-2*np.pi, 2*np.pi), # LO angle
            # (-80, 0), # MM_SR
            # (-160, 0), # loss_SR
            # (-2*np.pi, 2*np.pi), # mmPsi_SQZ
        ]



        # param0 = np.array(
        #     [[(bound0[i][1]+bound0[i][0])/2 + abs((bound0[i][1]-bound0[i][0])/2)*randn()
        #      for i in range(ndim)]
        #     for ii in range(nwalkers)]) 

        param0 = np.array(
            [[bound0[i][0] + (bound0[i][1]-bound0[i][0])*rand()
             for i in range(ndim)]
            for ii in range(nwalkers)]) 

        # np.save(folder + '0514_'+str(fislist[0])+'_theta_' + str(sqzAngle) + '_param0.npy', param0.astype(np.float32))



        # labels = ['$P_{arm}$ [kW]', 
        #           r'$\Lambda_{RO}$ [%]', 
        #           r'$\Upsilon_{OMC}$ [%]', 
        #           r'$\Upsilon_{OMC}$ phase [deg]', 
        #           r'$\Upsilon_{SQZ}$ [%]', 
        #           '$\phi_{rms}$ [mrad]', 
        #           '$\psi_{SR}$ [deg]', 
        #           # r'$\Upsilon_{SR}$ phase [deg]', 
        #           # r'$\Delta \phi_{SR}$ [deg]', 
        #           r'$\theta_{SQZ}$ [deg]', 
        #           # 'Gen SQZ [dB]', 
        #           # '$\Lambda_{INJ}$ [%]', 
        #           # r'$\theta_{LO}$ [deg]', 
        #           # r'$\Upsilon_{SR}$ [%]', 
        #           # '$\Lambda_{SR}$ [ppm]', 
        #           # r'$\Upsilon_{SQZ}$ phase [deg]'
        #          ]
        # plt.figure(1)
        # for i in np.arange(0,7):
        #     plt.subplot(3,3,i+1)
        #     pltmcmcsetup(param0[:,i], bound[i], labels[i])
        #     # plt.ticklabel_format(axis='x', style='sci', scilimits=(4,4))
        #     if i == 0:
        #         plt.legend(loc='upper right')
        #         ax = plt.gca()
        #         ax.xaxis.set_major_formatter(lambda x, pos: str(round(db2mag(x)/1000, 1)))
        #     elif i in [1,2,4]:
        #         ax = plt.gca()
        #         ax.xaxis.set_major_formatter(lambda x, pos: str(round(db2mag(x)*100, 1)))
        #     elif i in [3,6,7]:
        #         ax = plt.gca()
        #         ax.xaxis.set_major_formatter(lambda x, pos: str(round(x*180/np.pi, 1)))
        #     elif i == 5:
        #         ax = plt.gca()
        #         ax.xaxis.set_major_formatter(lambda x, pos: str(round(db2mag(x)*1e3, 1)))
        #     if i%3 == 0:
        #         plt.ylabel('Probability distribution')
        # plt.subplot(3,3,9)
        # post = []
        # for p in param0:
        #     post.append(logLike(p, freq, budget, key, data, relerr))
        # # print(post)
        # plt.hist(post, density=False, label='Likelihood of initial walkers')
        # plt.plot([defaultLike, defaultLike], [0, nwalkers], '--', color='black', label='Likelihood of default param$')
        # plt.xlabel('log(Likelihood)')
        # plt.ticklabel_format(axis='x', style='sci', scilimits=(4,4))
        # plt.grid(which='both')
        # plt.legend()
        # # plt.grid(which='both')
        # fig = plt.gcf()
        # fig.set_size_inches(16,14)
        # fig.suptitle(title, y=0.95, fontsize=10)
        # plt.savefig(folder+'0514_'+str(fislist[0])+'_theta_' + str(sqzAngle) + '_initial.pdf')



        result_queue = multiprocessing.Queue()
        workers = []
        for i in range(ncores):
            worker = multiprocessing.Process(
                target=mcmc, args=(result_queue, file, param0[i*nwalkerPerCore:(i+1)*nwalkerPerCore, :])
            )
            # print('Start '+str(i))
            worker.start()
            workers.append(worker)

        finished = 0
        while True:
            try:
                chain, result = result_queue.get(timeout=0.1)
                if result:
                    finished += 1
                    print(str(finished) + " cores finished!")
                    if finished == 1:
                        chains = chain
                    else:
                        chains = np.concatenate((chains, chain), axis=0)
                    if finished == ncores:
                        break
            except KeyboardInterrupt:
                break
            except (multiprocessing.TimeoutError, queue.Empty):
                pass
        for worker in workers:
            worker.kill()

        
        np.save(folder + file[0] + '.npy', chains.astype(np.float32))
        
    end = time.time()
    print(str((end - start)/60) + ' min')

if __name__ == "__main__":
    main()