import numpy as np
import os
import matplotlib.pyplot as plt
from scipy.optimize import minimize
import pytest
from copy import deepcopy

from gwinc import load_budget
from gwinc import Struct
from gwinc.noise.quantum import shotrad_debug
from wield.utilities.file_io import load, save
from wield.pytest import fpath_join, tpath_join, dprint
from wield.bunch import Bunch

import lib


@pytest.fixture
def sqz_data(fpath_join):
    tseries_name = fpath_join('llo_timeseries_data.h5')
    fseries_name = fpath_join('llo_sqz_data.h5')
    force_calc = False
    if os.path.exists(fseries_name) and not force_calc:
        data = load(fseries_name)
    else:
        from gwpy.timeseries import TimeSeries

        nds_data = load(tseries_name)
        calib_data = np.loadtxt(fpath_join('llo_oaf_cal_correction_2022-05-13.txt')).T

        data = Bunch()

        for key, dset in nds_data.items():
            print('Calculating ' + key)
            data[key] = Bunch()
            tseries = TimeSeries(
                data = dset.timeseries,
                dt   = dset.duration_s / len(dset.timeseries),
            )
            psd = tseries.psd(
                fftlength = 2,
                window    = 'hann',
                method    = 'median',
            )
            F_Hz = psd.frequencies.value
            calib = np.interp(F_Hz, calib_data[0], calib_data[1])
            data[key].psd = psd.value * 10**(calib / 10)
            data[key].F_Hz = F_Hz
            data[key].duration_s = dset.duration_s
            data[key].binwidth_Hz = psd.df.value
            data[key] = lib.make_psd_errors(data[key])
        save(fseries_name, data)
    return data


@pytest.fixture
def sqz_data_notched(sqz_data):
    F_Hz = sqz_data[list(sqz_data.keys())[0]].F_Hz
    inds = lib.notch_freq_inds(
        F_Hz,
        (40, 47),
        (55, 65),
        # (296, 320),
        (495, 545),
        # (600, 640),
        (990, 1050),
        (1400, 1600),
        (1700, 1800),
        (1920, 2020),
        fmin=30,
        fmax=2e3,
    )
    F_Hz = F_Hz[inds]
    notched = Bunch()
    for key, dset in sqz_data.items():
        assert np.all(dset.F_Hz[inds] == F_Hz)
        notched[key] = Bunch({
            k: v[inds] for k, v in dset.items()
            if isinstance(v, np.ndarray)
        })
    return notched


@pytest.mark.skip
def test_plot_llo_darm(sqz_data, tpath_join, fpath_join, dprint):
    ref_data = Bunch(load(fpath_join('Feb10_longPSD2.mat')))
    dprint(ref_data.keys())
    dprint(list(sqz_data.keys()))
    dprint(list(sqz_data.fds.keys()))

    dkeys = ['fdas', 'fds', 'fias', 'fis', 'no_sqz']
    rkeys = ['S_fdas', 'S_fds', 'S_fias', 'S_fis', 'S_nosqz']
    for dkey, rkey in zip(dkeys, rkeys):
        fig, ax = plt.subplots()
        ax.loglog(sqz_data[dkey].F_Hz, np.sqrt(sqz_data[dkey].psd))
        ax.loglog(ref_data.freq, np.sqrt(ref_data[rkey]), ls='--')
        lib.makegrid(ax)
        ax.set_title(dkey)
        fig.savefig(tpath_join(dkey + '.pdf'))


def test_llo_notches(sqz_data_notched, fpath_join, tpath_join, dprint):
    data = sqz_data_notched
    F_Hz = data[list(data.keys())[0]].F_Hz
    Flog_Hz = np.logspace(np.log10(F_Hz[0]), np.log10(F_Hz[-1]), 200)

    fig1, ax1 = plt.subplots()
    fig2, ax2 = plt.subplots()
    for key, dset in data.items():
        rebinned = lib.resample_psd(dset, Flog_Hz)
        ax1.loglog(F_Hz, dset.psd, label=key)
        ax2.loglog(rebinned.F_Hz, rebinned.psd, label=key)
    lib.makegrid(ax1, F_Hz)
    ax1.legend()
    fig1.savefig(tpath_join('psd.pdf'))
    lib.makegrid(ax2, Flog_Hz)
    ax2.legend()
    fig2.savefig(tpath_join('rebinned.pdf'))

    fig3, ax3 = plt.subplots()
    fig4, ax4 = plt.subplots()
    for key, dset in data.items():
        if 'no_sqz' in key:
            continue
        diff = lib.subtract_psds(dset, data.no_sqz)
        diff_rebinned = lib.resample_psd(diff, Flog_Hz)
        ax3.semilogx(F_Hz, diff.psd, label=key)
        ax4.semilogx(diff_rebinned.F_Hz, diff_rebinned.psd, label=key)
    lib.makegrid(ax3, F_Hz)
    ax3.legend()
    fig3.savefig(tpath_join('diff.pdf'))
    lib.makegrid(ax4, Flog_Hz)
    ax4.legend()
    fig4.savefig(tpath_join('diff_rebinned.pdf'))


def test_llo_readout_fit(sqz_data_notched, fpath_join, tpath_join, dprint):
    export_yaml = False
    meas = sqz_data_notched['no_sqz']
    # meas = lib.psd2asd(meas, scale=4e3)
    Flog_Hz = np.logspace(np.log10(meas.F_Hz[0]), np.log10(meas.F_Hz[-1]), 200)
    meas = lib.resample_psd(meas, Flog_Hz)
    meas = lib.psd2asd(meas, scale=4e3)
    dprint(len(meas.asd))

    inds = meas.F_Hz > 1e3
    for k, v in meas.items():
        meas[k] = v[inds]
    F_Hz = meas.F_Hz

    budget = load_budget(fpath_join('ifos', 'LLO.yaml'), bname='Quantum')
    budget.ifo.Optics.BSLoss = 3000e-6
    lib.set_gwinc_params(
        budget.ifo,
        Parm_W         = 250e3,
        sqz_dB         = 0,
        homodyne_deg   = -11,
        sec_detune_deg = -0.1,
    )

    def calc_gwinc(xx):
        readout_loss = xx[0]
        budget.ifo.Optics.PhotoDetectorEfficiency = 1 - readout_loss
        traces = budget.run(freq=F_Hz)
        return traces
        # return traces.asd

    def cost(xx):
        model = calc_gwinc(xx).asd
        return np.sum(((meas.asd - model) / meas.err)**2)

    res = minimize(cost, [0.15], bounds=[(0.01, 0.4)])
    dprint(res)
    traces_opt = calc_gwinc(res.x)
    if export_yaml:
        budget.ifo.to_yaml(fpath_join('ifos', 'LLO_readout.yaml'))

    fig = lib.plot_budget_residuals(traces_opt, meas, dtype='asd')
    ax_bdgt, ax_res = fig.axes
    plot_15 = False
    if plot_15:
        traces_15 = calc_gwinc([0.15])
        ax_res.errorbar(
            F_Hz,
            traces_15.asd - meas.asd,
            meas.err,
            marker='.',
            color='xkcd:brick red',
            # label='15%',
        )
        handles = ax_res.lines
        labels = [
            '{:0.2f}'.format(res.x[0]),
            '0.15']
        ax_res.legend(handles, labels)
    # ax_bdgt.set_yscale('linear')
    ax_bdgt.set_ylim(5e-21, 1e-19)
    ax_res.set_xscale('linear')
    fig.savefig(tpath_join('fit.pdf'))


def test_llo_subtraction(sqz_data_notched, fpath_join, tpath_join, dprint):
    from gwinc.noise.quantum import shotrad_debug
    from gwinc import load_budget

    # ifo = load_budget(fpath_join('LLO_no_sqz.yaml')).ifo
    ifo = load_budget(fpath_join('ifos', 'LLO.yaml')).ifo
    lib.set_gwinc_params(
        ifo,
        Parm_W         = 250e3,
        sqz_dB         = 0,
        homodyne_deg   = 11,
        sec_detune_deg = -0.1,
        readout_loss   = 0.23,
    )
    F_Hz = sqz_data_notched.fis.F_Hz
    Flog_Hz = np.logspace(np.log10(F_Hz[0]), np.log10(F_Hz[-1]), 200)
    sr = shotrad_debug(F_Hz, ifo)
    calG = sr.PSDdisplacement / ifo.Infrastructure.Length**2

    relgamma_dB = Bunch()
    for key, psd in sqz_data_notched.items():
        if 'no_sqz' in key:
            continue
        relgamma_dB[key] = lib.make_relgamma_dB(
            psd, sqz_data_notched.no_sqz, calG, sr.Gamma_IFO, Flog_Hz=Flog_Hz,
        )

    fig, ax = plt.subplots()
    ax.loglog(F_Hz, calG)
    lib.makegrid(ax, F_Hz)
    fig.savefig(tpath_join('cal.pdf'))

    fig, ax = plt.subplots()
    ax.loglog(F_Hz, sr.Gamma_IFO)
    lib.makegrid(ax, F_Hz)
    fig.savefig(tpath_join('gamma.pdf'))

    fig, ax = plt.subplots()
    for key, dset in relgamma_dB.items():
        # ax.semilogx(dset.F_Hz, dset.psd, label=key)
        ax.errorbar(
            dset.F_Hz, dset.psd, dset.err, marker='.', label=key)
    ax.axhline(-5.1)
    ax.axhline(-5.5)
    ax.set_ylim(-8, 16)
    ax.set_xscale('log')
    lib.makegrid(ax, F_Hz)
    ax.legend()
    fig.savefig(tpath_join('relgamma.pdf'))


def calc_gamma_calibration(ifo, F_Hz, total_readout):
    """
    Calculates the calibration G and optomechanical gain Gamma

    This sets the squeezing and all mismatch to zero to get the optical gain.
    This should also generally be called before any rebinning.

    ifo: ifo struct
    F_Hz: frequency vector
    total_readout: total readout loss including the "true readout loss" and
      unexplained losses attributed to mode mismatch
    """
    ifo = deepcopy(ifo)
    lib.set_gwinc_params(ifo, sqz_dB=0, readout_loss=total_readout)
    ifo.Optics.MM_IFO_OMC = 0
    # SQZ_OMC is already 0 when sqz_dB = 0
    sr = shotrad_debug(F_Hz, ifo)
    return Bunch(
        calG      = sr.PSDdisplacement / ifo.Infrastructure.Length**2,
        Gamma_IFO = sr.Gamma_IFO,
    )


def fit_sqz_angle(meas_dB, budget, phi0_deg):
    """
    Fit the SQZ angle to data

    meas_dB: relgamma data in dB
    budget: QuantumRelGamma budget
    phi0_deg: initial guess for SQZ angle
    """
    def calc_gwinc(xx):
        budget.ifo.Squeezer.SQZAngle = xx[0] * np.pi / 180
        traces = budget.run(freq=meas_dB.F_Hz)
        return 10 * np.log10(traces.psd)

    def cost(xx):
        model = calc_gwinc(xx)
        return np.sum(((meas_dB.psd - model) / meas_dB.err)**2)

    res = minimize(cost, [phi0_deg])
    print(res)
    return res.x[0]


@pytest.mark.skip
def test_llo_single_sqz_fit(sqz_data_notched, fpath_join, tpath_join, dprint):
    # meas_psd = sqz_data_notched.fis
    # meas_psd = sqz_data_notched.fias
    meas_psd = sqz_data_notched.fds
    # meas_psd = sqz_data_notched.fdas
    Flog_Hz = np.logspace(
        np.log10(meas_psd.F_Hz[0]), np.log10(meas_psd.F_Hz[-1]), 200,
    )

    budget = load_budget(fpath_join('ifos', 'LLO.yaml'), bname='QuantumRelGamma')
    ifo = deepcopy(budget.ifo)
    total_readout = 0.2
    readout_loss = 1 - ifo.Optics.PhotoDetectorEfficiency
    homodyne_deg = -11
    Parm_W = 250e3
    sec_detune_deg = 0
    lib.set_gwinc_params(
        ifo,
        Parm_W = Parm_W,
        homodyne_deg = homodyne_deg,
        sec_detune_deg = sec_detune_deg,
    )
    cals = calc_gamma_calibration(ifo, meas_psd.F_Hz, total_readout)

    meas_relgamma = lib.make_relgamma_dB(
        meas_psd, sqz_data_notched.no_sqz, cals.calG, cals.Gamma_IFO, Flog_Hz=Flog_Hz,
    )
    F_Hz = meas_relgamma.F_Hz

    lib.set_gwinc_params(
        ifo,
        Parm_W = Parm_W,
        sqz_dB = 14.7,
        homodyne_deg = homodyne_deg,
        sec_detune_deg = sec_detune_deg,
        readout_loss = readout_loss,
    )
    # ifo.Optics.MM_IFO_OMC = total_readout - readout_loss
    # ifo.Squeezer.SQZAngle = 83 * np.pi/180 # -10 * np.pi/180
    # ifo.Squeezer.Type = 'Freq Independent'
    # ifo.Squeezer.Type = 'Freq Dependent'
    ifo = Struct.from_file(fpath_join('fis_ref.yaml'))
    ifo.Squeezer.FilterCavity.fdetune = -25
    ifo.Squeezer.Type = 'Freq Dependent'
    traces = budget.run(freq=F_Hz, ifo=ifo)
    SQZAngle_deg = fit_sqz_angle(meas_relgamma, budget, 0)
    dprint(SQZAngle_deg)
    ifo.Squeezer.SQZAngle = SQZAngle_deg * np.pi / 180

    traces = budget.run(ifo=ifo)

    fig = lib.plot_budget_residuals(traces, meas_relgamma, dtype='psd')
    # fig.axes[0].set_ylim(-20, 12)
    fig.axes[0].set_ylim(-20, 20)
    fig.savefig(tpath_join('fit.pdf'))


def test_llo_all_sqz_fits(sqz_data_notched, fpath_join, tpath_join, dprint):
    export_yaml = False
    fis_data  = sqz_data_notched.fis
    fias_data = sqz_data_notched.fias
    fds_data  = sqz_data_notched.fds
    fdas_data = sqz_data_notched.fdas
    Flog_Hz = np.logspace(
        np.log10(fis_data.F_Hz[0]), np.log10(fis_data.F_Hz[-1]), 200,
    )

    budget = load_budget(fpath_join('ifos', 'LLO.yaml'), bname='QuantumRelGamma')
    ifo = deepcopy(budget.ifo)
    total_readout = 0.23
    readout_loss = 1 - ifo.Optics.PhotoDetectorEfficiency
    homodyne_deg = -11
    Parm_W = 250e3
    sec_detune_deg = -0.1
    lib.set_gwinc_params(
        ifo,
        Parm_W         = Parm_W,
        homodyne_deg   = homodyne_deg,
        sec_detune_deg = sec_detune_deg,
    )
    cals = calc_gamma_calibration(ifo, fis_data.F_Hz, total_readout)

    fis_rg = lib.make_relgamma_dB(
        fis_data, sqz_data_notched.no_sqz, cals.calG, cals.Gamma_IFO, Flog_Hz=Flog_Hz,
    )
    fias_rg = lib.make_relgamma_dB(
        fias_data, sqz_data_notched.no_sqz, cals.calG, cals.Gamma_IFO, Flog_Hz=Flog_Hz,
    )
    F_Hz = fis_rg.F_Hz

    sqz_dB = 14.7
    ifo.Squeezer.Type = 'Freq Independent'
    ifo.Squeezer.direct_mm_sqz_ifo = False
    dread_loss = 0.05
    ifo.Optics.MM_IFO_OMC = total_readout - readout_loss - dread_loss
    dprint(ifo.Optics.MM_IFO_OMC)
    ifo.Squeezer.MM_SQZ_OMC = ifo.Optics.MM_IFO_OMC
    ifo.Optics.MM_IFO_OMCphi = 0 * np.pi / 180
    ifo.Squeezer.MM_SQZ_OMCphi = 0 * np.pi / 180
    fit_mismatch = True
    fit_fds = True
    plot_fis = True
    plot_fds = True

    lib.set_gwinc_params(
        ifo,
        Parm_W         = Parm_W,
        sqz_dB         = sqz_dB,
        homodyne_deg   = homodyne_deg,
        sec_detune_deg = sec_detune_deg,
        readout_loss   = readout_loss + dread_loss,
    )
    traces = budget.run(freq=F_Hz, ifo=ifo)

    def calc_budget(dset, key, phi0_deg=None):
        """
        if phi0_deg is None, fit the SQZ angle, otherwise that is the SQZ angle
        """
        if phi0_deg is None:
            if key in ['fis', 'fds']:
                SQZAngle_deg = fit_sqz_angle(dset, budget, 0)
            elif key in ['fias', 'fdas']:
                SQZAngle_deg = fit_sqz_angle(dset, budget, 90)
        else:
            SQZAngle_deg = phi0_deg
        dprint(key, SQZAngle_deg)
        ifo.Squeezer.SQZAngle = SQZAngle_deg * np.pi / 180
        traces = budget.run(ifo=ifo)
        return traces

    phi_fis_deg  = -12.5
    phi_fias_deg = 83.3

    def calc_gwinc(xx):
        """
        0: MM_SQZ_OMC
        1: MM_SQZ_OMCphi
        2: SQZ db
        """
        budget.ifo.Squeezer.MM_SQZ_OMC = xx[0]
        budget.ifo.Squeezer.MM_SQZ_OMCphi = xx[1] * np.pi / 180
        budget.ifo.Squeezer.SQZAngle = phi_fis_deg * np.pi / 180
        budget.ifo.Squeezer.AmplitudedB = xx[2]
        tr_sqz = budget.run()
        budget.ifo.Squeezer.SQZAngle = phi_fias_deg * np.pi / 180
        tr_asqz = budget.run()
        return 10 * np.log10(tr_sqz.psd), 10 * np.log10(tr_asqz.psd)

    def cost(xx):
        model_sqz, model_asqz = calc_gwinc(xx)
        chi2_sqz  = np.sum(((fis_rg.psd - model_sqz) / fis_rg.err)**2)
        chi2_asqz = np.sum(((fias_rg.psd - model_asqz) / fias_rg.err)**2)
        return chi2_sqz + chi2_asqz

    if fit_mismatch:
        res = minimize(
            cost,
            [0.02, 4, 14.6],
            bounds=[(0.0, 0.1), (-180, 180), (14.3, 15)],
        )
        dprint(res)
        budget.ifo.Squeezer.MM_SQZ_OMC = res.x[0]
        budget.ifo.Squeezer.MM_SQZ_OMCphi = res.x[1] * np.pi / 180
        budget.ifo.Squeezer.AmplitudedB = res.x[2]

    tr_fis  = calc_budget(fis_rg, 'fis', phi_fis_deg)
    if export_yaml:
        budget.ifo.to_yaml(fpath_join('ifos', 'LLO_fis.yaml'))
        ifo_nosqz = deepcopy(budget.ifo)
        ifo_nosqz.Squeezer.AmplitudedB = 0
        ifo_nosqz.to_yaml(fpath_join('ifos', 'LLO_no_sqz.yaml'))
    tr_fias = calc_budget(fias_rg, 'fias', phi_fias_deg)
    if export_yaml:
        budget.ifo.to_yaml(fpath_join('ifos', 'LLO_fias.yaml'))

    if plot_fis:
        fig_fis  = lib.plot_budget_residuals(tr_fis, fis_rg, dtype='psd')
        fig_fias = lib.plot_budget_residuals(tr_fias, fias_rg, dtype='psd')
        fig_fis.axes[0].set_ylim(-20, 12)
        fig_fias.axes[0].set_ylim(-1, 20)
        fig_fis.savefig(tpath_join('fis.pdf'))
        fig_fias.savefig(tpath_join('fias.pdf'))

    if not fit_fds:
        return

    fds_rg = lib.make_relgamma_dB(
        fds_data, sqz_data_notched.no_sqz, cals.calG, cals.Gamma_IFO, Flog_Hz=Flog_Hz,
    )
    fdas_rg = lib.make_relgamma_dB(
        fdas_data, sqz_data_notched.no_sqz, cals.calG, cals.Gamma_IFO, Flog_Hz=Flog_Hz,
    )

    dprint('MM_SQZ_OMC', ifo.Squeezer.MM_SQZ_OMC)
    ifo.Squeezer.Type = 'Freq Dependent'
    # dprint(ifo.Squeezer.FilterCavity.fdetune)
    ifo.Squeezer.FilterCavity.L_mm = 0.01
    ifo.Squeezer.FilterCavity.psi_mm = 176 * np.pi / 180
    ifo.Squeezer.FilterCavity.fdetune = -24
    ifo.Squeezer.FilterCavity.Lrms = 0.5e-12
    fit_fc = False

    traces = budget.run(freq=F_Hz, ifo=ifo)

    phi_fds_deg = -12.9
    phi_fdas_deg = 82.2

    def calc_gwinc(xx):
        """
        0: fdetune
        1: L_mm
        2: psi_mm
        """
        ifo.Squeezer.FilterCavity.fdetune = xx[0]
        ifo.Squeezer.FilterCavity.L_mm = xx[1]
        ifo.Squeezer.FilterCavity.psi_mm = xx[2] * np.pi / 180
        ifo.Squeezer.SQZAngle = phi_fds_deg * np.pi / 180
        tr_sqz= budget.run(ifo=ifo)
        ifo.Squeezer.SQZAngle = phi_fdas_deg * np.pi / 180
        tr_asqz = budget.run(ifo=ifo)
        return 10 * np.log10(tr_sqz.psd), 10 * np.log10(tr_asqz.psd)

    def cost(xx):
        model_sqz, model_asqz = calc_gwinc(xx)
        chi2_sqz  = np.sum(((fds_rg.psd - model_sqz) / fds_rg.err)**2)
        chi2_asqz = np.sum(((fdas_rg.psd - model_asqz) / fdas_rg.err)**2)
        return chi2_sqz + chi2_asqz

    if fit_fc:
        res = minimize(
            cost,
            [-25, 0.01, 10],
            bounds = [(-40, -20), (0.0, 0.015), (-180, 180)],
        )
        dprint(res)
        ifo.Squeezer.FilterCavity.fdetune = res.x[0]
        ifo.Squeezer.FilterCavity.L_mm = res.x[1]
        ifo.Squeezer.FilterCavity.psi_mm = res.x[2] * np.pi / 180

    tr_fds  = calc_budget(fds_rg, 'fds', phi_fds_deg)
    if export_yaml:
        budget.ifo.to_yaml(fpath_join('ifos', 'LLO_fds.yaml'))
    tr_fdas = calc_budget(fdas_rg, 'fdas', phi_fdas_deg)
    if export_yaml:
        budget.ifo.to_yaml(fpath_join('ifos', 'LLO_fdas.yaml'))

    if plot_fds:
        fig_fds  = lib.plot_budget_residuals(tr_fds, fds_rg, dtype='psd')
        fig_fdas = lib.plot_budget_residuals(tr_fdas, fdas_rg, dtype='psd')
        fig_fds.axes[0].set_ylim(-20, 12)
        fig_fdas.axes[0].set_ylim(-1, 20)
        fig_fds.savefig(tpath_join('fds.pdf'))
        fig_fdas.savefig(tpath_join('fdas.pdf'))


def test_plot_fits(sqz_data_notched, fpath_join, tpath_join, dprint):
    dprint(sqz_data_notched.keys())
    F0_Hz = sqz_data_notched.fis.F_Hz
    Flog_Hz = np.logspace(np.log10(F0_Hz[0]), np.log10(F0_Hz[-1]), 200)
    b_disp = load_budget(fpath_join('ifos', 'LLO.yaml'), bname='Quantum')
    b_relg = load_budget(fpath_join('ifos', 'LLO.yaml'), bname='QuantumRelGamma')
    dset_keys = ['fis', 'fias', 'fds', 'fdas', 'no_sqz']

    ifos = Struct({
        k: Struct.from_file(fpath_join('ifos', 'LLO_{:s}.yaml'.format(k)))
        for k in dset_keys
    })
    # ifos.no_sqz.Squeezer.Readout.follow_fringe = False
    ifos['no_sqz'] = Struct.from_file(fpath_join('ifos', 'LLO_readout.yaml'))
    sqz_data = Bunch({
        k: lib.resample_psd(v, Flog_Hz) for k, v in sqz_data_notched.items()})
    tr_disp  = Bunch()
    tr_relg  = Bunch()
    sqz_relg = Bunch()

    total_readout = 0.24
    cals = calc_gamma_calibration(ifos.no_sqz, F0_Hz, total_readout)

    F_Hz = sqz_data.fis.F_Hz
    for k, ifo in ifos.items():
        tr_disp[k]  = b_disp.run(freq=F_Hz, ifo=ifo)
        tr_relg[k]  = b_relg.run(freq=F_Hz, ifo=ifo)
        sqz_relg[k] = lib.make_relgamma_dB(
            sqz_data_notched[k], sqz_data_notched.no_sqz, cals.calG,
            cals.Gamma_IFO, Flog_Hz=Flog_Hz,
        )

    colors = dict(
        fds    = 'xkcd:cerulean',
        fdas   = 'xkcd:crimson',
        fis    = 'xkcd:kelly green',
        fias   = 'xkcd:goldenrod',
        no_sqz = 'xkcd:slate',
    )

    fig_disp, ax_disp = plt.subplots()
    fig_relg, ax_relg = plt.subplots()
    for k in dset_keys:
        ax_disp.loglog(F_Hz, tr_disp[k].asd, label=k, c=colors[k])
        ax_disp.loglog(F_Hz, np.sqrt(sqz_data[k].psd) * 4e3, c=colors[k], ls='--')
        ax_relg.semilogx(F_Hz, 10 * np.log10(tr_relg[k].psd), label=k, c=colors[k])
        ax_relg.semilogx(F_Hz, sqz_relg[k].psd, c=colors[k], ls='--')
    for ax_ in [ax_disp, ax_relg]:
        lib.makegrid(ax_, F_Hz)
        ax_.set_xlabel('Frequency [Hz]')
        ax_.legend()
    fig_disp.savefig(tpath_join('displacement.pdf'))
    fig_relg.savefig(tpath_join('relgamma.pdf'))


def test_plot_relgamma_fits(sqz_data_notched, fpath_join, tpath_join, dprint):
    budget = load_budget(fpath_join('ifos', 'LLO.yaml'), bname='QuantumRelGamma')
    ifos = Struct({
        k: Struct.from_file(fpath_join('ifos', 'LLO_{:s}.yaml'.format(k)))
        for k in sqz_data_notched.keys() if k != 'no_sqz'
    })

    total_readout = 0.24
    F0_Hz = sqz_data_notched.fis.F_Hz
    cals = calc_gamma_calibration(
        ifos.fis, F0_Hz, total_readout,
    )

    Flog_Hz = np.logspace(np.log10(F0_Hz[0]), np.log10(F0_Hz[-1]), 200)
    sqz_data = Bunch({
        k: lib.resample_psd(v, Flog_Hz) for k, v in sqz_data_notched.items()
        if k != 'no_sqz'
    })
    F_Hz = sqz_data.fis.F_Hz

    traces = Bunch()
    sqz_relg = Bunch()
    for k, ifo in ifos.items():
        traces[k] = budget.run(freq=F_Hz, ifo=ifo)
        sqz_relg[k] = lib.make_relgamma_dB(
            sqz_data_notched[k], sqz_data_notched.no_sqz, cals.calG,
            cals.Gamma_IFO, Flog_Hz=Flog_Hz,
        )

    colors = dict(
        fds    = 'xkcd:cerulean',
        fdas   = 'xkcd:crimson',
        fis    = 'xkcd:kelly green',
        fias   = 'xkcd:goldenrod',
    )
    labels = dict(
        fds    = 'FD SQZ',
        fdas   = 'FD ASQZ',
        fis    = 'FI SQZ',
        fias   = 'FI ASQZ',
    )


    fig = plt.figure(figsize=(6.4, 6.4))
    gs = fig.add_gridspec(2, 1, height_ratios=[2, 1], hspace=0.1)
    ax_dat = fig.add_subplot(gs[0])
    ax_res = fig.add_subplot(gs[1], sharex=ax_dat)

    for k, trace in traces.items():
        ax_dat.semilogx(F_Hz, 10 * np.log10(trace.psd),
                        label=labels[k], c=colors[k])
        ax_dat.errorbar(
            F_Hz, sqz_relg[k].psd, sqz_relg[k].err,
            ls='', marker='.', c=colors[k],
        )
        residuals = sqz_relg[k].psd - 10 * np.log10(trace.psd)
        ax_res.errorbar(
            F_Hz, residuals, sqz_relg[k].err, marker='.', c=colors[k],
        )
    for ax in [ax_dat, ax_res]:
        lib.makegrid(ax, F_Hz)
    ax_dat.axhline(-5.1, ls=':', c='xkcd:slate', label='5.1 dB')
    ax_dat.axhline(-5.5, ls='--', c='xkcd:tangerine', label='5.5 dB')
    ax_dat.legend()
    ax_res.set_xlabel('Frequency [Hz]')
    ax_dat.set_ylabel('Noise relative to unsqueezed vacuum [dB]')
    ax_res.set_ylabel('Residuals')
    _, ymax = ax_dat.get_ylim()
    # ax_dat.set_ylim(-8, ymax)
    ax_dat.set_ylim(-8, 2)
    ax_res.set_ylim(-4, 5)
    ax_dat.legend()
    plt.setp(ax_dat.get_xticklabels(), visible=False)
    fig.savefig(tpath_join('all_relgamma.png'))

    fig = lib.plot_budget_residuals(traces.fis, sqz_relg.fis, dtype='psd')
    fig.axes[0].set_ylim(-18, 12)
    fig.axes[0].set_title('Frequency Independent Squeezing')
    fig.savefig(tpath_join('fis.pdf'))

    fig = lib.plot_budget_residuals(traces.fias, sqz_relg.fias, dtype='psd')
    fig.axes[0].set_ylim(2.5, 17.5)
    fig.axes[0].set_title('Frequency Independent Anti-Squeezing')
    fig.savefig(tpath_join('fias.pdf'))

    fig = lib.plot_budget_residuals(traces.fds, sqz_relg.fds, dtype='psd')
    fig.axes[0].set_ylim(-20, 8)
    fig.axes[0].set_title('Frequency Dependent Squeezing')
    fig.savefig(tpath_join('fds.pdf'))

    fig = lib.plot_budget_residuals(traces.fdas, sqz_relg.fdas, dtype='psd')
    fig.axes[0].set_ylim(2.5, 17.5)
    fig.axes[0].set_title('Frequency Dependent Anti-Squeezing')
    fig.savefig(tpath_join('fdas.pdf'))


def test_discrepancy(fpath_join, tpath_join, dprint):
    budget = load_budget(fpath_join('ifos', 'LLO.yaml'))
    ifo1 = Struct.from_file(fpath_join('ifos', 'LLO_readout.yaml'))
    ifo2 = Struct.from_file(fpath_join('ifos', 'LLO_no_sqz.yaml'))
    F_Hz = np.logspace(np.log10(30), np.log10(3e3), 1000)

    ifo2.Squeezer.Readout.follow_fringe = True

    tr1 = budget.run(freq=F_Hz, ifo=ifo1)
    tr2 = budget.run(freq=F_Hz, ifo=ifo2)
    sr1 = shotrad_debug(F_Hz, ifo1)
    sr2 = shotrad_debug(F_Hz, ifo2)

    fig, ax = plt.subplots()
    ax.loglog(F_Hz, tr1.asd)
    ax.loglog(F_Hz, tr2.asd, ls='--')
    lib.makegrid(ax, F_Hz)
    fig.savefig(tpath_join('displacement.pdf'))

    fig, ax = plt.subplots()
    ax.loglog(F_Hz, sr1.PSDdisplacement)
    ax.loglog(F_Hz, sr2.PSDdisplacement)
    lib.makegrid(ax, F_Hz)
    fig.savefig(tpath_join('PSDdisplacement.pdf'))

    dprint(ifo1.diff(ifo2))
    dprint(ifo2.Optics.MM_IFO_OMC + 1 - ifo2.Optics.PhotoDetectorEfficiency)
    dprint(1 - ifo1.Optics.PhotoDetectorEfficiency)
