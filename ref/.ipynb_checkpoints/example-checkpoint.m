% Examples of MATLAB calling python functions

pyenv % get python environments

% add current path
if count(py.sys.path,'') == 0
    insert(py.sys.path,int32(0),'');
end

% create a python list
N = py.list({'Jones','Johnson','James'})

% calling a python module in the current path
py.mymod.myfunc

% update module if the source code is changed
clear classes; py.importlib.reload(py.importlib.import_module('mymod'));                
py.mymod.myfunc % re-run to see the change


% call gwinc directly
traces = py.gwinc.load_budget("Aplus").run()

% get Quantum traces
traces.budget{1}.asd

% get Seismic traces
traces.budget{2}.asd

% get Quantum subtraces
temp = traces.budget{1}
temp.budget{1}.asd

% change frequency input
traces = py.gwinc.load_budget("Aplus").run(freq=py.numpy.geomspace(10,5000)) % remember that python only takes python input values, not MATLAB arrays




% Sometimes directly calling python module would crash MATLAB, so we can 
% call pyrunfile instead (R2021b)

% Always run this first
pyenv(ExecutionMode="OutOfProcess");

% Update module if the source code is changed
terminate(pyenv); pyenv(ExecutionMode="OutOfProcess");

% Call the python module
data =double(pyrunfile("mymod.py", "asdtrace")); 