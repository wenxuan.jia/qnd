# FIXME: would be more conveient to put all this stuff into a SQZData class with
# appropriate __mul__ and __add__ methods and psd, asd, and dB properties
import numpy as np
from wield.bunch import Bunch


def copy_single_bunch(old):
    # new is needed when loading data into a HDFDeepBunch for some applications
    new = Bunch({k: v for k, v in old.items()})
    return new


def make_psd_errors(psd):
    new = copy_single_bunch(psd)
    new.err = psd.psd / np.sqrt(2 * psd.binwidth_Hz * psd.duration_s)
    return new


def subtract_psds(psd1, psd2):
    assert np.all(psd1.F_Hz == psd2.F_Hz)
    return Bunch(
        F_Hz = psd1.F_Hz,
        psd  = psd1.psd - psd2.psd,
        err  = np.sqrt(psd1.err**2 + psd2.err**2),
    )


def psd2asd(psd, scale=None):
    asd = Bunch(
        F_Hz = psd.F_Hz,
        asd  = np.sqrt(psd.psd),
        err  = np.sqrt(psd.psd + psd.err) - np.sqrt(psd.psd),
    )
    if scale is not None:
        asd.asd *= scale
        asd.err *= scale
    return asd


def psd2dB(psd):
    return Bunch(
        F_Hz = psd.F_Hz,
        psd  = 10 * np.log10(psd.psd),
        err  = 10 * np.log10(1 + psd.err / psd.psd),
    )


def resample_psd(psd, Fnew_Hz):
    inds = np.searchsorted(psd.F_Hz, Fnew_Hz)
    psd_new = Bunch(
        F_Hz = [],
        psd  = [],
        err  = [],
    )
    bins = zip(inds[:-1], inds[1:])
    for indL, indR in bins:
        bin_width = indR - indL
        if bin_width == 0:
            continue
        psd_new.F_Hz.append(np.mean(psd.F_Hz[indL:indR]))
        psd_new.psd.append(np.median(psd.psd[indL:indR]))
        psd_new.err.append(
            np.median(psd.err[indL:indR]) / np.sqrt(bin_width)
        )
    psd_new = Bunch({k: np.array(v) for k, v in psd_new.items()})
    return psd_new


def rebin(psd, Fnew_Hz):
    inds = np.searchsorted(psd.F_Hz, Fnew_Hz)
    psd_new = Bunch(
        F_Hz = [],
        psd  = [],
        err  = [],
    )
    bins = zip(inds[:-1], inds[1:])
    for indL, indR in bins:
        bin_width = indR - indL
        if bin_width == 0:
            continue
        psd_new.F_Hz.append(np.mean(psd.F_Hz[indL:indR]))
        psd_new.psd.append(np.median(psd.psd[indL:indR]))
        psd_new.err.append(
            np.median(psd.err[indL:indR]) / np.sqrt(bin_width)
        )
    psd_new = Bunch({k: np.array(v) for k, v in psd_new.items()})
    return psd_new


def make_relgamma_dB(sqz_data, ref_data, calG, Gamma_IFO, Flog_Hz=None):
    diff = subtract_psds(sqz_data, ref_data)
    diff.psd = diff.psd / calG
    diff.err = diff.err / calG
    relgamma = Bunch(
        F_Hz = diff.F_Hz,
        psd  = diff.psd / Gamma_IFO + 1,
        err  = diff.err / Gamma_IFO,
    )
    if Flog_Hz is not None:
        relgamma = resample_psd(relgamma, Flog_Hz)
    return psd2dB(relgamma)


def notch_freq_inds(F_Hz, *notches, fmin=None, fmax=None):
    inds = np.ones(len(F_Hz), dtype=bool)
    for flow, fhigh in notches:
        new_inds = np.logical_or(F_Hz < flow, F_Hz > fhigh)
        inds = np.logical_and(inds, new_inds)
    if fmin is not None:
        inds = np.logical_and(inds, F_Hz >= fmin)
    if fmax is not None:
        inds = np.logical_and(inds, F_Hz <= fmax)
    return inds


def set_gwinc_params(
        ifo,
        Parm_W         = None,
        sqz_dB         = None,
        homodyne_deg   = 0,
        sec_detune_deg = 0,
        readout_loss   = None,
):
    if Parm_W is not None:
        ifo.Laser.ArmPower = Parm_W
        del ifo.Laser.Power
    if sqz_dB is not None:
        ifo.Squeezer.AmplitudedB = sqz_dB
    if readout_loss is not None:
        ifo.Optics.PhotoDetectorEfficiency = 1 - readout_loss
    ifo.Optics.Quadrature.dc = np.pi / 180 * (90 - homodyne_deg)
    # ifo.Squeezer.Readout.Angle = np.pi / 180 * (90 - homodyne_deg)
    ifo.Optics.SRM.Tunephase = 2 * sec_detune_deg * np.pi / 180


def makegrid(ax, xarr=None):
    ax.grid(True, which='major', alpha=0.5)
    ax.grid(True, which='minor', alpha=0.2)
    if xarr is not None:
        ax.set_xlim(xarr[0], xarr[-1])


def plot_budget_residuals(traces, data, dtype, freq_lims=None):
    import matplotlib.pyplot as plt

    F_Hz = traces.freq
    fig = plt.figure(figsize=(6.4, 6.4))
    gs = fig.add_gridspec(2, 1, height_ratios=[2, 1], hspace=0.1)
    ax_bdgt = fig.add_subplot(gs[0])
    ax_res  = fig.add_subplot(gs[1], sharex=ax_bdgt)

    ax_bdgt.errorbar(
        F_Hz,
        getattr(data, dtype),
        data.err,
        marker='.',
        alpha=0.7,
        lw=3,
        color='xkcd:royal blue',
        label='Data',
    )

    ############################################################
    # gwinc budget
    # need to hack and I don't want to change gwinc itself
    ############################################################

    if dtype == 'asd':
        def plot_func(trace):
            return trace.asd
    elif dtype == 'psd':
        def plot_func(trace):
            return 10 * np.log10(trace.psd)

    style = dict(
        color='#000000',
        alpha=0.6,
        linewidth=4,
    )
    style.update(getattr(traces, 'style', {}))
    if 'color' not in style and 'c' not in style:
        style['color'] = '#000000'
    if 'alpha' not in style:
        style['alpha'] = 0.6
    if 'linewidth' not in style and 'lw' not in style:
        style['linewidth'] = 4
    if 'label' in style:
        style['label'] = 'Total ' + style['label']
    else:
        style['label'] = 'Total'

    ax_bdgt.plot(F_Hz, plot_func(traces), **style)
    for name, strace in traces.items():
        style = strace.style
        if 'label' not in style:
            style['label'] = name
        if 'linewidth' not in style and 'lw' not in style:
            style['linewidth'] = 3
        ax_bdgt.plot(F_Hz, plot_func(strace), **style)

    # traces.plot(ax=ax_bdgt)
    handles, labels = ax_bdgt.get_legend_handles_labels()
    handles = [handles[-1]] + handles[:-1]
    labels = [labels[-1]] + labels[:-1]
    ax_bdgt.legend(handles, labels, ncol=2, fontsize='small')

    ############################################################
    # residuals and final formatting
    ############################################################

    if freq_lims is not None:
        inds = np.logical_and(
            traces.freq >= freq_lims[0],
            traces.freq <= freq_lims[1],
        )
    else:
        inds = np.ones(len(traces.freq), dtype=bool)
    if dtype == 'asd':
        residuals = data.asd - traces.asd
        ylabel = 'Displacement [m/Hz$^{1/2}$]'
        ax_bdgt.set_yscale('log')
    elif dtype == 'psd':
        residuals = data.psd - 10 * np.log10(traces.psd)
        ylabel = 'Noise relative to unsqueezed vacuum [dB]'
    ax_res.errorbar(
        F_Hz[inds],
        # (data.asd - traces.asd)[inds],
        residuals[inds],
        data.err[inds],
        marker='.',
        color='xkcd:royal blue',
    )
    ax_bdgt.set_xlabel('')
    ax_bdgt.set_ylabel(ylabel)
    ax_res.set_xlabel('Frequency [Hz]')
    ax_res.set_ylabel('Residuals')
    plt.setp(ax_bdgt.get_xticklabels(), visible=False)
    for ax in [ax_bdgt, ax_res]:
        makegrid(ax, F_Hz)
        # ax.grid(True, which='major', alpha=0.5)
        # ax.grid(True, which='minor', alpha=0.2)
    ax_res.set_xscale('log')

    return fig
