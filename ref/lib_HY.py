import numpy as np
import scipy as sp
import scipy.signal
import matplotlib
import matplotlib.pyplot as plt
import pickle
import control as ctr
from scipy import stats
import scipy.constants as scc
from scipy.io import loadmat
import gwinc

# deprecated. Use average='median' now
def psd(time_series, dur=10, segdur=2): 
    # DARM from DCPD, same proved
    darm2 = {}
    
    rate = 16384
    # dur = 10
    # segdur = 2  # fft length per segment [s]

    band = range(dur*rate) # each FFT is 10-sec long data

    # get sample frequency
    f,dcpdsum1 = scipy.signal.welch(time_series[band],rate,window='hann',nperseg=rate*segdur,noverlap=0.5) 

    fulldur = len(time_series)/rate
    band = range(dur*rate)
    n = int(fulldur/dur) #segment no
    darm2s = []
    for k in range(n):
        dcpdsumraw = time_series[band]
        f,dcpdsum1 = scipy.signal.welch(dcpdsumraw,rate,window='hann',nperseg=rate*segdur,noverlap=0.5)
        darm2s.append(np.sqrt(dcpdsum1))
        band = [x+dur*rate for x in band]
        # print('Finished ' + str(k))
    S = (np.median(darm2s,axis=0)[1:])**2
    freq = f[1:]
    
    return (freq, S)


def psd_mean(time_series, dur=10, segdur=2):
    # DARM from DCPD, same proved
    darm2 = {}
    
    rate = 16384
    f,S = scipy.signal.welch(time_series,rate,window='hann',nperseg=rate*segdur,noverlap=0.5,average='median')
    S = S[1:]
    freq = f[1:]
    
    return (freq, S)