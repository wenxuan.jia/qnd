filename = './qnd';
fileext = '.tex';

fid1 = fopen([filename, fileext]);
fid2 = fopen([filename, '_clean', fileext], 'w');

tline = fgetl(fid1);

ignore = false;
linenum = 1;

while ischar(tline)
    % disp(tline)
    if contains(tline, '\begin{tcolorbox}')
        ignore = true;
    end

    if ~ignore
        fprintf(fid2, '%s', tline);
        fprintf(fid2, '\n');
    end
    linenum = linenum + 1;
    
    if contains(tline, '\end{tcolorbox}')
        ignore = false;
    end

    tline = fgetl(fid1);
end



fclose(fid1);
fclose(fid2);