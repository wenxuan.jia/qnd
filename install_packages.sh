# Install python packages
pip install numpy
pip install -U matplotlib
pip install scipy
pip install h5py

pip install corner
pip install emcee

# Install wield packages
pip install wield.utilities

pip install pyyaml


# Install gwinc
git clone git@git.ligo.org:gwinc/pygwinc.git
cd pygwinc/
pip install -e .
git checkout superQK